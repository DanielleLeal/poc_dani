<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ItensPatrimonio;

class Patrimonio extends Model
{
    protected $table = "patrimonio";

    protected $primaryKey = "patCodigo";

    protected $fillable = [
        'patNome', 'patFoto', 'patQuantidade','categoria_catCodigo','fornecedor_forCodigo','patValorCompra',
    ];

    public function categoria(){
        return $this->belongsTo('App\Categoria','categoria_catCodigo','catCodigo');
    }

    public function user(){
        return $this->belongsTo('App\Users','users_useCodigo','useCodigo');
    }

    public function itens_patrimonio(){
        return $this->hasMany('App\ItensPatrimonio','patrimonio_patCodigo','patCodigo');
    }

    public function fornecedor(){
        return $this->belongsTo('App\Fornecedor', 'fornecedor_forCodigo','forCodigo');
        
    }

    public function getItens($patCodigo)
    {
        $itens = ItensPatrimonio::where('patrimonio_patCodigo',$patCodigo)
                                ->where('itpStatus','<>','Inativo')
                                ->get();
        return $itens;
    }

    


    
}
