<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = "estado";

    protected $primaryKey = "estCodigo";

    protected $fillable = [
        'estEstado', 'estSigla',
    ];

    public function enderecos(){
        return $this->hasMany('App\Endereco','estado_estCodigo','estCodigo');
    }

    public function cidades(){
        return $this->hasMany('App\Cidade','estado_estCodigo','estCodigo');
    }
}
