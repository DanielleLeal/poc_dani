<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Historico;

class Depreciacao implements ShouldQueue
{
    private $patrimonio;
    private $vidaUtil;
    private $ano;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($patrimonio,$vidaUtil,$ano)
    {
        $this->patrimonio = $patrimonio;
        $this->vidaUtil = $vidaUtil;
        $this->ano = $ano;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            // só pegando itens de todos os status, menos inativo
            foreach($this->patrimonio->getItens($this->patrimonio->patCodigo) as $item){
                // Ex: carro => Vida útil =5 anos. Então o item deve depreciar por 5 anos.
                // Então aqui eu estou verificando o número de vezes que o item depreciou
                if($item->itpDepreciacao < $this->vidaUtil){ 
                    $his = Historico::where('itensPatrimonio_itpCodigo',$item->itpCodigo)
                                            ->where('hisAno',$this->ano)
                                            ->first();
                    // o item só pode depreciar uma vez no ano
                    if($his == null){ // então se nn foi depreciado no ano informado, eu realizo a depreciacao
                        $valorResidual = ($this->patrimonio->patValorCompra/$this->vidaUtil);
                        /*
                            método linear = a depreciacao a cada ano corresponde ao mesmo valor
                            a vida útil já corresponde ao limite de deprecicao
                            •depreciaçãoPorAno = (valor de compra – valor residual) / vida útil;
                        */
                        $depreciacao = ($this->patrimonio->patValorCompra - $valorResidual)/$this->vidaUtil;

                        $valorDepreciado = $item->itpValorAtual - $depreciacao;
                        $item->itpValorAtual = $valorDepreciado;
                        $item->itpDepreciacao = $item->itpDepreciacao + 1;
                        $item->update();

                        $historico = new Historico();
                        $historico->hisAno = $this->ano;
                        $historico->hisValor =  $valorDepreciado;
                        $historico->itensPatrimonio_itpCodigo = $item->itpCodigo;
                        $historico->save();

                    }
                }
            }
            
            
        
    }
}
