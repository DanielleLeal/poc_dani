<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itens_Emprestimo extends Model
{
    protected $table = "itens_emprestimo";

    protected $primaryKey = "iteCodigo";

    protected $fillable = [
        'itensPatrimonio_itpCodigo', 'emprestimo_empCodigo',
    ];
}
