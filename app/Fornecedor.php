<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    protected $table = "fornecedor";

    protected $primaryKey = "forCodigo";

    protected $fillable = [
        'forNome', 'forCPF', 'forTelefone','forCelular','forEmail','forStatus','endereco_endCodigo',
    ];

    public function endereco(){
        return $this->belongsTo('App\Endereco','endereco_endCodigo','endCodigo');
    }

    public function patrimonios(){
        return $this->hasMany('App\Patrimonio','fornecedor_forCodigo','forCodigo');
    }

    public function emprestimo(){
        return $this->hasMany('App\Emprestimo','fornecedor_forCodigo','forCodigo');
    }
}
