<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    protected $table = "unidade";

    protected $primaryKey = "uniCodigo";

    protected $fillable = [
        'uniNome', 'endereco_endCodigo',
    ];

    public function endereco(){
        return $this->belongsTo('App\Endereco','endereco_endCodigo','endCodigo');
    }

    public function itensPatrimonio(){
        return $this->hasMany('App\ItensPatrimonio','unidade_uniCodigo','uniCodigo');
    }


}
