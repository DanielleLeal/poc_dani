<?php

namespace App\Exports;

use App\Patrimonio;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PatrimoniosExport implements FromCollection,ShouldAutoSize,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $patrimonios = Patrimonio::join('categoria as cat','cat.catCodigo','patrimonio.categoria_catCodigo')
                                ->join('fornecedor as for','for.forCodigo','patrimonio.fornecedor_forCodigo')
                                ->get(['patrimonio.patNome','patrimonio.patValorCompra','patQuantidade',
                                        'cat.catNome','for.forNome',]);
        return $patrimonios;
    }

    public function headings(): array
    {
        return [
            'Nome do Patrimônio',
            'Valor de Compra',
            'Quantidade de itens',
            'Categoria',
            'Fornecedor'
        ];
    }
}
