<?php

namespace App\Exports;

use App\ItensPatrimonio;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ItensPatrimonioExport implements FromCollection,ShouldAutoSize,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $patCodigo;

    public function __construct($patCodigo)
    {
        $this->patCodigo = $patCodigo;
    }
    public function collection()
    {
        $itensPatrimonio = ItensPatrimonio::join('patrimonio as pat','pat.patCodigo','itens_patrimonios.patrimonio_patCodigo')
                                        ->join('unidade as uni','uni.uniCodigo','itens_patrimonios.unidade_uniCodigo')
                                        ->where('itens_patrimonios.patrimonio_patCodigo',$this->patCodigo)
                                        ->get(['pat.patNome','itens_patrimonios.itpCodigoBarras','uni.uniNome',
                                            'itens_patrimonios.itpStatus','itens_patrimonios.itpSituacao','itens_patrimonios.itpTipo',
                                            'itens_patrimonios.itpValorAtual']);
        return $itensPatrimonio;
    }

    public function headings(): array
    {
        return [
            'Nome do Patrimônio',
            'Código de barras',
            'Unidade',
            'Status',
            'Situacao',
            'Tipo',
            'Valor Atual'
        ];
    }
}
