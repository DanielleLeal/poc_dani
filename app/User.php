<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'useNome', 'useEmail', 'useSenha','useCelular','useCPF','useFoto','funcao_funCodigo'
    ];
    protected $primaryKey = "useCodigo";
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function funcao(){
        return $this->belongsTo('App\Funcao','funcao_funCodigo','funCodigo');
    }

    public function autorizacoes(){
        return $this->hasMany('App\Autorizacao','users_useCodigo','useCodigo');
    }

    public function patrimonios(){
        return $this->hasMany('App\Patrimonio','users_useCodigo','useCodigo');
    }

    public function emprestimos(){
        return $this->hasMany('App\Emprestimo','users_useCodigo','useCodigo');
    }

    
}
