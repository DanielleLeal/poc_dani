<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = "cidade";

    protected $primaryKey = "Codigo";

    protected $fillable = [
        'cidCidade', 'estado_estCodigo',
    ];

    public function enderecos(){
        return $this->hasMany('App\Endereco','estado_estCodigo','estCodigo');
    }

    public function estado(){
        return $this->belongsTo('App\Estado','estado_estCodigo','estCodigo');
    }

}
