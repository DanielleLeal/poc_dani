<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItensPatrimonio extends Model
{
    protected $table = "itens_patrimonios";

    protected $primaryKey = "itpCodigo";

    protected $fillable = [
       'itpCodigoBarras','itpValorAtual', 'itpStatus', 'itpSituacao','itpTipo','unidade_uniCodigo',
        'patrimonio_patCodigo','nota_fiscal_nofCodigo',
    ];

    public function nota_fiscal(){
        return $this->belongsTo('App\NotaFiscal','nota_fiscal_nofCodigo','nofCodigo');
    }

    public function patrimonio(){
        return $this->belongsTo('App\Patrimonio','patrimonio_patCodigo','patCodigo');
    }

    public function unidade(){
        return $this->belongsTo('App\Unidade','unidade_uniCodigo','uniCodigo');
    }

    public function emprestimo(){
         return $this->belongsTo('App\Emprestimo');
    }

    public function historicos()
    {
        return $this->hasMany('App\Historico','itensPatrimonio_itpCodigo','itpCodigo');
    }
}
