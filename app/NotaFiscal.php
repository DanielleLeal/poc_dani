<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaFiscal extends Model
{
    protected $table = "nota_fiscal";

    protected $primaryKey = "nofCodigo";

    protected $fillable = [
        'nofDescricao', 'nofArquivo',
    ];

    public function items_patrimonio(){
        return $this->hasMany('App\ItensPatrimonio','nota_fiscal_nofCodigo','nofCodigo');
    }
}
