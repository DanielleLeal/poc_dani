<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $table = "endereco";

    protected $primaryKey = "endCodigo";

    protected $fillable = [
        'endRua', 'endNumero', 'endBairro','EndCidade','endCEP','endUF',
    ];

    public function fornecedor(){
        return $this->hasOne('App\Fornecedor','endereco_endCodigo','endCodigo');
    }

    public function unidade(){
        return $this->hasOne('App\Unidade','endereco_endCodigo','endCodigo');
    }

    public function estado(){
        return $this->belongsTo('App\Estado','estado_estCodigo','estCodigo');
    }

    public function cidade(){
        return $this->belongsTo('App\Cidade','cidade_cidCodigo','cidCodigo');
    }

}
