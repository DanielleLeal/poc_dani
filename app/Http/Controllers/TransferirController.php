<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patrimonio;
use App\Unidade;
use App\ItensPatrimonio;

class TransferirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('multiple');
    }

    public function index()
    {
        return view('patrimonio.transferencia',[
            'patrimonios'=> Patrimonio::orderBy('patNome')->get(),
            'unidades'=> Unidade::orderBy('uniNome')->get()
        ]);
    }

    public function getItens($patCodigo)
    {
        $itens = ItensPatrimonio::where('patrimonio_patCodigo',$patCodigo)
                                ->with('unidade')
                                ->paginate(10);
        return response()->json(['itens'=>$itens]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras = [
            'unidade'=> 'required',
            'itensT'=>'required'
        ];

        $mensagens = [
            'unidade.required'=> 'Selecione uma unidade válida para transferir os itens',
            'itensT.required'=>'Selecione os itens que deseja transferir'
        ];

        $request->validate($regras,$mensagens);

        $itens = explode(',' ,$request->itensT[0]);
        
        foreach($itens as $item){
            $it = ItensPatrimonio::find($item);
            if($it != null){
                $it->unidade_uniCodigo = $request->unidade;
                $it->update();
            }
        }

        return redirect()->route('transferencia.index');
    }

    public function pesquisar(Request $request, $patCodigo = null)
    {
        $pesquisar = $request->pesquisar;
        $itens = ItensPatrimonio::when($patCodigo, function ($query, $patCodigo) {
                                    return $query->where('patrimonio_patCodigo', $patCodigo);
                            })
                            ->where(function($query)  use ($pesquisar){
                                $query->where('itpSituacao','like','%'.$pesquisar.'%')
                                        ->orWhere('itpCodigoBarras','like','%'.$pesquisar.'%')
                                        ->orWhere('itpStatus','like','%'.$pesquisar.'%')
                                        ->orWhere('itpTipo','like','%'.$pesquisar.'%');
                            })
                            ->with('unidade')
                            ->paginate(10);
        return response()->json(['itens'=>$itens]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
