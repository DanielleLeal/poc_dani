<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }
    public function index()
    {
        return view('categoria.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras =[
            'catNome' =>'required',
            'catVidaUtil'=>'required',
            'catDescricao'=> 'nullable'
        ];
        $mensagens =[
            'catNome.required'=> 'Nome é um requisito obrigatório',
            'catVidaUtil.required'=> 'Vida útil é um requisito obrigatório',
        ];
        $request->validate($regras,$mensagens);
        $categoria = new Categoria();
        $categoria->catNome =$request->catNome;
        $categoria->catDescricao = $request->catDescricao;
        $categoria->catVidaUtil = $request->catVidaUtil;

        $categoria->save();
        return redirect()->route('categoria.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = Categoria::find($id);
        if($categoria != null){
            return response()->json(['categoria'=>$categoria]);
        }

        return response('Vazio'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Categoria::find($id);
        if($categoria == null){
            return redirect()->back();
        }
        return view('categoria.edit',['categoria'=>$categoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        if($categoria != null){

            $regras =[
                'catNome' =>'required',
                'catVidaUtil'=>'required',
            ];
            $mensagens =[
                'catNome.required'=> 'Nome é requisito obrigatório',
                'catVidaUtil.required'=> 'Vida útil é requisito obrigatório',
            ];
            $request->validate($regras,$mensagens);

            $categoria->catNome =$request->catNome;
            $categoria->catDescricao = $request->catDescricao;
            $categoria->catVidaUtil = $request->catVidaUtil;
    
            $categoria->update();
            return redirect()->route('categoria.index');
        }

        return redirect()
                ->back()
                ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::find($id);
        if($categoria != null){
            $categoria->delete();
            return response()->json('true');

        }
        return response()->json('false');
    }

    public function dataTableJson(){
        return response()->json(['data'=>Categoria::all()]);
    }
}
