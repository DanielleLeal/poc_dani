<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Categoria;
use App\Unidade;
use App\Patrimonio;
use App\ItensPatrimonio;

class RelatoriosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('multiple');
    }
    public function index()
    {
        
       return view('relatorios.index',
        ['unidades'=>Unidade::orderBy('uniNome')->get(),
        'categorias'=>Categoria::orderBy('catNome')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itpStatus = $request->itpStatus;
        $itpSituacao = $request->itpSituacao;
        $itpTipo = $request->itpTipo;
        $uniCodigo = $request->uniCodigo;
        $catCodigo = $request->catCodigo;

        $patrimonios = DB::table('itens_patrimonios as itp')
            ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
            ->join('unidade as uni','uni.uniCodigo','itp.unidade_uniCodigo')
            ->join('categoria as cat','cat.catCodigo','pat.categoria_catCodigo')
            ->select(DB::raw('count(*)  as qtde,patrimonio_patCodigo,itpStatus,
                            itpSituacao,itpTipo,unidade_uniCodigo,
                            pat.patNome,cat.catNome,uni.uniNome,
                            itp.itpValorAtual,pat.created_at'))
                            ->when($itpSituacao, function ($query, $itpSituacao) {
                                return $query->where('itpSituacao', $itpSituacao);
                            })
                            ->when($itpTipo, function ($query, $itpTipo) {
                                return $query->where('itpTipo', $itpTipo);
                            })
                            ->when($itpStatus, function ($query, $itpStatus) {
                                return $query->where('itpStatus', $itpStatus);
                            })
                            ->when($catCodigo, function ($query, $catCodigo) {
                                return $query->where('pat.categoria_catCodigo', $catCodigo);
                            })
                            ->when($uniCodigo, function ($query, $uniCodigo) {
                                return $query->where('itp.unidade_uniCodigo', $uniCodigo);
                            })    
            ->groupBy(['itpStatus',
                        'itpSituacao','itpTipo','patrimonio_patCodigo',
                        'unidade_uniCodigo','pat.patNome',
                        'cat.catNome','uni.uniNome',
                        'itp.itpValorAtual','pat.created_at'])
            ->orderBy('patNome')
            ->get();
            
        return response()->json(['patrimonios'=>$patrimonios]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
