<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\ItensPatrimonio;
use App\Patrimonio;
use App\Historico;
use App\Jobs\Depreciacao;

class DepreciacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }

    public function index()
    {

        return view('patrimonio.depreciacao',['categorias'=>Categoria::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras =[
            'categoria' =>'required',
            'catVidaUtil'=>'required|min:1',
            'depAno'=> 'required'
        ];
        $mensagens =[
            'categoria.required'=> 'Categoria é um requisito obrigatório',
            'catVidaUtil.required'=> 'Vida útil é um requisito obrigatório',
            'catVidaUtil.min'=>'A vida útil deve ser no mínimo 1',
            'depAno.required'=> 'Ano é um requisito obrigatório',
        ];

        $request->validate($regras,$mensagens);

        $categoria = Categoria::find($request->categoria);
        $categoria->catVidaUtil = $request->catVidaUtil;
        $categoria->update();

        $patrimonios = Patrimonio::where("categoria_catCodigo",$request->categoria)->get();
        foreach($patrimonios as $patrimonio){
            dispatch(new Depreciacao($patrimonio,$request->catVidaUtil,$request->depAno));
        }
        return  redirect()->route('depreciacao.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = Categoria::find($id);
        if($categoria != null){
            return response()->json(['categoria'=>$categoria]);
        }
        return response('Vazio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
