<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Funcao;

class FuncaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('administrador');
    }
    public function index()
    {
        return view('funcao.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //tratamento
        $regras =[
            'funNome' =>'required'
        ];
        $mensagens =[
            'funNome.required'=> 'Nome é requisito obrigatório'
        ];
        $request->validate($regras,$mensagens);
        
        $fun = new Funcao();
        $fun->funNome = $request->funNome;
        $fun->funDescricao = $request->funDescricao;
        $fun->save();
        
        return redirect()->route('funcao.index')-> withInput(['tab' =>'tab_2']) ;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fun = Funcao::find($id);
        if($fun != null){
            return response()->json(['funcao'=>$fun ]);
        }

        return response('Vazio'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fun = Funcao::find($id);
        if($fun == null){
            return redirect()
            ->back()
            ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
        }
        return view('funcao.edit',['funcao'=>$fun]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //verificando se existe
        $fun = Funcao::find($id);
        if($fun == null){
            return redirect()
            ->back()
            ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
        }
        //validando
        $regras =[
            'funNome' =>'required'
        ];
        $mensagens =[
            'funNome.required'=> 'Nome é requisito obrigatório'
        ];
        $request->validate($regras,$mensagens);

        $fun->funNome = $request->funNome;
        $fun->funDescricao = $request->funDescricao;
        $fun->update();

        return redirect()->route('funcao.index','#tab_2');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fun = Funcao::find($id);
        if($fun != null){
            $fun->delete();
            return response()->json('true');

        }
        return response()->json('false');
    }

    public function dataTableJson()
    {
        $funcoes = Funcao::where('funCodigo','>',1)->get();
        return response()->json(['data'=>$funcoes]);
    }
}
