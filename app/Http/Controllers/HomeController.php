<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Emprestimo;
use App\Categoria;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dataTableJson()
    {
        $dataAtual = date('Y-m-d');
        //atualizando o status dos emprestimos;
        $emprestimos = Emprestimo::all();
        foreach($emprestimos as $emp){
            if($emp->empDataDevolucao < $dataAtual){
                if($emp->empStatus != 'Devolvido'){
                    $emp->empStatus = 'Atrasado';
                    $emp->update();
                }
            }
        }
        return response()->json(['data'=>Emprestimo::where('empStatus','Atrasado')->with('fornecedor','user')->get()]);
    }

    public function getCategorias()
    {
        $cats = DB::table('itens_patrimonios as itp')
        ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
        ->join('categoria as cat','cat.catCodigo','pat.categoria_catCodigo')
        ->select(DB::raw('count(*)  as qtde,cat.catNome'))    
        ->groupBy(['cat.catNome'])
        ->get();

        
        return response()->json(['categorias'=>$cats]);
    }

    public function getUnidades()
    {
        $unis = DB::table('itens_patrimonios as itp')
        ->join('unidade as uni','uni.uniCodigo','itp.unidade_uniCodigo')
        ->select(DB::raw('count(*)  as qtde,uni.uniNome'))    
        ->groupBy(['uni.uniNome'])
        ->orderBy('qtde','desc')
        ->get();


        return response()->json(['unidades'=>$unis]);
    }
}
