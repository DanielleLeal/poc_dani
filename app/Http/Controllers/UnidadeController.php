<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unidade;
use App\Endereco;
use App\Estado;
use App\Cidade;
class UnidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }
    public function index()
    {
        return view('unidade.create',['estados'=>Estado::all()]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras= [
            'uniNome' => 'required',
            'endRua' => 'required',
            'endNumero' => 'required|integer',
            'estado_estCodigo' => 'required',
            'endBairro' => 'required',
            'endCEP'=>'nullable|min:9',
            'cidade_cidCodigo'=>'required',
       ];
       
       $mensagens = [
        'uniNome.required'=>'Nome é um requisito obrigatório',
        'endRua.required'=>'Rua é um requisito obrigatório',
        'endNumero.required'=>'Número é um requisito obrigatório',
        'endNumero.integer'=>'Entre só com números',
        'endBairro.required'=>'Bairro é um requisito obrigatório',
        'estado_estCodigo.required'=>'Estado é um requisito obrigatório',
        'endCEP.min'=>'Formato inválido.Ex.: 00000-000',
        'cidade_cidCodigo.required'=>'Cidade é um requisito obrigatório',

       ];
       $request->validate($regras,$mensagens);

       $endereco = new Endereco();
       $endereco->endRua = $request->endRua;
       $endereco->endNumero = $request->endNumero;
       $endereco->endBairro = $request->endBairro;
       $endereco->endCEP = $request->endCEP;
       $endereco->estado_estCodigo = $request->estado_estCodigo;
       $endereco->cidade_cidCodigo = $request->cidade_cidCodigo;
       $endereco->save();
       
       $unidade = new Unidade();

       $unidade->uniNome = $request->uniNome;
       $unidade->endereco_endCodigo = $endereco->endCodigo;
       $unidade->save();

       return redirect()->route('unidade.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unidade = Unidade::find($id);
        if($unidade != null){
            return response()->json(['unidade'=>$unidade ]);
        }

        return response('Vazio'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unidade = Unidade::find($id);
        if($unidade == null){
            return redirect()
            ->back()
            ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
        }
        return view('unidade.edit',
        [
            'unidade'=>$unidade,
            'estados'=>Estado::all(),
            'cidades'=>Cidade::where('estado_estCodigo',$unidade->endereco->estado_estCodigo)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unidade = Unidade::find($id);
        if($unidade != null){
            $regras= [
                'uniNome' => 'required',
                'endRua' => 'required',
                'endNumero' => 'required|integer',
                'estado_estCodigo' => 'required',
                'endBairro' => 'required',
                'endCEP'=>'nullable|min:9',
                'cidade_cidCodigo'=>'required',
           ];
           
           $mensagens = [
            'uniNome.required'=>'Nome é um requisito obrigatório',
            'endRua.required'=>'Rua é um requisito obrigatório',
            'endNumero.required'=>'Número é um requisito obrigatório',
            'endNumero.integer'=>'Entre só com números',
            'endBairro.required'=>'Bairro é um requisito obrigatório',
            'estado_estCodigo.required'=>'Estado é um requisito obrigatório',
            'endCEP.min'=>'Formato inválido.Ex.: 00000-000',
            'cidade_cidCodigo.required'=>'Cidade é um requisito obrigatório',
    
           ];
           $request->validate($regras,$mensagens);

            $endereco = Endereco::find($unidade->endereco_endCodigo);
            if($endereco != null){
                $endereco->endRua = $request->endRua;
                $endereco->endNumero = $request->endNumero;
                $endereco->endBairro = $request->endBairro;
                $endereco->endCEP = $request->endCEP;
                $endereco->estado_estCodigo = $request->estado_estCodigo;
                $endereco->cidade_cidCodigo = $request->cidade_cidCodigo;
                $endereco->update();

                $unidade->uniNome = $request->uniNome;
                $unidade->endereco_endCodigo = $endereco->endCodigo;
                $unidade->update();
            }else{
                return redirect()
                ->back()
                ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
            }
            

            return redirect()->route('unidade.index');
        }

        return redirect()
            ->back()
            ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unidade = Unidade::find($id);
        if($unidade != null){
            $end = Endereco::find($unidade->endereco_endCodigo);
            if($end != null){
                $unidade->delete();
                $end->delete();

                return response()->json('true');
            }
        }
        return response()->json('false');
    }

    public function dataTableJson(){

        return response()->json(['data'=>Unidade::with('endereco.cidade','endereco.estado')->get()]);
    }

    public function getCidades($id){
        
        $cidades = Cidade::where('estado_estCodigo',$id)->get();
        return response()->json(['cidades'=> $cidades,'tamanho'=>count($cidades)]);
    }
}
