<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datetime;
use App\Fornecedor;
use App\Unidade;
use App\Categoria;
use App\Emprestimo;
use App\ItensPatrimonio;
use App\Itens_Emprestimo;
use App\Patrimonio;
use Auth;

class EmprestimoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }
    public function index()
    {
        $fornecedores = Fornecedor::where('forStatus','Parceiro')->orderBy('forNome')->get();
        return view('emprestimo.create',
                    ['fornecedores'=>$fornecedores,
                    'unidades'=>Unidade::orderBy('uniNome')->get(),
                    'categorias'=>Categoria::orderBy('catNome')->get(),
                    'patrimonios'=>Patrimonio::orderBy('patNome')->get(),
                    'dataAtual'=>date('Y-m-d')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $regras = [
            'empNomeSolicitante'=>'required',
            'fornecedor_forCodigo'=>'required|integer|min:1',
            'empDataEmprestimo'=>'required',
            'empDataDevolucao'=>'required',
        ];

        $mensagens =[
            'empNomeSolicitante.required'=>'Nome do solicitante é um requisito obrigatório',
            'fornecedor_forCodigo.required' =>'Empresa é um requisito obrigatório',
            'fornecedor_forCodigo.min' =>'Selecione uma empresa válida',
            'empDataEmprestimo.required'=>'A data do empréstimo é um requisito obrigatório',
            'empDataDevolucao.required'=>'A data da devolução é um requisito obrigatório',
        ];
        $request->validate($regras,$mensagens);

        $empPatrimonios = $request->empPatrimonios[0];
        $pats = explode(',' ,$empPatrimonios );//transformando em um array, os itpCodigo advindos do input

        $empQuantidade = count($pats);

        $emprestimo = new Emprestimo();
        $emprestimo->empNomeSolicitante = $request->empNomeSolicitante;
        $emprestimo->empDataEmprestimo = $request->empDataEmprestimo;
        $emprestimo->empDataDevolucao = $request->empDataDevolucao;
        $emprestimo->empFinalidade = $request->empFinalidade;
        $emprestimo->fornecedor_forCodigo = $request->fornecedor_forCodigo;
        $emprestimo->users_useCodigo = Auth::user()->useCodigo;
        $emprestimo->empQuantidade = $empQuantidade;
        $emprestimo->empValorTotal =  $request->empValorTotal;
        $emprestimo->save();
        
        //preenchendo a tabela itens_emprestimo
        $emprestimo->itensPatrimonio()->attach($pats);
        if($request->gerarComp != null){
            $itens = Itens_Emprestimo::join('itens_patrimonios as itp','itp.itpCodigo','itensPatrimonio_itpCodigo')
                                    ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
                                    ->select(DB::raw('count(*)  as qtde,pat.patNome'))
                                    ->where('itens_emprestimo.emprestimo_empCodigo',$emprestimo->empCodigo)
                                    ->groupBy('pat.patNome')
                                    ->orderBy('pat.patNome')
                                    ->get();
            $emp = Emprestimo::where('empCodigo',$emprestimo->empCodigo)->with('fornecedor','user')->first();
            return redirect()->route('emprestimo.index')->with(['emprestimoComprovante'=>json_encode($emp),'itensComprovante'=>json_encode($itens)]);
        }
            return redirect()->route('emprestimo.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $emprestimo = Emprestimo::find($id);
        if($emprestimo != null){
            $emprestimo = Emprestimo::where('empCodigo',$id)->with('fornecedor')->first();
            return response()->json(['emprestimo'=>$emprestimo]);
        }

        return response('Vazio'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emprestimo = Emprestimo::find($id);
        if($emprestimo){
            $fornecedores = Fornecedor::where('forStatus','Parceiro')->orderBy('forNome')->get();
            return view('emprestimo.edit',
                        ['fornecedores'=>$fornecedores,
                        'unidades'=>Unidade::orderBy('uniNome')->get(),
                        'categorias'=>Categoria::orderBy('catNome')->get(),
                        'patrimonios'=>Patrimonio::orderBy('patNome')->get(),
                        'emprestimo'=>$emprestimo]);
                        
        }else{
            return redirect()->back();
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emprestimo = Emprestimo::find($id);
        if($emprestimo != null){
            $regras = [
                'empNomeSolicitante'=>'required',
                'fornecedor_forCodigo'=>'required|integer|min:1',
                'empDataEmprestimo'=>'required',
                'empDataDevolucao'=>'required',
                'empPatrimonios'=>'required',
                'empValorTotal'=>'required'
            ];
    
            $mensagens =[
                'empNomeSolicitante.required'=>'Nome do solicitante é um requisito obrigatório',
                'fornecedor_forCodigo.required' =>'Empresa é um requisito obrigatório',
                'fornecedor_forCodigo.min' =>'Selecione uma empresa válida',
                'empDataEmprestimo.required'=>'A data do empréstimo é um requisito obrigatório',
                'empDataDevolucao.required'=>'A data da devolução é um requisito obrigatório',
                'empPatrimonios'=>'Selecionar os patrimônios é um requisito obrigatório!',
                'empValorTotal'=>'Valor total é um requisito obrigatório',
    
            ];
            $request->validate($regras,$mensagens);
    
            $empPatrimonios = $request->empPatrimonios[0];
            $pats = explode(',' ,$empPatrimonios );
    
            $empQuantidade = count($pats);

            if($request->empDataDevolucao < date('Y-m-d')){
                $emprestimo->empStatus = 'Atrasado';
            }else{
                $emprestimo->empStatus = 'Pendente';
            }

            $emprestimo->empNomeSolicitante = $request->empNomeSolicitante;
            $emprestimo->empDataEmprestimo = $request->empDataEmprestimo;
            $emprestimo->empDataDevolucao = $request->empDataDevolucao;
            $emprestimo->empFinalidade = $request->empFinalidade;
            $emprestimo->fornecedor_forCodigo = $request->fornecedor_forCodigo;
            $emprestimo->users_useCodigo = Auth::user()->useCodigo;
            $emprestimo->empQuantidade = $empQuantidade;
            $emprestimo->empValorTotal =  $request->empValorTotal;
            $emprestimo->update();

            $itens_emp = Itens_Emprestimo::where('emprestimo_empCodigo',$id)->get();
            foreach($itens_emp as $item){
                $i = ItensPatrimonio::where('itpCodigo',$item->itensPatrimonio_itpCodigo)->first();
                $i->itpSituacao = 'Local';
                $i->update();
            }

            $emprestimo->itensPatrimonio()->detach();
            $emprestimo->itensPatrimonio()->attach($pats);
            
            return redirect()->route('emprestimo.index');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emprestimo = Emprestimo::find($id);
        if($emprestimo != null){
            $itens_emp = Itens_Emprestimo::where('emprestimo_empCodigo',$id)->get();
            foreach($itens_emp as $item){
                $i = ItensPatrimonio::where('itpCodigo',$item->itensPatrimonio_itpCodigo)->first();
                $i->itpSituacao = 'Local';
                $i->update();
            }
            $emprestimo->delete();
            return response()->json('true');

        }
        return response()->json('false');
    }

    //preencher datatable de emprestimo
    public function dataTableJson($empStatus = null,$empData = null)
    {
        $dataAtual = date('Y-m-d');
        //atualizando o status dos emprestimos;
        $emprestimos = Emprestimo::all();
        foreach($emprestimos as $emp){
            if($emp->empDataDevolucao < $dataAtual){
                if($emp->empStatus != 'Devolvido'){
                    $emp->empStatus = 'Atrasado';
                    $emp->update();
                }
            }
        }
        $data = Emprestimo::join('fornecedor as for','for.forCodigo','emprestimo.fornecedor_forCodigo')
                            ->join('users as use','use.useCodigo','emprestimo.users_useCodigo')
                            ->when($empStatus, function ($query, $empStatus) {
                                    return $query->where('emprestimo.empStatus', $empStatus);
                            })
                            ->when($empData, function ($query, $empData) {
                                return $query->where('emprestimo.empDataDevolucao', $empData);
                            })
                            ->orderBy('empDataDevolucao')
                            ->paginate(10);

        return response()->json(['emprestimo'=>$data]);
    }

   

    //preencher a tabela de patrimônios a serem emprestados - Usado no cadastro e edição
    public function getPatrimonios(Request $request,$uniCodigo =null, $status =null,$catCodigo =null,$patCodigo=null)
    {
        $patrimonios = DB::table('itens_patrimonios as itp')
                        ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
                        ->join('categoria as cat','cat.catCodigo','pat.categoria_catCodigo')
                        ->where(function($query)  {
                            $query->where('itp.itpSituacao','Local');
                        })
                        ->when($uniCodigo, function ($query, $uniCodigo) {
                            return $query->where('itp.unidade_uniCodigo', $uniCodigo);
                        })
                        ->when($status, function ($query, $status) {
                            return $query->where('itp.itpStatus', $status);
                        })
                        ->when($catCodigo, function ($query, $catCodigo) {
                            return $query->where('pat.categoria_catCodigo', $catCodigo);
                        })
                        ->when($patCodigo, function ($query, $patCodigo) {
                            return $query->where('itp.patrimonio_patCodigo', $patCodigo);
                        })
                        ->when($request->pesquisar, function ($query, $pesquisar) {
                            return $query->where('pat.patNome','like','%'.$pesquisar.'%')
                                        ->orWhere('itp.itpCodigoBarras','like','%'.$pesquisar.'%')
                                        ->orWhere('itp.itpValorAtual','like','%'.$pesquisar.'%');
                        })
                        ->orderBy('pat.patNome')
                        ->paginate(5);
        return response()->json(['patrimonios'=>$patrimonios]);
    }

    public function pesquisarListagem(Request $request)
    {
        $empStatus = $request->empStatus;
        $pesquisar = $request->pesquisar;
        $emprestimos = Emprestimo::join('fornecedor as for','for.forCodigo','emprestimo.fornecedor_forCodigo')
                                ->join('users as use','use.useCodigo','emprestimo.users_useCodigo')
                                ->when($empStatus, function ($query, $empStatus) {
                                    return $query->where('emprestimo.empStatus', $empStatus);
                                })
                                ->where(function($query)  use ($pesquisar){
                                    $query->where('use.useNome','like','%'.$pesquisar.'%')
                                            ->orWhere('for.forNome','like','%'.$pesquisar.'%');
                                })
                                ->paginate(10);
        return response()->json(['emprestimo'=>$emprestimos]);
    }

    //buscar item de patrimonio escolhido para o emprestimo - durante cadastro e edição
    public function adicionar($itpCodigo)
    {
        
        if($itpCodigo !=0){
            $item = DB::table('itens_patrimonios as itp')
                ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
                ->join('categoria as cat','cat.catCodigo','pat.categoria_catCodigo')
                ->where('itp.itpSituacao','Local')
                ->where('itp.itpCodigo',$itpCodigo)
                ->first();
            return response()->json(['item'=>$item]);
        }else{
            return response()->json(false);
        }
        
    }

    //preencher view detalhes
    public function verMais($empCodigo){
        $emprestimo = Emprestimo::find($empCodigo);
        if($emprestimo != null){
            $itens = Itens_Emprestimo::join('itens_patrimonios as itp','itp.itpCodigo','itens_emprestimo.itensPatrimonio_itpCodigo')
            ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
            ->where('emprestimo_empCodigo',$empCodigo)
            ->get();
            return view('emprestimo.detalhes',['emprestimo'=>$emprestimo,'itens'=>$itens]);
        }
        return redirect()->back();
    }

    //item removido da table de emprestado e vou  adicionar em selecionar patrimonio - EDITAR
    public function getItem($itpCodigo){
        $item = Itens_Emprestimo::join('itens_patrimonios as itp','itp.itpCodigo','itens_emprestimo.itensPatrimonio_itpCodigo')
            ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
            ->where('itens_emprestimo.itensPatrimonio_itpCodigo',$itpCodigo)->first();
        return response()->json(['item'=>$item]);
    }

    //preencher tabela de itens emprestados - EDITAR
    public function getItensEditar($empCodigo){
        $itens = Itens_Emprestimo::join('itens_patrimonios as itp','itp.itpCodigo','itens_emprestimo.itensPatrimonio_itpCodigo')
            ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
            ->where('itp.itpSituacao','Emprestado')
            ->where('emprestimo_empCodigo',$empCodigo)->get();
        $emprestimo = Emprestimo::find($empCodigo);
        return response()->json(['itens'=>$itens,'emprestimo'=>$emprestimo]);
    }

    //pegando o item atraves do código lido pelo leitor
    public function getItemLeitor($itpCodigoBarras){
        $item = ItensPatrimonio::where('itpCodigoBarras',$itpCodigoBarras)
                                ->where('itpSituacao','Local')
                                ->with('patrimonio')
                                ->first();
        if($item != null){
            return response()->json(['item'=>$item]);
        }else{
            return response()->json(false);
        }
        
    }
}
