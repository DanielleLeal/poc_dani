<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emprestimo;
use App\Fornecedor;
use App\Itens_Emprestimo;
use App\ItensPatrimonio;
use Datetime;

class DevolucaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('administrador');
    }
    public function index()
    {
        $dataAtual = date('Y-m-d');
        $mesAtual = substr($dataAtual, 5,2);
        $dataSemana = date('d/m/Y', strtotime('+7 days'));

        //meses para mostrar na view
        $meses = ['01'=>'Janeiro','02'=>'Fevereiro','03'=>'Março','04'=>'Abril','05'=>'Maio','06'=>'Junho',
        '07'=>'Julho','08'=>'Agosto','09'=>'Setembro','10'=>'Outubro','11'=>'Novembro','12'=>'Dezembro'];

        return view('emprestimo.listagem_devolucao',[
            'dataAtual'=>$dataAtual,
            'mesAtual'=>$mesAtual,
            'dataSemana'=>$dataSemana,
            'meses'=>$meses]);
    }
    public function devolucao()
    {
        $dataAtual = date('Y-m-d');
        $mesAtual = substr($dataAtual, 5,2);
        $dataSemana = date('d/m/Y', strtotime('+7 days'));

        //devoluções do dia atual
        $empDia = Emprestimo::where('empDataDevolucao',$dataAtual)
                            ->where(function($query){
                                $query->where('empStatus','Pendente')
                                        ->orWhere('empStatus','Atrasado');
                            })
                            ->with('fornecedor')->get();

        //pegar os empCodigo do dia para não serem listados nos da semana
        $empCodigo_dia = Emprestimo::where('empDataDevolucao',$dataAtual)->get(['empCodigo']);

        //devoluções da semana
        $empSemana = Emprestimo::whereBetween('empDataDevolucao',[date('Y/m/d'),date('Y/m/d', strtotime('+7 days'))])
                                ->whereNotIn('empCodigo',$empCodigo_dia)
                                ->where(function($query){
                                    $query->where('empStatus','Pendente')
                                            ->orWhere('empStatus','Atrasado');
                                })
                                ->with('fornecedor')
                                ->get();
        //pegar os empCodigo da semana para não serem listados nos do mês
        $empCodigo_semana = Emprestimo::whereBetween('empDataDevolucao',[date('Y/m/d'),date('Y/m/d', strtotime('+7 days'))])
                                ->get(['empCodigo']);
        //devoluções do mês
        $empMes = [];
        $emprestimos = Emprestimo::whereNotIn('empCodigo',$empCodigo_semana)
                                   ->whereNotIn('empCodigo',$empCodigo_dia)
                                   ->where(function($query){
                                        $query->where('empStatus','Pendente')
                                            ->orWhere('empStatus','Atrasado');
                                    })
                                   ->with('fornecedor')
                                   ->get();
        foreach ($emprestimos as $emp){
            $mes= substr($emp->empDataDevolucao, 5,2);  
            if($mes == $mesAtual){
                array_push($empMes,$emp);
            }
        };

        $empTodos = Emprestimo::with('fornecedor')
                                ->where('empStatus','<>','Devolvido')
                                ->orderBy('empDataEmprestimo')
                                ->get();

        return response()->json(['empDia'=> $empDia,'empSemana'=>$empSemana,
                                'empMes'=>$empMes,'empTodos'=>$empTodos]);

    }

    public function telaDevolver($empCodigo)
    {
        $emprestimo = Emprestimo::find($empCodigo);
        if($emprestimo != null){
            return view('emprestimo.devolucao',['emprestimo'=>$emprestimo]);
        }
        return redirect()->back();
    }
    public function dataTableJson($empCodigo)
    {
        $emprestimo = Emprestimo::find($empCodigo);
        if($emprestimo!= null){
            $itens = Itens_Emprestimo::join('itens_patrimonios as itp','itp.itpCodigo','itensPatrimonio_itpCodigo')
            ->join('patrimonio as pat','pat.patCodigo','itp.patrimonio_patCodigo')
            ->where('itp.itpSituacao','Emprestado')
            ->where('itens_emprestimo.emprestimo_empCodigo',$emprestimo->empCodigo)
            ->get(['itp.itpCodigo','pat.patNome','itp.itpValorAtual','itp.itpCodigoBarras']);

            return response()->json(['data'=>$itens]);
        }
            
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $itensDevolvidos = $request->itensDevolvidos;
        $itens = explode(',' ,$itensDevolvidos );//transformando em um array, os itpCodigo advindos do input
        foreach($itens as $it){
            $item = ItensPatrimonio::find($it);
            if($item !=null){
                $item->itpSituacao = 'Local';
                $item->update();
            }
        }
        $emprestimo = Emprestimo::find($request->empCodigo);
        if($emprestimo != null){
            $emprestimo->empStatus = 'Devolvido';
            $emprestimo->update();
        }
        
        return redirect()->route('emprestimo.indexD');
    }

    public function storeJson(Request $request)
    {
        $itensDevolvidos = $request->itensDevolvidos;
        $itens = explode(',' ,$itensDevolvidos );//transformando em um array, os itpCodigo advindos do input
        foreach($itens as $it){
            $item = ItensPatrimonio::find($it);
            if($item !=null){
                $item->itpSituacao = 'Local';
                $item->update();
            }
        }
        foreach( $request->itensNaoDevolvidos as $value){
            
            $i = ItensPatrimonio::find($value['itpCodigo']);
            if($item != null){
                if($value['itpStatus'] != null){
                    $i->itpStatus = $value['itpStatus'];
                    $i->update();
                }
            }
        }
        
        $emprestimo = Emprestimo::find($request->empCodigo);
        if($emprestimo != null){
            $emprestimo->empStatus = 'Pendente';
            $emprestimo->update();
        }
        return response()->json(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
