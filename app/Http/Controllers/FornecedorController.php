<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fornecedor;
use App\Estado;
use App\Cidade;
use App\Endereco;

class FornecedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }
    public function index()
    {
        return view('fornecedor.create',['estados'=>Estado::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras= [
            'forNome' => 'required',
            'forEmail' => 'required|email',
            'forCPF' => 'required|unique:fornecedor|cpf_cnpj|formato_cpf_cnpj',
            'forCelular' => 'required|min:16',
            'forTelefone'=> 'nullable|min:9',
            'forStatus' => 'required',
            'endRua' => 'required',
            'endNumero' => 'required|integer',
            'estado_estCodigo' => 'required',
            'endBairro' => 'required',
            'endCEP'=>'nullable|min:9',
            'cidade_cidCodigo'=>'required',
       ];
       
       $mensagens = [
        'forNome.required'=>'Nome é um requisito obrigatório',
        'forEmail.required'=>'Email é um requisito obrigatório',
        'forEmail.email'=>'Formato do e-mail inválido. Ex.: empresa@gmail.com',
        'forCelular.required'=>'Celular é um requisito obrigatório',
        'forCelular.min'=>'Celular deve conter no mínimo 16 caracteres',
        'forTelefone.min'=>'Telefone deve conter no mínimo 9 caracteres',
        'forCPF.required'=>'CPF/CNPJ é um requisito obrigatório',
        'forCPF.formato_cpf_cnpj'=>'Formato inválido do CPF/CNPJ',
        'forCPF.unique'=>'CPF/CNPJ já está sendo usado',
        'forCPF.cpf_cnpj'=> 'O CPF ou CNPJ não é válido',
        'forStatus.required'=>'Tipo é um requisito obrigatório',
        'endRua.required'=>'Rua é um requisito obrigatório',
        'endNumero.required'=>'Número é um requisito obrigatório',
        'endNumero.integer'=>'Entre só com números',
        'endBairro.required'=>'Bairro é um requisito obrigatório',
        'endCEP.min'=>'Formato inválido.Ex.: 00000-000',
        'estado_estCodigo.required'=>'Estado é um requisito obrigatório',
        'cidade_cidCodigo.required'=>'Cidade é um requisito obrigatório',

       ];
       $request->validate($regras,$mensagens);
       

       //Salvando o endereço
       $endereco = new Endereco();
       $endereco->endRua = $request->endRua;
       $endereco->endNumero = $request->endNumero;
       $endereco->endBairro = $request->endBairro;
       $endereco->endCEP = $request->endCEP;
       $endereco->estado_estCodigo = $request->estado_estCodigo;
       $endereco->cidade_cidCodigo = $request->cidade_cidCodigo;
       $endereco->save();
       
       $fornecedor = new Fornecedor();
       $fornecedor->forNome = $request->forNome;
       $fornecedor->forEmail = $request->forEmail;
       $fornecedor->forCelular = $request->forCelular;
       $fornecedor->forTelefone = $request->forTelefone;
       $fornecedor->forCPF = $request->forCPF;
       $fornecedor->forStatus = $request->forStatus;
       $fornecedor->endereco_endCodigo = $endereco->endCodigo;
       $fornecedor->save();

       return redirect()->route('fornecedor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fornecedor = Fornecedor::find($id);
        if($fornecedor != null){
            return response()->json(['fornecedor'=>$fornecedor ]);
        }

        return response('Vazio'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fornecedor = Fornecedor::find($id);
        if($fornecedor == null){
            return redirect()
            ->back()
            ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
        }
        return view('fornecedor.edit',
        [
            'fornecedor'=>$fornecedor,
            'estados'=>Estado::all(),
            'cidades'=>Cidade::where('estado_estCodigo',$fornecedor->endereco->estado_estCodigo)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fornecedor = Fornecedor::find($id);
        if($fornecedor != null){
            $regras= [
                'forNome' => 'required',
                'forEmail' => 'required|email',
                'forCPF' => 'required|unique:fornecedor,forCPF,'.$id.',forCodigo|formato_cpf_cnpj',
                'forCelular' => 'required|min:16',
                'forTelefone'=>'nullable|min:9',
                'forStatus' => 'required|string',
                'endRua' => 'required',
                'endNumero' => 'required|integer',
                'estado_estCodigo' => 'required|min:1',
                'endBairro' => 'required',
                'endCEP'=>'nullable|min:9',
                'cidade_cidCodigo'=>'required',
           ];
           
           $mensagens = [
            'forNome.required'=>'Nome é um requisito obrigatório',
            'forEmail.required'=>'Email é um requisito obrigatório',
            'forEmail.email'=>'Formato do e-mail inválido. Ex.: empresa@gmail.com',
            'forCelular.required'=>'Celular é um requisito obrigatório',
            'forCelular.min'=>'Celular deve conter no mínimo 16 caracteres',
            'forTelefone.min'=>'Telefone deve conter no mínimo 9 caracteres',
            'forCPF.required'=>'CPF/CNPJ é um requisito obrigatório',
            'forCPF.unique'=>'CPF/CNPJ já está sendo usado',
            'forCPF.formato_cpf_cnpj'=>'Formato inválido do CPF/CNPJ',
            'forStatus.required'=>'Tipo é um requisito obrigatório',
            'forStatus.string'=>'Selecione um tipo válido',
            'endRua.required'=>'Rua é um requisito obrigatório',
            'endNumero.required'=>'Número é um requisito obrigatório',
            'endNumero.integer'=>'Entre só com números',
            'endBairro.required'=>'Bairro é um requisito obrigatório',
            'estado_estCodigo.required'=>'Estado é um requisito obrigatório',
            'estado_estCodigo.min'=>'Selecione um estado válido',
            'endCEP.min'=>'Formato inválido.Ex.: 00000-000',
            'cidade_cidCodigo.required'=>'Cidade é um requisito obrigatório',
    
           ];
           $request->validate($regras,$mensagens);

            $endereco = Endereco::find($fornecedor->endereco_endCodigo);
            if($endereco != null){
                $endereco->endRua = $request->endRua;
                $endereco->endNumero = $request->endNumero;
                $endereco->endBairro = $request->endBairro;
                $endereco->endCEP = $request->endCEP;
                $endereco->estado_estCodigo = $request->estado_estCodigo;
                $endereco->cidade_cidCodigo = $request->cidade_cidCodigo;
                $endereco->update();
                
                
                $fornecedor->forNome = $request->forNome;
                $fornecedor->forEmail = $request->forEmail;
                $fornecedor->forCelular = $request->forCelular;
                $fornecedor->forTelefone = $request->forTelefone;
                $fornecedor->forCPF = $request->forCPF;
                $fornecedor->forStatus = $request->forStatus;
                $fornecedor->endereco_endCodigo = $endereco->endCodigo;
                $fornecedor->update();
            }else{
                return redirect()
                ->back()
                ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
            }
            

       return redirect()->route('fornecedor.index');
        }
        return redirect()
            ->back()
            ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fornecedor = Fornecedor::find($id);
        if($fornecedor != null){
            $end = Endereco::find($fornecedor->endereco_endCodigo);
            if($end != null){
                $fornecedor->delete();
                $end->delete();

                return response()->json('true');
            }
        }
        return response()->json('false');
    }

    public function dataTableJson()
    {
        return response()->json(['data'=>Fornecedor::with('endereco.cidade','endereco.estado')->get()]);
    }
    //pegar as cidades do estado escolhido
    public function getCidades($id)
    {
        $cidades = Cidade::where('estado_estCodigo',$id)->get();
        return response()->json(['cidades'=> $cidades,'tamanho'=>count($cidades)]);
    }

    public function verMais($forCodigo)
    {
        $fornecedor = Fornecedor::where('forCodigo',$forCodigo)->with('endereco')->first();
        if($fornecedor != null){
            return response()->json(['fornecedor'=>$fornecedor]);
        }

        return response()->json(false);
    }
}
