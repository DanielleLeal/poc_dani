<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotaFiscal;
use App\Patrimonio;
use App\Unidade;
use Illuminate\Support\Facades\Storage;

class NotaFiscalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }
    public function index()
    {
        return view('notaFiscal.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras = [
            'nofDescricao'=> 'required|max:100',
            'nofArquivo'=>'required|image|mimes:jpeg,png,jpg|max:2048'
        ];

        $mensagens = [
            'nofDescricao.required'=>'O campo descrição é um requisito obrigatório',
            'nofDescricao.max'=>'O máximo de caracteres permitido são 100',
            'nofArquivo.required'=>'O campo arquivo é um requisito obrigatório',
            'nofArquivo.image'=>'Arquivo deve ser uma imagem (Ex: jpeg,png,jpg)',
            'nofArquivo.mimes'=>'Arquivo deve ser uma imagem (Ex: jpeg,png,jpg)',
            'nofArquivo.max'=>'Arquivo excedeu o tamanho máximo aceitável',
        ];
        $request->validate($regras,$mensagens);
        
        //tratamento da imagem
        $path = $request->file('nofArquivo')->store('notaFiscal','public');
            if ( !$path ){
                return redirect()
                        ->back()
                        ->withSuccess('Falha ao fazer upload');
                }
        $nota = new NotaFiscal();
        $nota->nofDescricao = $request->nofDescricao;
        $nota->nofArquivo = $path;
        $nota->save();

        return \redirect()->route('notaFiscal.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nota = NotaFiscal::find($id);
        if($nota != null){
            return response()->json(['nota'=>$nota]);
        }

        return \response()->json('Vazio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nota = NotaFiscal::find($id);
        if($nota != null){
            return view('notaFiscal.edit',['nota'=>$nota]);
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nota = NotaFiscal::find($id);
        if($nota != null){
            $regras = [
                'nofDescricao'=> 'required|max:100',
                'nofArquivo'=>'nullable|image|mimes:jpeg,png,jpg|max:2048'
            ];
    
            $mensagens = [
                'nofDescricao.required'=>'O campo descrição é um requisito obrigatório',
                'nofDescricao.max'=>'O máximo de caracteres permitido são 100',
                'nofArquivo.image'=>'Arquivo deve ser uma imagem (jpeg,png,jpg)',
                'nofArquivo.mimes'=>'Arquivo deve ser uma imagem (jpeg,png,jpg)',
                'nofArquivo.max'=>'Arquivo excedeu o tamanho máximo aceitável',
            ];
            $request->validate($regras,$mensagens);
            
            if($request->nofArquivo != null){
                //apagando a foto da pasta
                $arquivo = $nota->nofArquivo;
                Storage::disk('public')->delete($arquivo);
                //tratamento da imagem
                $path = $request->file('nofArquivo')->store('notaFiscal','public');
                if ( !$path ){
                    return redirect()
                            ->back()
                            ->withSuccess('Falha ao fazer upload');
                }
                $nota->nofArquivo = $path;
            }
            
            $nota->nofDescricao = $request->nofDescricao;
            $nota->update();
    
            return redirect()->route('notaFiscal.index');
        }

        return redirect()->route();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nota = NotaFiscal::find($id);
        if($nota != null){
            $arquivo = $nota->nofArquivo;
            Storage::disk('public')->delete($arquivo);
            $nota->delete();

            return response()->json('true');
        }

        return response()->json('false');
    }

    public function dataTableJson(){
        return response()->json(['data'=>NotaFiscal::all()]);
    }

    public function cadastrarJS(Request $request){
        $regras = [
            'nofDescricao'=> 'required|max:100',
            'nofArquivo'=>'required|image|mimes:jpeg,png,jpg|max:2048'
        ];

        $mensagens = [
            'nofDescricao.required'=>'O campo descrição é um requisito obrigatório',
            'nofDescricao.max'=>'O máximo de caracteres permitido são 100',
            'nofArquivo.required'=>'O campo arquivo é um requisito obrigatório',
            'nofArquivo.image'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
            'nofArquivo.mimes'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
            'nofArquivo.max'=>'Foto excedeu o tamanho máximo aceitável',
        ];

        $validator = \Validator::make($request->all(),$regras,$mensagens);

        $errors = $validator->errors()->all();
        if($errors){
            return response()->json(['error'=>$validator->errors()]);
        }
        
        //tratamento da imagem
        $path = $request->file('nofArquivo')->store('notaFiscal','public');
            if ( !$path ){
                return response()
                        ->json(['upload'=>false]);
                }
        $nota = new NotaFiscal();
        $nota->nofDescricao = $request->nofDescricao;
        $nota->nofArquivo = $path;
        $nota->save();
        $notas = NotaFiscal::orderBy('nofDescricao')->get();
        $tam = count($notas);
        return response()->json(['notas'=>$notas,'nota'=>$nota,
                                'tamanho'=>$tam,'error'=>false,'upload'=>true]);

    }

    //visualizar arquivo
    public function verNF($id){
        $nota_fiscal = NotaFiscal::find($id);
        if($nota_fiscal != null){
            return response()->json(['notaFiscal'=>$nota_fiscal]);
        }
         return response()->json('Vazio');   
        
    }
}
