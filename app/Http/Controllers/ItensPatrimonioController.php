<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItensPatrimonio;
use App\NotaFiscal;
use App\Patrimonio;
use App\Unidade;
use App\Autorizacao;
use App\Exports\ItensPatrimonioExport;
use Maatwebsite\Excel\Facades\Excel;
use DNS1D;

class ItensPatrimonioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('itensPatrimonio.create',
        ['patrimonios'=>Patrimonio::orderBy('patNome')->get(),
        'notasFiscais'=>NotaFiscal::orderBy('nofDescricao')->get(),
        'unidades'=>Unidade::orderBy('uniNome')->get(),]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras = [ 
            'patrimonio_patCodigo'=>'required',
            'itpTipo'=>'required',
            'itpSituacao'=>'required',
            'itpStatus'=>'required',
            'unidade_uniCodigo'=>'required|integer',
            'nota_fiscal_nofCodigo'=>'required|integer',
            'quantidade'=>'required',
        ];

        $mensagens =[
            'patrimonio_patCodigo.required'=>'Patrimônio é requisito obrigatório',
            'patrimonio_patCodigo.min'=>'Selecione um patrimônio válido',
            'itpTipo.required'=>'Tipo é requisito obrigatório',
            'itpStatus.required'=>'Status é requisito obrigatório',
            'itpSituacao.required'=>'Situação é requisito obrigatório',
            'quantidade.required'=>'Quantidade é um requisito obrigatório',
            'quantidade.min'=>'No mínimo 1',
            'nota_fiscal_nofCodigo.required'=>'Nota Fiscal é requisito obrigatório',
            'nota_fiscal_nofCodigo.min'=>'Selecione uma nota fiscal válida',
            'unidade_uniCodigo.required'=>'Unidade é requisito obrigatório',
            'unidade_uniCodigo.min'=>'Selecione uma unidade válida',
        ];

        $validator = \Validator::make($request->all(),$regras,$mensagens);

        $errors = $validator->errors()->all();
        if($errors){
            return response()->json(['error'=>$validator->errors()]);
        }

        $svgs = array();
        $request->validate($regras,$mensagens);
        $quantidade = $request->quantidade;
        $patrimonio = Patrimonio::find($request->patrimonio_patCodigo);
            if($quantidade >= 1){
                for($i =0;$i<$quantidade;$i++){
                    $item = new ItensPatrimonio();
                    $item->itpTipo = $request->itpTipo;
                    $item->itpStatus = $request->itpStatus;
                    $item->itpSituacao = $request->itpSituacao;
                    $item->unidade_uniCodigo = $request->unidade_uniCodigo;
                    $item->nota_fiscal_nofCodigo = $request->nota_fiscal_nofCodigo;
                    $item->patrimonio_patCodigo = $request->patrimonio_patCodigo;
                    $item->itpValorAtual = $patrimonio->patValorCompra;
                    $item->save();
                    $codigo = 'ABC'.$item->itpCodigo;
                    $item->itpCodigoBarras = $codigo;
                    $item->update();
                    if($request->gerarCodigo != null){
                        $codigo = 'ABC'.$item->itpCodigo;
                        $item->itpCodigoBarras = $codigo;
                        $item->update();
                        $svg = ['codigoB'=>"data:image/png;base64,".DNS1D::getBarcodePNG($codigo, 'C39+', 3 , 40 ),
                                'nome'=>$patrimonio->patNome,'codigo'=>$codigo];
                        array_push($svgs,$svg);
                    }
                }
                
            }
            $patrimonio->patQuantidade = $patrimonio->patQuantidade + $quantidade;
            $patrimonio->update();
            
            if($request->gerarCodigo != null){
                return response()->json(['gerou'=>true,'svgs'=>$svgs,'error'=>false]);
            }else{
                return response()->json(['gerou'=>false,'error'=>false]);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = ItensPatrimonio::find($id);
        if($item != null){
            return response()->json(['item'=>$item]);
        }
        return response('Vazio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = ItensPatrimonio::find($id);
        if($item != null){
            $patrimonio = Patrimonio::where('patCodigo',$item->patrimonio_patCodigo)->first();
        
            return view('itensPatrimonio.edit',
            ['item'=>$item,
            'unidades'=>Unidade::orderBy('uniNome')->get(),
            'notasFiscais'=>NotaFiscal::orderBy('nofDescricao')->get(),
            'patrimonio'=>$patrimonio
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = ItensPatrimonio::find($id);
        if($item != null){
            $regras = [
                'itpTipo'=>'required|string',
                'itpSituacao'=>'required|string',
                'itpStatus'=>'required|string',
                'unidade_uniCodigo'=>'required|integer|min:1',
                'nota_fiscal_nofCodigo'=>'required|integer|min:1',
                'itpValorAtual'=>'required|min:1|integer',
            ];
    
            $mensagens =[
                'itpTipo.required'=>'Tipo é requisito obrigatório',
                'itpTipo.string'=>'Selecione um tipo válido',
                'itpStatus.required'=>'Status é requisito obrigatório',
                'itpStatus.string'=>'Selecione um status válido',
                'itpSituacao.required'=>'Situação é requisito obrigatório',
                'itpSituacao.string'=>'Selecione uma situação válida',
                'nota_fiscal_nofCodigo.required'=>'Nota Fiscal é um requisito obrigatório',
                'nota_fiscal_nofCodigo.min'=>'Selecione uma nota fiscal válida',
                'unidade_uniCodigo.required'=>'Unidade é um requisito obrigatório',
                'unidade_uniCodigo.min'=>'Selecione uma unidade válida',
                'itpValorAtual.required' =>'Valor atual é um requisito obrigatório',
                'itpValorAtual.min' =>'O valor atual deve ser no mínimo 1',
                'itpValorAtual.integer' =>'O valor atual deve ser número'
            ];
            $request->validate($regras,$mensagens);
            $item->itpTipo = $request->itpTipo;
            $item->itpStatus = $request->itpStatus;
            $item->itpSituacao = $request->itpSituacao;
            $item->unidade_uniCodigo = $request->unidade_uniCodigo;
            $item->nota_fiscal_nofCodigo = $request->nota_fiscal_nofCodigo;
            $item->itpValorAtual = $request->itpValorAtual;
            $item->patrimonio_patCodigo = $request->patrimonio_patCodigo;
            $item->update();

            $patCodigo =$request->patrimonio_patCodigo;

            return \redirect()->route('patrimonio.itensPatrimonio',$patCodigo);
        }

        return redirect()
                ->back()
                ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ItensPatrimonio::find($id);
        if($item != null){
            $item->delete();
            return response()->json('true');
        }

        return response()->json('false');
    }

    public function getNF($id){
        $nota_fiscal = NotaFiscal::find($id);
        if($nota_fiscal != null){
            return response()->json(['notaFiscal'=>$nota_fiscal]);
        }
         return response()->json('Vazio');   
        
    }    
    //filtro e preenchimento da tabela
    public function getJson($patCodigo,$tipo =null,$valor =null){
         if($tipo == 'tipo'){
                $itens = ItensPatrimonio::with('unidade')->where('patrimonio_patCodigo',$patCodigo)
                                                        ->where('itpTipo',$valor)
                                                        ->orderBy('itpCodigo')
                                                        ->paginate(15);
                return response()->json(['itens'=>$itens]);
    
            }else if($tipo =='unidade'){
                $itens = ItensPatrimonio::with('unidade')->where('patrimonio_patCodigo',$patCodigo)
                                                        ->where('unidade_uniCodigo',$valor)
                                                        ->orderBy('itpCodigo')
                                                        ->paginate(15);
                return response()->json(['itens'=>$itens]);
    
            }else if($tipo=='situacao'){
                $itens = ItensPatrimonio::with('unidade')->where('patrimonio_patCodigo',$patCodigo)
                                                        ->where('itpSituacao',$valor)
                                                        ->orderBy('itpCodigo')
                                                        ->paginate(15);
                return response()->json(['itens'=>$itens]);
    
            }else if($tipo=='status'){
                $itens = ItensPatrimonio::with('unidade')->where('patrimonio_patCodigo',$patCodigo)
                                                        ->where('itpStatus',$valor)
                                                        ->orderBy('itpCodigo')
                                                        ->paginate(15);
                return response()->json(['itens'=>$itens]);
            }else if($tipo == 'todos'){
                $itens = ItensPatrimonio::with('unidade')->where('patrimonio_patCodigo',$patCodigo)
                                                        ->orderBy('itpCodigo')
                                                        ->paginate(15);
                return response()->json(['itens'=>$itens]);
            }else{
                $itens = ItensPatrimonio::with('unidade')->where('patrimonio_patCodigo',$patCodigo)
                                                        ->orderBy('itpCodigo')
                                                        ->paginate(15);
                return response()->json(['itens'=>$itens]);
            }
        }
    
    //preenchimento dos filtros    
    public function preencherFiltro($tipo){
        if($tipo == 'unidade'){
            $unidades = Unidade::all();
            return response()->json(['unidades'=>$unidades]);
        }else if($tipo == 'status'){
            $status = array(['nome'=>'Ativo'],
                            ['nome'=>'Inativo'],
                            ['nome'=>'Manutenção'],
                            ['nome'=>'Estragado']);
            return response()->json(['status'=>$status]);
        }else if($tipo == 'situacao'){
            $situacao = array(['nome'=>'Local'],
                            ['nome'=>'Emprestado']);
            return response()->json(['situacao'=>$situacao]);
        }else if($tipo == 'tipo'){
            $tipo = array(['nome'=>'Comprado'],
                            ['nome'=>'Doado']);
            return response()->json(['tipo'=>$tipo]);
        }
    }

    //metodo de pesquisa instântanea
    public function pesquisa(Request $request){
        $itens = ItensPatrimonio::where('itpCodigoBarras','like','%'.$request->pesquisar.'%')
                                ->orWhere('itpStatus','like','%'.$request->pesquisar.'%')
                                ->orWhere('itpSituacao','like','%'.$request->pesquisar.'%')
                                ->orWhere('itpTipo','like','%'.$request->pesquisar.'%')
                                ->orWhere('itpValorAtual','like','%'.$request->pesquisar.'%')
                                ->with('unidade')
                                ->orderBy('itpCodigo')
                                ->paginate(15);
        return response()->json(['itens'=>$itens]);
    }

    //metodo de gerar código de barras
    public function gerarCodigoBarras($itpCodigo){
        $item = ItensPatrimonio::find($itpCodigo);
        $svg = "data:image/png;base64,".DNS1D::getBarcodePNG($item->itpCodigoBarras, 'C39', 3 , 40 );
        $item = ItensPatrimonio::where('itpCodigo',$itpCodigo)->with('patrimonio')->first();
        return response()->json(['svg'=>$svg,'item'=>$item]);
    }

    public function export($patCodigo) 
    {
        return Excel::download(new ItensPatrimonioExport($patCodigo), 'itens_patrimonio.xlsx');
    }

}
