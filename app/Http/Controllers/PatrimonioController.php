<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patrimonio;
use App\Categoria;
use App\Fornecedor;
use App\Unidade;
use App\NotaFiscal;
use App\ItensPatrimonio;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Exports\PatrimoniosExport;
use Auth;
class PatrimonioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('multiple');
    }
    
    public function index()
    { 
            return view('patrimonio.create',
            [
            'categorias'=>Categoria::orderBy('catNome')->get(),
            'fornecedores'=> Fornecedor::orderBy('forNome')->get(),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patrimonio.create',
            [
                'categorias'=>Categoria::all(),
                'fornecedores'=> Fornecedor::all(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras =[
            'patNome' =>'required',
            'patFoto'=>'image|mimes:jpeg,png,jpg|max:2048',
            'categoria_catCodigo'=>'required|integer|min:1',
            'fornecedor_forCodigo'=>'required|integer|min:1',
            'patValorCompra'=>'required',
            
        ];
        $mensagens =[
            'patNome.required'=> 'Nome é requisito obrigatório',
            'patFoto.image'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
            'patFoto.mimes'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
            'patFoto.max'=>'Foto excedeu o tamanho máximo aceitável',
            'categoria_catCodigo.required'=> 'Categoria é requisito obrigatório',
            'categoria_catCodigo.min'=> 'Selecione uma categoria válida',
            'fornecedor_forCodigo.required'=>'Fornecedor é requisito obrigatório',
            'fornecedor_forCodigo.min'=>'Selecione um fornecedor válido',
            'patValorCompra.required'=>'Valor de compra é requisito obrigatório',
        ];

        $request->validate($regras,$mensagens);
        

        $path;
        
       //tratamento da imagem
       if($request->patFoto != null){
            $path = $request->file('patFoto')->store('patrimonios','public');
            if ( !$path ){
                return redirect()
                            ->back()
                            ->withSuccess('Falha ao fazer upload');
            }
       }else{
           $path  = 'imagens/default-patrimonio.png';
       }

        $user = Auth::user();
        if($user != null){

            $patrimonio = new Patrimonio();
            $patrimonio->patNome = $request->patNome;
            $patrimonio->categoria_catCodigo = $request->categoria_catCodigo;
            $patrimonio->fornecedor_forCodigo = $request->fornecedor_forCodigo;
            $patrimonio->users_useCodigo = $user->useCodigo;
            $patrimonio->patQuantidade = 0;
            /*trocando vírgula por ponto*/
            $request->patValorCompra = str_replace(".","", $request->patValorCompra);
            $request->patValorCompra = str_replace(",",".", $request->patValorCompra); 
            /* transformando a string em número */
            $patrimonio->patValorCompra = (float)$request->patValorCompra;
            $patrimonio->patFoto = $path;
            $patrimonio->save();

            return redirect()->route('patrimonio.index');
            
            
        }else{
            return view('auth.login');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patrimonio = Patrimonio::find($id);
        if($patrimonio != null){
            return response()->json(['patrimonio'=>$patrimonio]);
        }

        return response('Vazio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patrimonio = Patrimonio::find($id);
        if($patrimonio != null){

            return view('patrimonio.edit',
            [
                'patrimonio'=>$patrimonio,
                'categorias'=>Categoria::orderBy('catNome')->get(),
                'fornecedores'=> Fornecedor::orderBy('forNome')->get(),
                ]);                 
        }
        return redirect()->back();
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patrimonio = Patrimonio::find($id);
        if($patrimonio != null){
            $regras =[
                'patNome' =>'required',
                'patFoto'=>'image|mimes:jpeg,png,jpg|max:2048',
                'categoria_catCodigo'=>'required|integer|min:1',
                'patValorCompra'=>'required',
                'fornecedor_forCodigo'=>'required|integer|min:1',
                
            ];
            $mensagens =[
                'patNome.required'=> 'Nome é requisito obrigatório',
                'patFoto.image'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
                'patFoto.mimes'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
                'patFoto.max'=>'Foto excedeu o tamanho máximo aceitável',
                'categoria_catCodigo.required'=> 'Categoria é requisito obrigatório',
                'categoria_catCodigo.min'=> 'Selecione uma categoria válida',
                'fornecedor_forCodigo.required'=>'Fornecedor é requisito obrigatório',
                'fornecedor_forCodigo.min'=>'Selecione um fornecedor válido',
                'patValorCompra.required'=>'Valor de compra é requisito obrigatório',
                
            ];

            $request->validate($regras,$mensagens);

            $path;
            //tratando a imagem
           if($request->file('patFoto') != null){
                //apagando a foto da pasta
                $arquivo = $patrimonio->patFoto;
                Storage::disk('public')->delete($arquivo);
                //salvando a nova foto
                $path = $request->file('patFoto')->store('patrimonios','public');
                if ( !$path ){
                    return redirect()
                                ->back()
                                ->withSuccess('Falha ao fazer upload');
                }
           }else if($request->tirar_foto != null){ // se a pessoa tirar a foto
                //apagando a foto da pasta
                $arquivo = $patrimonio->patFoto;
                Storage::disk('public')->delete($arquivo);
                $path = 'imagens/default-patrimonio.png';
           }else{
               $path = $patrimonio->patFoto;
           }

            $user = Auth::user();
            $patrimonio->patNome = $request->patNome;
            $patrimonio->categoria_catCodigo = $request->categoria_catCodigo;
            $patrimonio->fornecedor_forCodigo = $request->fornecedor_forCodigo;
            $patrimonio->users_useCodigo = $user->useCodigo;
            
            /*trocando vírgula por ponto*/
            $request->patValorCompra = str_replace(".","", $request->patValorCompra);
            $request->patValorCompra = str_replace(",",".", $request->patValorCompra); 

            /* transformando a string em número */
            $patrimonio->patValorCompra = (float)$request->patValorCompra;
            $patrimonio->patValorCompra = $request->patValorCompra;
            $patrimonio->patFoto = $path;
            $patrimonio->update();
            return redirect()->route('patrimonio.index');
                
        }
        return redirect()
                ->back()
                ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patrimonio = Patrimonio::find($id);
        if($patrimonio != null){
            if($patrimonio->patFoto != null){
                //apagando a foto da pasta
            $arquivo = $patrimonio->patFoto;
            Storage::disk('public')->delete($arquivo);
            }
            $patrimonio->delete();
            return response()->json('true');
        }
        return response()->json('false');
    }

    //pegando todos os patrimonios
    public function getPatrimonios($catCodigo = null){
        $patrimonios = Patrimonio::when($catCodigo, function ($query, $catCodigo) {
                return $query->where('categoria_catCodigo', $catCodigo);
        })->with('categoria','fornecedor')->orderBy('patNome')->paginate(10);
            
        return response()->json(['patrimonios'=>$patrimonios]);   
    }

    //pegando todos os itens de patrimonio do patrimonio escolhido
    public function getItensPatrimonio($patCodigo){
        $patrimonio = Patrimonio::find($patCodigo);   
        $unidades = Unidade::orderBy('uniNome')->get();
        $notasFiscais =NotaFiscal::orderBy('nofDescricao')->get();                             
        return view('itensPatrimonio.index',
            ['patCodigo'=>$patCodigo,
             'patrimonio'=> $patrimonio,
             'unidades'=>$unidades,
             'notasFiscais'=>$notasFiscais]);
    }

    //metodo para pesquisa instântanea
    public function pesquisa(Request $request){
        $patrimonios = Patrimonio::where('patNome','like','%'.$request->pesquisar.'%')
                                ->orWhere('patCodigo','like','%'.$request->pesquisar.'%')
                                ->orWhere('patQuantidade','like','%'.$request->pesquisar.'%')
                                ->orWhere('patValorCompra','like','%'.$request->pesquisar.'%')
                                ->with('categoria','fornecedor')
                                ->orderBy('patNome')
                                ->paginate(10);
        return response()->json(['patrimonios'=>$patrimonios]);
    }

    public function export() 
    {
        return Excel::download(new PatrimoniosExport, 'patrimonios.xlsx');
    }
}
