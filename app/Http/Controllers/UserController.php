<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Funcao;
use App\User;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth');
        $this->middleware('administrador', ['except' => ['perfil','editarPerfil','upload']]);
    }
    public function index()
    {
        return view('users.create',['funcoes'=>Funcao::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $regras= [
            'useNome' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8',
            'useCelular' => 'required|min:16',
            'useCPF' => 'nullable|unique:users|cpf|formato_cpf',
            'useFoto'=>'image|mimes:jpeg,png,jpg|max:2048',
            'funcao_funCodigo'=>'required|integer|min:1',
       ];
       
       $mensagens = [
           'useNome.required'=>'Nome é um requisito obrigatório',
           'email.required'=>'Email é um requisito obrigatório',
           'email.unique' =>'Esse email já está sendo usado',
           'password.required'=>'Senha é um requisito obrigatório',
           'password.min'=>'Sua senha deve conter no mínimo 8 caracteres',
           'useCelular.required'=>'Celular é um requisito obrigatório',
           'useCelular.min'=>'Seu número deve conter no mínimo 16 caracteres',
           'useCPF.formato_cpf'=>'Formato inválido do CPF',
           'useCPF.unique'=> 'Esse CPF já está sendo usado',
           'useCPF.cpf'=>'Esse CPF não é válido',
           'useFoto.image'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
           'useFoto.mimes'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
           'useFoto.max'=>'Foto excedeu o tamanho máximo aceitável',
           'funcao_funCodigo.min'=> 'Selecione uma função válida',
       ];
       $request->validate($regras,$mensagens);
       
       $path;
       //tratamento da imagem
       if($request->useFoto != null){
            $path = $request->file('useFoto')->store('imagensUsers','public');
            if ( !$path ){
                return redirect()
                            ->back()
                            ->withSuccess('Falha ao fazer upload');
            }
       }else{
           $path  = 'imagens/default-user.png';
       }
       
       $user = new User();
       $user->useNome = $request->useNome;
       $user->email = $request->email;
       $user->password = bcrypt($request->password);
       $user->useCPF = $request->useCPF;
       $user->useCelular = $request->useCelular;
       $user->useFoto = $path;
       $user->funcao_funCodigo = $request->funcao_funCodigo;
       $user->save();

       return redirect()->route('user.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $useCodigo = $id;
        $user = User::find($useCodigo);
        if($user != null){
            return response()->json(['user'=>$user ]);
        }

        return response('Vazio'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $funcoes = Funcao::all();
        if($user == null){
            return redirect()->back();
        }
        return view('users.edit',
            ['user'=>$user,
            'funcoes'=>$funcoes
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($user != null){
            $regras= [
                'useNome' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email,'.$id.',useCodigo',
                'password' => 'nullable|min:8',
                'useCelular' => 'required|min:16',
                'useCPF' => 'nullable|unique:users,useCPF,'.$id.',useCodigo|cpf|formato_cpf',
                'useFoto'=>'image|mimes:jpeg,png,jpg|max:2048',
                'funcao_funCodigo'=>'required|integer|min:1',
           ];
           
           $mensagens = [
               'useNome.required'=>'Nome é um requisito obrigatório',
               'email.required'=>'Email é um requisito obrigatório',
               'email.unique' =>'Esse email já está sendo usado',
               'password.min'=>'Sua senha deve conter no mínimo 8 caracteres',
               'useCelular.required'=>'Celular é um requisito obrigatório',
               'useCelular.min'=>'Seu número deve conter no mínimo 16 caracteres',
               'useCPF.formato_cpf'=>'Formato inválido do CPF',
               'useCPF.unique'=> 'Esse CPF já está sendo usado',
               'useCPF.cpf'=>'Esse CPF não é válido',
               'useFoto.image'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
               'useFoto.mimes'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
               'useFoto.max'=>'Foto excedeu o tamanho máximo aceitável',
               'funcao_funCodigo.min'=> 'Selecione uma função válida',
           ];
           $request->validate($regras,$mensagens);

           //tratando a senha
           if($request->password != null){
            if(strlen($request->password) < 8){
                return redirect()
                            ->back()
                            ->withSuccess('Sua nova senha deve ter no mínimo 8 caracteres');
            }
                $user->password = bcrypt($request->password);
            }

            $path;
            //tratando a imagem
           if($request->file('useFoto') != null){ // se a pessoa trocar de foto
            //apagando a foto da pasta
            $arquivo = $user->useFoto;
            Storage::disk('public')->delete($arquivo);
            //salvando a nova foto
            $path = $request->file('useFoto')->store('imagensUsers','public');
                if ( !$path ){
                    return redirect()
                                ->back()
                                ->withSuccess('Falha ao fazer upload');
                }
            }else if($request->tirar_foto != null){ // se a pessoa tirar a foto
                //apagando a foto da pasta
                $arquivo = $user->useFoto;
                Storage::disk('public')->delete($arquivo);
                $path = 'imagens/default-user.png';
            }else{ // se a pessoa não modificar
                $path = $user->useFoto;
            }

            
            $user->useNome = $request->useNome;
            $user->email = $request->email;
            $user->useCPF = $request->useCPF;
            $user->useCelular = $request->useCelular;
            $user->useFoto = $path;
            $user->funcao_funCodigo = $request->funcao_funCodigo;
            $user->update();

            return redirect()->route('user.index');
        }

        return redirect()
                ->back()
                ->withSuccess('Não foi possível atualizar o cadastro, tente mais tarde!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $useCodigo = $id;
        $user = User::find($useCodigo);
        if($user != null){
            //apagando a foto da pasta
            $arquivo = $user->useFoto;
            Storage::disk('public')->delete($arquivo);

            $user->delete();
            return response()->json('true');

        }
        return response()->json('false');
    }

    //preecher datatable
    public function dataTableJson()
    {
        $logado = Auth::user();
        $users = User::with('funcao')->where('useCodigo','<>',$logado->useCodigo)->get();
        return response()->json(['data'=>$users]);
    }

    public function perfil($id)
    {
        $user = User::find($id);
        if($user != null){
            return view('users.perfil',['user'=>$user]);
        }
        return redirect()
                ->back()
                ->withSuccess('Não foi possível visualizar seu perfil, tente mais tarde!');

    }

    public function editarPerfil(Request $request)
    {
        $json = $request->all();
        $campo = $json['campo'];
        $valor = $json['valor'];
        $user = Auth::user();

        if($campo == 'email'){
            if(strlen($valor) >0){
                $user->$campo =$valor;
                $user->update();
                $mensagem = 'O campo EMAIL foi atualizado com sucesso!';
                return response()->json(['acao'=>true,'user'=>$user,'mensagem'=>$mensagem]);
            }else{
                $mensagem= 'O campo EMAIL não pode ser nulo ou foi preenchido de maneira errada';
               return response()->json(['user'=>$user,'mensagem'=>$mensagem]);
            }
        }else if($campo == 'useNome'){
             if(strlen($valor) >0){
                $user->$campo =$valor;
                $user->update();
                $mensagem = 'O campo NOME foi atualizado com sucesso! ';
                return response()->json(['acao'=>true,'user'=>$user,'mensagem'=>$mensagem]);
             }else{
                 $mensagem= 'O campo nome não pode ser nulo';
                return response()->json(['user'=>$user,'mensagem'=>$mensagem]);
             }
         }else if($campo == 'useCPF'){
             if(strlen($valor) == 14){
                $user->$campo =$valor;
                $user->update();
                $mensagem ='Campo CPF foi atualizado com sucesso!';
                return response()->json(['acao'=>true,'mensagem'=>$mensagem,'user'=>$user]);
             }else{
                 $mensagem = 'O campo CPF não pode ser nulo e tem que ter no minímo 14 caracteres.Não atualizado!';
                return response()->json(['user'=>$user,'mensagem'=>$mensagem]);
             }
         }else if($campo == 'useCelular'){
            if(strlen($valor) == 16){
                $user->$campo =$valor;
                $user->update();
                $mensagem = 'Campo CELULAR foi atualizado com sucesso!';
                return response()->json(['acao'=>true,'mensagem'=>$mensagem,'user'=>$user]);
             }else{
                 $mensagem = 'O campo CELULAR não pode ser nulo e tem que ter no minímo 16 caracteres.Não atualizado!';
                return response()->json(['user'=>$user,'mensagem'=>$mensagem,'user'=>$user]);
             }
         }else if($campo == 'password'){
            if(strlen($valor) > 8){
                $user->$campo = bcrypt($valor);
                $user->update();
                $mensagem = 'Campo SENHA foi atualizado com sucesso!';
                return response()->json(['acao'=>true,'mensagem'=>$mensagem,'user'=>$user]);
            }else{
                $mensagem = 'A sua NOVA SENHA não pode ser nula e tem que ter no minímo 8 caracteres.Não atualizado!';
                return response()->json(['user'=>$user,'mensagem'=>$mensagem]);
                
            }
         }
    }

    //salvando a imagem quando for atualizada na tela Meu perfil
    public function upload(Request $request)
    {
        $regras = [
            'useFoto'=>'image|mimes:jpeg,png,jpg|max:2048'
        ];
        $mensagens = [
            'useFoto.image'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
            'useFoto.mimes'=>'Foto deve ser uma imagem (jpeg,png,jpg)',
            'useFoto.max'=>'Foto excedeu o tamanho máximo aceitável',
        ];
        $request->validate($regras,$mensagens);

        $user = Auth::user();
        $path;
        //tratando a imagem
        if($request->file('useFoto') != null){
            //apagando a foto da pasta
            $arquivo = $user->useFoto;
            Storage::disk('public')->delete($arquivo);
            //salvando a nova foto
            $path = $request->file('useFoto')->store('imagensUsers','public');
            if ( !$path ){
                return redirect()
                        ->back()
                        ->withSuccess('Falha ao fazer upload');
            }
       }else{
           $path = $user->useFoto;
       }

       $user->useFoto = $path;
       $user->update();
       return redirect()->route('user.perfil',$user->useCodigo);
    }
}
