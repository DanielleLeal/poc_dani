<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class FuncaoAdmMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     //Middleware criado para barrar rotas de usuários que não são administradores
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if($user->funcao->funNome == 'Administrador'){
            return $next($request);
        }
        return redirect()->route('home')->withSuccess('Sua função não lhe permite acessar essa rota!');
        
    }
}
