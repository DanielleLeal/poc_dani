<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emprestimo extends Model
{
    protected $table = "emprestimo";

    protected $primaryKey = "empCodigo";

    protected $fillable = [
        'empNomeSolicitante','empDataEmprestimo', 'empDataDevolucao', 'empQuantidade','empFinalidade',
        'users_useCodigo','fornecedor_forCodigo',
    ];
    public function user(){
        return $this->belongsTo('App\User','users_useCodigo','useCodigo');
    }
    public function fornecedor(){
        return $this->belongsTo('App\Fornecedor','fornecedor_forCodigo','forCodigo');
    }

    public function itensPatrimonio()
    {
        return $this->belongsToMany(Emprestimo::class,'itens_emprestimo','emprestimo_empCodigo','itensPatrimonio_itpCodigo');
    }

    
}

