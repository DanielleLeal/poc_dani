<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "categoria";

    protected $primaryKey = "catCodigo";
    
    protected $fillable = [
        'catNome', 'catDescricao','catVidaUtil', 
    ];

    public function patrimonio(){
        return $this->hasMany('App\Patrimonio','categoria_catCodigo','catCodigo');
    }
}
