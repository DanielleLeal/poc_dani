<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $table = "historico";

    protected $primaryKey = "hisCodigo";

    protected $fillable = [
        'hisCodigo', 'hisAno','hisValor','itensPatrimonio_itpCodigo', 
    ];

    public function item_patrimonio()
    {
        return $this->belongsTo('App\Itens_Patrimonio','itensPatrimonio_itpCodigo','itpCodigo');
    }
    
}
