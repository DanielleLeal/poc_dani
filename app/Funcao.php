<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcao extends Model
{
    protected $table = "funcao";

    protected $primaryKey = "funCodigo";

    protected $fillable = [
        'funNome', 'funDescricao', 
    ];

    public function user(){
        return $this->hasMany('App\User','funcao_funCodigo','funCodigo');
    }
}
