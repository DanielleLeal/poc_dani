<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Tatame do Bem - Login</title>

    <!-- Icone no title -->
	<link rel="shortcut icon" href="{{ asset('imagens/tatame3.png') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
</head>
<body>
    <div class="container-fluid cont">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-center p-4">
                            <img src="{{asset('imagens/tatame3.png')}}" height="100" width="100">
                        </div>
                        <div class="row justify-content-center ">
                            <div class="col-md-9">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <div class="label-float">
                                            <input type="email" id="email" placeholder=" " class="form-control @error('email') is-invalid @enderror"
                                             name="email" value="{{ old('email') }}" autocomplete="off" required/>
                                            <label>{{ __('E-mail')}}</label>
                                        </div>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>     
                                    <div class="form-group">
                                        <div class="label-float">
                                            <input type="password" id="password" placeholder=" " class="form-control @error('password') is-invalid @enderror"
                                             name="password" required autocomplete="off"/>
                                            <label>{{ __('Senha')}}</label>
                                        </div>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check float-right mb-2">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                {{ __('Lembre-me') }}
                                            </label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                    <div class="d-flex justify-content-center">       
                                        <button type="submit" class="btnLogin fourth">
                                            {{ __('Login') }}
                                        </button>
                                    </div> 
                                    <div class="text-center pt-5 pb-4">
                                        @if (Route::has('password.request'))
                                            <a class="link" href="{{ route('password.request') }}">
                                                {{ __('Esqueceu sua senha?') }}
                                            </a>
                                        @endif  
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>




