@extends('layouts.master') @section('title', '- Usuário')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i> Editar Usuário </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="box-body box-success">
                <form action="{{ route('user.update',$user->useCodigo) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-2 text-center  text-sm-left">
                                @if($user->useFoto == 'imagens/default-user.png')
                                    <img src="{{asset('imagens/default-user.png')}}" class="img-circle user-image"
                                     id="img" height="120" width="120" alt="Imagem padrão de usuário">
								@else	
                                    <img src="/storage/{{$user->useFoto}}" alt="" class="img-circle" height="120" width="120" id="img">
								@endif
                                <label for="useFoto"><i class="fa fa-camera"></i></label>
                                <input type="file" name="useFoto" id="useFoto" style="display:none;">
                                <div class="checkbox">
                                    <label for="tirar_foto">
                                     <input type="checkbox" id="tirar_foto" name="tirar_foto" onchange="tirarFoto(this)">
                                     <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        Retirar foto
                                     </label>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="useNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="useNome" name="useNome" value="{{$user->useNome}}">
                                    @if($errors->get('useNome'))
                                        @foreach($errors->get('useNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="email" name="email" id="email" value="{{$user->email}}" 
                                    class="form-control"placeholder="Ex: administrador@gmail.com" >
                                    @if($errors->get('email'))
                                        @foreach($errors->get('email') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
									<label for="password">Nova senha</label>
									<div class="inputGroupContainer">
										<div class="input-group">
                                            <input type="password" id="password" name="password" class="form-control ccformfieldtarefas" 
                                            placeholder="Digite sua nova senha">
                                            <span class="input-group-addon" >
                                                <button type="button" id="olho"><i class="fa fa-eye"></i></button>
                                            </span>
                                        </div>
                                        @if($errors->get('password'))
                                            @foreach($errors->get('password') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach 
                                        @endIf
                                        @if (Session::has('success'))
                                            <span class="text-danger">{{Session::get('success')}}</span>
                                        @endif
									</div>
								</div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="useCelular">Celular <span class="text-danger">*</span></label>
                                    <input type="text" name="useCelular" id="useCelular"placeholder="Ex: (00) 0 0000-0000" class="form-control" value="{{$user->useCelular}}" >
                                    @if($errors->get('useCelular'))
                                        @foreach($errors->get('useCelular') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="useCPF">CPF</label>
                                    <input type="text" name="useCPF" id="useCPF" class="form-control" 
                                    value="{{$user->useCPF}}" placeholder="Ex: 000.000.000-00">
                                    @if($errors->get('useCPF'))
                                        @foreach($errors->get('useCPF') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="useFoto">Função <span class="text-danger">*</span></label>
                                    <select name="funcao_funCodigo" id="funcao_funCodigo" class="form-control">
                                        <option value="0">Selecione</option>
                                        @foreach ($funcoes as $funcao)
                                            <option value="{{$funcao->funCodigo}}"
                                                @if($user->funcao_funCodigo == $funcao->funCodigo )
                                                selected
                                                @endif
                                            >
                                                {{$funcao->funNome}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->get('funcao_funCodigo'))
                                        @foreach($errors->get('funcao_funCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('user.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 1-->  
    </div>
</div>
 
<!--Modal aviso sobre a foto-->
@component('components.aviso')
@endcomponent
<!--Modal -->

<script type="text/javascript"> 
    // Mascara para os campos
    $('#useCPF').mask("000.000.000-00");
	$("#useCelular").mask("(00) 0 0000-0000");

    //Configuração para o usuário visualizar a senha digitada
	$("#olho").mousedown(function() { // quando pressiona
			$('#password').attr("type", "text");
	});

    $("#olho").mouseup(function() { // quando solta
		$('#password').attr("type", "password");
	});
	$( "#olho" ).mouseout(function() { 
		$("#senha").attr("type", "password");
	});

    //preview da imagem antes do upload
    $('#useFoto').change(function(){
        const file = $(this)[0].files[0];// acessando o nome do arquivo selecionado
        const fileReader = new FileReader();
        fileReader.onloadend = function(){ // lendo o arquivo
            if(file.type.match('image.*')){
                $('#img').attr('src',fileReader.result);
            }else{
                $('#aviso').modal('show');
            }
        }
        fileReader.readAsDataURL(file); // lendo o arquivo
        
    });

    function tirarFoto(obj){
        if(obj.checked)
        $('#img').attr('src','{{asset("imagens/default-user.png")}}');
    }

</script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
