@extends('layouts.master')
@section('title', '- Perfil Usuário')
@section('principal') @parent
<!-- Estilo CSS -->
<link rel="stylesheet" href="{{asset('css/perfil.css')}}">
<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<br>
<br>

<!-- Modal de alerta para atualização correta --> 
<div class="modal" tabindex="-1" role="dialog" id="modalAtualizado">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="text-sucess">Atualizado</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="mensagem_2"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<div class="nav-tabs-custom">
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3">
                        @if($user->useFoto == 'imagens/default-user.png')
                        <img src="{{asset('imagens/default-user.png')}}" class="img-circle user-image" id="img" height="150" width="150" alt="Imagem padrão de usuário">
                        @else
                            <img src="/storage/{{$user->useFoto}}" class="img-circle user-image" id="img" height="150" width="150">
                        @endif
                        <label for="useFoto"><i class="fa fa-camera"></i></label>
                        <form action="{{route('user.uploadFoto')}}" method="POST" id="formImg" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="useFoto" id="useFoto" style="display:none;" onfocus="habilitar('useFoto')" onchange="enviarFoto()">
                            @if($errors->get('useFoto'))
                                @foreach($errors->get('useFoto') as $error)
                                    <span class="text-danger">{{ $error }}</span><br>
                                @endForeach
                            @endIf
                            @if (Session::has('success'))
                                <span class="text-danger">{{Session::get('success')}}</span>
                            @endif
                        </form>
                    </div>
                    <div class="col-lg-6">
                        <h1>Perfil do Usuário</h1>
                        <hr class="linha">
                        <h3>Dados de acesso</h3>
                        <div class="groups">
                            <span class="title"><img src="{{asset('imagens/iconeEmail.ico')}}" width="20"> E-mail</span>
                            <input type="email" name="email" id="email" value="{{$user->email}}"readonly onfocus="habilitar('email')" onchange="editar('email',this.value)">
                            <br><hr class="separa">
                            <span class="title"><i class="fa fa-lock" style="font-size:20px"></i> Senha</span>
                            <input type="password" name="password" id="password" placeholder="Digite sua nova senha"
                             readonly onfocus="habilitar('password')" onchange="editar('password',this.value)">
                        </div>
                        <h3>Dados Pessoais</h3>
                        <div class="groups">
                            <span class="title"><i class="fa fa-user" style="font-size:20px"></i> Nome</span>
                            <input type="text" name="useNome" id="useNome" value="{{$user->useNome}}" readonly onfocus="habilitar('useNome')" onchange="editar('useNome',this.value)">
                            <br><hr class="separa">
                            <span class="title"><img src="{{asset('imagens/iconeCPF.ico')}}" width="20"> CPF</span>
                            <input type="text" name="useCPF" id="useCPF" value="{{$user->useCPF}}" readonly onfocus="habilitar('useCPF')" onchange="editar('useCPF',this.value)">
                            <br><hr class="separa">
                            <span class="title"><img src="{{asset('imagens/iconeCelular.ico')}}"width="22"> Celular</span>
                            <input type="text" name="useCelular" id="useCelular" value="{{$user->useCelular}}" readonly onfocus="habilitar('useCelular')"onchange="editar('useCelular',this.value)">
                        </div>
                        <h3>Função</h3>
                        <div class="groups">
                            <div class="row">
                              <div class="col-lg-4">
                                <span class="title"><img src="{{asset('imagens/iconeAdm.ico')}}"style="font-size:18px">
                                  {{$user->funcao->funNome}}
                              </span>
                              </div>
                              <div class="col-lg-6">
                                <textarea rows="4" title="{{$user->funcao->funDescricao}}"  name="funDescricao" id="funDescricao" readonly style="resize:none"
                                  onfocus="habilitar('funDescricao')">{{$user->funcao->funDescricao}}</textarea>
                                  <input type="hidden" name="funcao" id="funcao" value="{{$user->funcao->funNome}}">
                              </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal de alerta para erro de preenchimentos -->    
<div class="modal" tabindex="-1" role="dialog" id="modalAlerta">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Alerta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="mensagem"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
<br>

<script>
    var baseurl = window.location.protocol + '//' + window.location.host + '/';

    // Mascara para os campos
    $('#useCPF').mask("000.000.000-00");
    $("#useCelular").mask("(00) 0 0000-0000");
    
    //preview da imagem antes do upload
    $('#useFoto').change(function(){
        const file = $(this)[0].files[0];// acessando o nome do arquivo selecionado
        const fileReader = new FileReader();
        fileReader.onloadend = function(){ // lendo o arquivo
            $('#img').attr('src',fileReader.result);
        }
        fileReader.readAsDataURL(file); // lendo o arquivo
    }); 

    function habilitar(id){
        if( id !='funDescricao'){
            $('#'+id).attr('readonly', false);
        }else if( id != 'password'){
            $('#'+id).attr('readonly', true);
            abrirAlerta('Não pode mudar sua função');
        }   
    }

    function editar(id,valor){
        $.ajax({
				type: 'POST',
                url: baseurl + 'user/editar',
                data:{'campo': id,'valor': valor},
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta.acao == true){
                            $('#'+id).attr('readonly', true);
                            abrirModal(resposta.mensagem);
                        }else{
                            abrirAlerta(resposta.mensagem);
                            email.value = resposta.user.email;
                            password.value = '';
                            useNome.value = resposta.user.useNome;
                            useCelular.value = resposta.user.useCelular;
                            useCPF.value = resposta.user.useCPF;
                        }
                        
					}
            });
    }
     function abrirAlerta(texto){
         mensagem.innerHTML = texto;
         $('#modalAlerta').modal('show');
     }

     function abrirModal(texto){
        mensagem_2.innerHTML = texto;
         $('#modalAtualizado').modal('show');
     }

     function enviarFoto(){
         formImg.submit();
     }

</script>


@endsection