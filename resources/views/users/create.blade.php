@extends('layouts.master') @section('title', '- Usuários')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<h3 style="margin-left:10px;"><i class="fa fa-user"></i> Usuários </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Usuários</a></li>
        <li id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                                <div class="table-responsive">
                                    <table class="table table-hover " id="user-table">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>E-mail </th>
                                                <th>CPF</th>
                                                <th>Celular</th>
                                                <th>Função</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane " id="tab_2">
            <div class="box-body box-success">
                <form action="{{ route('user.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-2 text-center  text-sm-left">
								<img src="{{asset('imagens/default-user.png')}}" class="img-circle user-image" id="img" height="120" width="120" alt="Imagem padrão de usuário">
                                <label for="useFoto"><i class="fa fa-camera"></i></label>
                                <input type="file" name="useFoto" id="useFoto" style="display:none;">
                                @if($errors->get('useFoto'))
                                    @foreach($errors->get('useFoto') as $error)
                                        <span class="text-danger">{{ $error }}</span><br>
                                    @endForeach
                                @endIf
                                @if (Session::has('success'))
                                    <span class="text-danger">{{Session::get('success')}}</span>
                                @endif
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="useNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="useNome" name="useNome" placeholder="Digite o nome">
                                    @if($errors->get('useNome'))
                                        @foreach($errors->get('useNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="email" name="email" id="email" placeholder="Ex: administrador@gmail.com" class="form-control">
                                    @if($errors->get('email'))
                                        @foreach($errors->get('email') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
									<label for="password">Senha<span class="text-danger">*</span></label>
									<div class="inputGroupContainer">
										<div class="input-group">
                                            <input type="password" id="password" name="password" value="" class="form-control ccformfieldtarefas" placeholder="Digite sua senha">
                                            <span class="input-group-addon" ><button type="button" id="olho"><i class="fa fa-eye"></i></button></span>
                                        </div>
                                        @if($errors->get('password'))
                                            @foreach($errors->get('password') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach 
                                        @endIf
									</div>
								</div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="useCelular">Celular <span class="text-danger">*</span></label>
                                    <input type="text" name="useCelular" id="useCelular" class="form-control" placeholder="Ex: (00) 0 0000-0000">
                                    @if($errors->get('useCelular'))
                                        @foreach($errors->get('useCelular') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="useCPF">CPF</label>
                                    <input type="text" name="useCPF" id="useCPF" class="form-control" placeholder="Ex: 000.000.000-00">
                                    @if($errors->get('useCPF'))
                                        @foreach($errors->get('useCPF') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="funcao_funCodigo">Função <span class="text-danger">*</span></label>
                                    <select name="funcao_funCodigo" id="funcao_funCodigo" class="form-control">
                                        <option value="0">Selecione</option>
                                        @foreach ($funcoes as $funcao)
                                            <option value="{{$funcao->funCodigo}}">
                                                {{$funcao->funNome}}
                                            </option>
                                        @endforeach
                                        @if (Session::has('success'))
                                            <span class="text-danger">{{Session::get('success')}}</span>
                                        @endif
                                    </select>
                                    @if($errors->get('funcao_funCodigo'))
                                        @foreach($errors->get('funcao_funCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('user.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 2-->   
    </div>
</div>
<br>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão -->  
<!-- Controle das tabs -->
@if(count($errors)>0)
    <script type="text/javascript">
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
    </script>
@else
<script type="text/javascript">
    $('#tab_1').addClass('active');
    $('#li_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#li_2').removeClass('active');
</script>
@endif
<!-- Controle das tabs -->

<!--Modal aviso sobre a foto-->
@component('components.aviso')
@endcomponent
<!--Modal -->  

<script type="text/javascript"> 
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    
    // Mascara para os campos
    $('#useCPF').mask("000.000.000-00");
	$("#useCelular").mask("(00) 0 0000-0000");

    //Configuração para o usuário visualizar a senha digitada
	$("#olho").mousedown(function() { // quando pressiona
			$('#password').attr("type", "text");
	});

    $("#olho").mouseup(function() { // quando solta
		$('#password').attr("type", "password");
	});
	$( "#olho" ).mouseout(function() { 
		$("#senha").attr("type", "password");
	});
    jQuery(function() {
            jQuery('#user-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("user.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                        data: 'useNome',
                        name: 'Nome'
                    },
                    {
                        data: 'email',
                        name: 'E-mail'
                    },
                    {
                        data: 'useCPF',
                        name: 'CPF'
                    },
                    {
                        data: 'useCelular',
                        name: 'Celular'
                    },
                    {
                        data: 'funcao.funNome',
                        name: 'Função'
                    },
                    {
                        data: "useCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="user/' + data + '/edit" class="btn btn-primary btn-block">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-6">';
                            html += '<p><button type="button" class="btn btn-danger btn-block" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2,3,4] 
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2,3,4] 
                        }
                    },
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var useCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'user/'+useCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.user.useCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir o usuário <strong>"+ resposta.user.useNome+"</strong>?";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
        }  
            
        //mandando pro metodo destroy
        function funcaoExcluir(){
            var useCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "user/"+useCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload('#tab_2');
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
        }

        $('#useFoto').change(function(){
            const file = $(this)[0].files[0];// acessando o nome do arquivo selecionado
            const fileReader = new FileReader();
            fileReader.onloadend = function(){ // lendo o arquivo
                if(file.type.match('image.*')){
                    $('#img').attr('src',fileReader.result);
                }else{
                    $('#aviso').modal('show');
                }
            }
            fileReader.readAsDataURL(file); // lendo o arquivo
            
        });
    </script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
