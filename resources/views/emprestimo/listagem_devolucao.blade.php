@extends('layouts.master')
@section('title', '- Devolução')
@section('emprestimoAtivo','bg-danger')
@section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="{{asset('css/devolucao.css')}}">


<h3 style="margin-left:10px;"><i class="fa fa-check"></i>Realizar Devolução</h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Realizar Devolução </a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col lg-12">
                        <div class="pull-left" style="margin-left: 18px;">
                            <label for="visu">Visualizar</label>
                            <select name="visu" id="visu" class="select" onchange="visualizar(this.value)">
                                <option value="Todas">Todas</option>
                                <option value="Dia" selected>Hoje</option>
                                <option value="Mes">Este Mês</option>
                                <option value="Semana">Esta Semana</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                        <div id="dDia" class="show">
                            <h4 class="title text-center" style="width: 150px;">DIA: {{date_create($dataAtual)->format('d/m/Y')}}</h4>
                            <div class="col-lg-8">
                                <ul class="list-group" style="margin-left:5px;margin-bottom:0px;" id="dia">
                                </ul>
                                <div class="pull-right" id="paginationD">
                                    <ul class="pagination hide" id="paginationD">
                                        <li class="page-item">
                                            <a id="anteriorDia" style="cursor:pointer;" class="page-link">Anterior</a>
                                        </li>
                                        <li class="page-item">
                                            <a id="proximoDia"style="cursor:pointer;" class="page-link">Próximo</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pull-left titlePagination" id="mostrarD">
                                </div>
                            </div>
                        </div>
                        <div id="todos" class="hide">
                            <h4 class="title text-center" style="width: 130px;">Todas</h4>
                            <div class="col-lg-8">
                                <ul class="list-group" style="margin-left:5px;margin-bottom:0px;" id="liTodos">
                                </ul>
                                <div class="pull-right" id="paginationT" style="display:none;">
                                    <ul class="pagination hide" id="paginationT">
                                        <li class="page-item">
                                            <a id="anteriorTodos" style="cursor:pointer;" class="page-link">Anterior</a>
                                        </li>
                                        <li class="page-item">
                                            <a id="proximoTodos"style="cursor:pointer;" class="page-link">Próximo</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pull-left titlePagination" id="mostrarT"style="display:none;">
                                </div>
                            </div>
                        </div>
                </div>
                <div class="row hide" id="dSemana">
                    <div class="col lg-12">
                        <h4 class="title" style="width: 300px;">SEMANA:{{date_create($dataAtual)->format('d/m/Y')}} a {{$dataSemana}}</h4>
                        <div class="col-lg-8">
                            <ul class="list-group" style="margin-left:5px;margin-bottom:0px;" id="semana">
                            </ul>
                            <div class="pull-right hide" id="paginationS">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a id="anteriorS" style="cursor:pointer;" class="page-link">Anterior</a>
                                    </li>
                                    <li class="page-item">
                                        <a id="proximoS"style="cursor:pointer;" class="page-link">Próximo</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-left titlePagination" id="mostrarS">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hide" id="dMes">
                    <div class="col lg-12">
                        <h4 class="title" style="width: 150px">MÊS:{{$meses[$mesAtual]}}</h4>
                        <div class="col-lg-8">
                            <ul class="list-group " style="margin-left:5px;margin-bottom:0px;" id="mes">
                            </ul>
                            <div class="pull-right hide" id="paginationM">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a id="anteriorM" style="cursor:pointer;" class="page-link">Anterior</a>
                                    </li>
                                    <li class="page-item">
                                        <a id="proximoM"style="cursor:pointer;" class="page-link">Próximo</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-left titlePagination" id="mostrarM">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Aba 1-->
    </div>
</div>
<br>
    <script type="text/javascript" src="{{ asset('js/listagem_devolucao.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
