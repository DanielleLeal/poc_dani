@extends('layouts.master') @section('title', '- Empréstimo')@section('emprestimoAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Estilo para tabela -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-building"></i> Empréstimos </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Realizados</a></li>
        <li  id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pull-left" style="margin-left: 10px;">
                            <label for="empStatus">Status</label>
                            <select name="empStatus" id="empStatus" class="select" onchange="datatableFiltro(this.value)">
                                <option value="" selected>Todos</option>
                                <option value="Atrasado">Atrasado</option>
                                <option value="Devolvido">Devolvido</option>
                                <option value="Pendente">Pendente</option>
                            </select>
                        </div>
                        <div class="pull-right">
                            <label for="empPesquisa">Pesquisar</label>
                            <input type="search" id="empPesquisa" name="empPesquisa">
                        </div>
                        <div class="pull-right">
                            <label for="data">Data</label>
                            <input style="line-height: 1.42857143;"  onblur="filtroData(this.value)" class="select data" type="date" id="data" name="empDataDevolucao">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12" >
                        <div class="table-responsive">
                            <table class="table table-hover" id="emprestimo-table">
                                <thead>
                                    <tr>
                                        <th>Data da Devolução</th>
                                        <th>Status</th>
                                        <th>Solicitante</th>
                                        <th>Responsável</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>
                                <tbody ></tbody>
                            </table>
                            <div> 
                                <span id="mostrarNum"></span>
                                <span class="pull-right">
                                    <nav id="paginationEmp">
                                        <ul class="pagination">
                                        </ul>
                                    </nav>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane" id="tab_2">
            <div class="box-body box-success">
                <form action="{{ route('emprestimo.store') }}" method="POST" id="formEmp">
                    @csrf
                    <div class="container-fluid show" id="primeira">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="empNomeSolicitante">Nome do Solicitante <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="empNomeSolicitante" name="empNomeSolicitante" placeholder="Nome">
                                        @if($errors->get('empNomeSolicitante'))
                                            @foreach($errors->get('empNomeSolicitante') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                    <div id="empNome_error"></div>
                                </div>
                                <div class="form-group">
                                    <label for="fornecedor_forCodigo">Empresa <span class="text-danger">*</span></label>
                                        <select name="fornecedor_forCodigo" id="fornecedor_forCodigo" style="width: 100%;" class="form-control">
                                            <option value="0">Selecione</option>
                                             @foreach ($fornecedores as $fornecedor)
                                            <option value="{{$fornecedor->forCodigo}}">
                                                {{$fornecedor->forNome}}
                                            </option>
                                             @endforeach   
                                        </select>
                                    <div id="fornecedor_error"></div>
                                </div>
                                <div class="form-group">
                                    <label for="empFinalidade">Finalidade do uso </label>
                                    <textarea name="empFinalidade" id="empFinalidade"  rows="5" class="form-control" placeholder="Breve descrição sobre a finalidade do uso dos patrimônios que serão emprestados"></textarea>
                                    @if($errors->get('empFinalidade'))
                                        @foreach($errors->get('empFinalidade') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="empDataEmprestimo">Data do Empréstimo <span class="text-danger">*</span></label>
                                    <input type="date" name="empDataEmprestimo" id="empDataEmprestimo" class="form-control" value="{{$dataAtual}}" placeholder="dd/mm/YYYY">
                                        @if($errors->get('empDataEmprestimo'))
                                            @foreach($errors->get('empDataEmprestimo') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                        <div id="dataEmp_error"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="empDataDevolucao">Data do Devolução <span class="text-danger">*</span></label>
                                        <input type="date" name="empDataDevolucao" id="empDataDevolucao" class="form-control" placeholder="dd/mm/YYYY">
                                        @if($errors->get('empDataDevolucao'))
                                            @foreach($errors->get('empDataDevolucao') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                        <div id="dataDev_error"></div>
                                    </div>
                                    <div class="form-group">
                                    <div class="form-group">
                                        <label for="users_useCodigo">Responsável</label>
                                        <input type="text" readonly class="form-control" id="users_useCodigo" name="users_useCodigo" value="{{\Auth::user()->useNome}}">
                                        @if($errors->get('users_useCodigo'))
                                            @foreach($errors->get('users_useCodigo') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                        <div class="pull-right">
                                            <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                            <div class="form-group" style="margin-top:15px;">
                                                <button type="button" onclick="proximaEtapa()" class="btn btn-primary pull-right btn-block">Próximo</button>
                                            </div>
                                        </div><br>                                        
                                    </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- ****************************Segunda etapa - SEM Leitor de código ***************************** -->
                    <div class="container-fluid hide" id="segunda">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title" style=" margin-left:15px;">Selecione os patrimônios:</h4>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="patrimonio">Patrimônio</label>
                                        <select style="width: 100%;" name="patrimonio" id="patrimonio" class="form-control"onchange="filtrar('patrimonio',this.value)">
                                            <option value="0" selected>Todos</option>
                                            @foreach ($patrimonios as $patrimonio)
                                            <option value="{{$patrimonio->patCodigo}}">
                                                {{$patrimonio->patNome}}
                                            </option>
                                            @endforeach   
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select name="status" id="status" class="form-control" onchange="filtrar('status',this.value)">
                                            <option value="0" selected>Todos</option>
                                            <option value="Ativo">Ativo</option>
                                            <option value="Inativo">Inativo</option>
                                            <option value="Manutenção">Manutenção</option>
                                            <option value="Estragado">Estragado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="categoria">Categoria</label>
                                        <select style="width: 100%;" name="categoria" id="categoria" class="form-control"onchange="filtrar('categoria',this.value)">
                                            <option value="0" selected>Todas</option>
                                            @foreach ($categorias as $categoria)
                                            <option value="{{$categoria->catCodigo}}">
                                                {{$categoria->catNome}}
                                            </option>
                                            @endforeach   
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="unidade">Unidade</label>
                                        <select style="width: 100%;" name="unidade" id="unidade" class="form-control" onchange="filtrar('unidade',this.value)">
                                            <option value="0" selected>Todas</option>
                                            @foreach ($unidades as $uni)
                                                <option value="{{$uni->uniCodigo}}">
                                                    {{$uni->uniNome}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Para selecionar os patrimonios --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right" style="margin-right:15px;">
                                    <label for="pesquisar">Pesquisar</label>
                                    <input type="search" name="pesquisar" id="pesquisar">
                                </div>
                                <div class="pull-right" style="margin-right:15px;">
                                    <label for="pesquisarL">Adicionar</label>
                                    <input type="search" name="pesquisarL" id="pesquisarL">
                                </div>
                                <div class="form-group pull-left" style="margin-right:10px;">
                                    <div class="checkbox">
                                        <label for="ch">
                                         <input type="checkbox" id="ch" name="ch" onclick="selecionarTodos()">
                                         <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                         Selecionar Todos
                                         </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12" style="margin-top:20px;">
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="table-emprestimo">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th>Código de Barras</th>
                                                    <th>Nome</th>
                                                    <th>Valor Atual</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <div> 
                                            <span id="card-title"></span>
                                                <span class="pull-right">
                                                    <nav id="paginationNav">
                                                        <ul class="pagination">
                                                        </ul>
                                                    </nav>
                                            </span>
                                            <div class="clearfix"></div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- patrimonios selecionados pelo usuario --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title" style="margin-top:10px; margin-left:15px;">Patrimônios selecionados</h4>
                                <div class="col-lg-12" style="margin-top:20px;">
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="tabelaEmprestado">
                                            <thead>
                                                <tr>
                                                    <th>Código de Barras</th>
                                                    <th>Nome</th>
                                                    <th>Valor Atual</th>
                                                    <th>Opção</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyEmprestado">
                                                <tr>
                                                    <td class="text-center" colspan="4">Não possui nenhum item de patrimônio selecionado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="pull-right" id="pagination" style="display: none;"> 
                                            <ul class="pagination">
                                                <li class="page-item">
                                                    <a id="anterior" style="cursor:pointer;" class="page-link">Anterior</a>
                                                </li>
                                                <li class="page-item">
                                                    <a id="proximo"style="cursor:pointer;" class="page-link">Próximo</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="pull-left titlePagination" id="mostrarPaginas">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h4 class="title-emp pull-left">Valor Total: R$</h4><h4 style="display: inline;font-size: 18px;" id="valorRee"></h4><br>
                                    
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>     
                        <div class="pull-right" style="margin-top:20px;">
                            <div class="form-group pull-right">
                                <div class="checkbox">
                                    <label for="gerarComp">
                                    <input type="checkbox" id="gerarComp" name="gerarComp">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Deseja gerar um comprovante do empréstimo?
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="empPatrimonios[]" id="empPatrimonios">
                                <input type="hidden" name="empValorTotal" id="empValorTotal">
                                <button type="button" onclick="voltarEtapa()" class="btn btn-sm btn-danger">Voltar</button>
                                <button type="button" onclick="submeterForm()" class="btn btn-sm btn-primary">Cadastrar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>  
        </div>
        <!--Aba 2--> 
    </div>
</div>
<br>
<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão -->

<!--Modal de error -->
<div class="modal" tabindex="-1" role="dialog" id="error">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Atenção</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <br>
          </button>
        </div>
            <div class="modal-body">
                <p>Não foi selecionado nenhum patrimônio!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
      </div>
    </div>
</div> 
<!--Modal de error -->

<!-- Controle das tabs -->
@if($errors->get('empPatrimonios'))
    <script type="text/javascript">
        $(function(){
            $('#error').modal('show');
        });
    </script>
@endIf
@if(count($errors)>0)
    <script type="text/javascript">
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
    </script>
@else
<script type="text/javascript">
    $('#tab_1').addClass('active');
    $('#li_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#li_2').removeClass('active');
</script>
@endif
<!-- Controle das tabs -->
<!-- SELECT2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $(function(){
            $('#fornecedor_forCodigo').select2();
            $('#patrimonio').select2();
            $('#categoria').select2();
            $('#unidade').select2();

            //se for navegador safari, nn funciona o type date, e o campo passa a ser text
            //então se for do tipo text eu aplico a máscara
            if(empDataEmprestimo.type == 'text'){
                $("#empDataEmprestimo").mask('00/00/0000');
            }
            if(empDataDevolucao.type == 'text'){
                $("#empDataDevolucao").mask('00/00/0000');
            }

            if(data.type == 'text'){
                $("#data").mask('00/00/0000');
            }  
        });
    </script>
    <script type="text/javascript" src="{{asset('js/listagem_emprestimo.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/emprestimo_leitor.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/emprestimo.js') }}"></script> 

     @if(session('itensComprovante') && session('emprestimoComprovante'))
        <script type="text/javascript" >
            
            $(function(){
                var itensC =  {!! session('itensComprovante') !!};
                var emprestimoC = {!! session('emprestimoComprovante') !!};
                /*Transformando a imagem em string base64*/
                function toDataURL(url, callback) {
                        var xhr = new XMLHttpRequest();
                        xhr.onload = function() {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                            callback(reader.result);
                            }
                            reader.readAsDataURL(xhr.response);
                        };
                        xhr.open('GET', url);
                        xhr.responseType = 'blob';
                        xhr.send();
                    }
                    toDataURL('http://localhost:8000/imagens/tatame3.png', function(dataUrl) {
                        gerarComprovante(dataUrl,itensC,emprestimoC);
                    });

                /*Transformando a imagem em string base64*/
            });
            function gerarComprovante(logo,itens,emprestimo){
                
                monName = new Array ("janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho","agosto","setembro", "outubro", "novembro", "dezembro")
                now = new Date();
                var data = "Formiga, " + now.getDate() + " de " + monName[now.getMonth()] + " de " + now.getFullYear();
                /*** FONTS***/
                    pdfMake.fonts = {
                    Roboto: {
                        normal: 'Roboto-Regular.ttf',
                        bold: 'Roboto-Medium.ttf',
                        italics: 'Roboto-Italic.ttf',
                        bolditalics: 'Roboto-Italic.ttf'
                    },
                    Times: {
                    normal: 'Times-Roman.ttf',
                    bold: 'Times-Bold.otf',
                    italics: 'Times-Italic.ttf',
                    bolditalics: 'Times-BoldItalic.ttf'
                    }
                };      
                /*** FONTS***/

                var texts =[];
                for (var i = 0; i < itens.length; i++) {
                    var linha = itens[i].patNome +" -  Quantidade: "+ itens[i].qtde;
                    texts.push({text:linha});
                    
                }

                /*** INÍCIO DA MONTAGEM ***/
                var dd = {
                    pageSize: 'A4',
                    content: [
                        /***CABEÇALHO***/
                        {
                            table:{
                                body:
                                    [
                                        [
                                            {image:logo,width:60,height:60,alignment: 'left'}, 
                                            {
                                                table: {
                                                body: [
                                                        [
                                                            {text:'ASSOCIAÇÃO TATAME DO BEM',style:'header'}
                                                        ],   
                                                        [
                                                            {text:'R. José Pedro da Silva, - Bela Vista, Formiga - MG, 35570-000',style:'header_2'}
                                                        ]
                                                    ]
                                                },
                                                layout: 'noBorders'
                                            },    
                                        ],
                                    ]
                            },
                            layout: 'noBorders'
                        },
                        /***CABEÇALHO***/
                        /*** TÍTULO***/
                        {
                            text:'Comprovante do Empréstimo',
                            style:'header_3'
                        },
                        /*** TÍTULO***/
                        /*** TABELA COM OS PATRIMÔNIOS***/
                            
                        {
                            style:'col',
                            columns: [
                                {
                                    type:'none',
                                    ul: [
                                        {
                                            style:'ma',
                                            text:[
                                                {text: 'Empresa:',bold:true},
                                                {text: emprestimo.fornecedor.forNome},
                                            ]
                                        },
                                        {
                                            style:'ma',
                                            text:[
                                                {text: 'Nome do Solicitante:',bold:true,style:'ma'},
                                                {text: emprestimo.empNomeSolicitante},
                                            ]
                                        },
                                        {
                                            style:'ma',
                                            text:[
                                                {text: 'Responsável:',bold:true,style:'ma'},
                                                {text: emprestimo.user.useNome},
                                            ]
                                        },
                                    ]
                                },
                                {
                                    type:'none',
                                    style:'ulStyle',
                                    ul:[
                                        {
                                            style:'ma',
                                            text:[
                                                {text: 'Data Devolução:',bold:true,style:'ma'},
                                                {text: formatarDate(emprestimo.empDataDevolucao)},
                                            ]
                                        },
                                        {
                                            style:'ma',
                                            text:[
                                                {text: 'Valor Total:',bold:true,style:'ma'},
                                                {text: (emprestimo.empValorTotal).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })},
                                            ]
                                        },
                                    ]
                                }
                            ],
                        },
                        {
                            text:'__________________________________________________________________________________________',
                            alignment:'center'
                        },
                        {
                            text:'Patrimônios emprestados',
                            style:'header_4'

                        },
                        {
                            columns: [
                                {
                                    style:'ulItens',
                                    ul: texts
                                },
                            ],
                        },
                        {
                            text: 'Atenção: Caso haja algum dano aos patrimônios emprestados,o valor deverá ser reembolsado.',style:'clasula'
                        },
                        /*** TABELA COM OS PATRIMÔNIOS***/
                        /*** ASSINATURAS E DATA ***/
                        {
                            style: 'assinatura_data',
                            text: data
                        },
                        {
                            style: 'assinatura_linha',
                            alignment: 'center',
                            text: '_________________________________________________'
                        },
                        {
                            style: 'assinatura_nome',
                            alignment: 'center',
                            text:emprestimo.empNomeSolicitante
                        },
                        {
                            style: 'assinatura_linha',
                            alignment: 'center',
                            text: '_________________________________________________'
                        },
                        {
                            style: 'assinatura_nome',
                            alignment: 'center',
                            text: emprestimo.user.useNome,
                        },
                    ],
                    /*** ASSINATURAS E DATA ***/
                    /*** QUEBRA DE PÁGINAS ***/
                    pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
                            return currentNode.headlineLevel === 1 && followingNodesOnPage.length === 0;
                    },
                    /*** QUEBRA DE PÁGINAS ***/
                    /*** ESTILIZAÇÃO ***/
                    styles:{
                        header:{
                            font:'Times',
                            fontSize:15,
                            bold:true,
                            margin:[ 70, 0, 0, 0 ],
                            alignment:'center'
                        },
                        header_2:{
                            font:'Times',
                            fontSize:10,
                            bold:true,
                            margin:[ 75, 0, 0, 0 ],
                            alignment:'center'
                        },
                        header_3:{
                            font:'Times',
                            fontSize:14,
                            bold:true,
                            margin:[ 0, 15, 0, 0 ],
                            alignment:'center'
                        },
                        header_4:{
                            font:'Times',
                            fontSize:14,
                            bold:true,
                            margin:[ 18, 15, 0, 20],
                            alignment:'left'
                        },
                        clasula:{
                            margin:[18,20,0,0]
                        },
                        ulItens:{
                            margin:[18,0,0,0]
                        },
                        col:{
                            margin: [0,25,0,5]
                        },
                        ulStyle:{
                            margin: [10,0,0,0,0]
                        },
                        assinatura_data: {
                            margin: [0,60, 0,30],
                            alignment:'center'
                        },
                        assinatura_linha: {
                            margin: [0,20, 0,0],
                            alignment:'center'
                        },
                        assinatura_nome:{
                            margin:[0,0,0,30],
                            alignment:'center',
                        },
                        ma:{
                            margin:[0,5,5,5]
                        },
                    }
                };
                /*** ESTILIZAÇÃO ***/
                /*** FIM DA MONTAGEM ***/
                pdfMake.createPdf(dd).download('Comprovante');
            } 
        
    </script> 
    @endif
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
