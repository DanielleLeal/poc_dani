@extends('layouts.master') @section('title', '- Empréstimo')@section('emprestimoAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Estilo para tabela -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i>Editar Empréstimo </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li  id="li_1" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="active tab-pane" id="tab_1">
            <div class="box-body box-success">
                <form action="{{ route('emprestimo.update',$emprestimo->empCodigo) }}" method="POST" id="formEmp">
                    @csrf
                    @method('PUT')
                    <div class="container-fluid show" id="primeira">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="hidden" value="{{$emprestimo->empCodigo}}" id="emprestimo_id">
                                    <label for="empNomeSolicitante">Nome do Solicitante <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="empNomeSolicitante" name="empNomeSolicitante"
                                        value="{{$emprestimo->empNomeSolicitante}}" placeholder="Nome">
                                        @if($errors->get('empNomeSolicitante'))
                                            @foreach($errors->get('empNomeSolicitante') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                    <div id="empNome_error"></div>
                                </div>
                                <div class="form-group">
                                    <label for="fornecedor_forCodigo">Empresa <span class="text-danger">*</span></label>
                                        <select name="fornecedor_forCodigo" id="fornecedor_forCodigo" class="form-control">
                                            <option value="0">Selecione</option>
                                             @foreach ($fornecedores as $fornecedor)
                                            <option value="{{$fornecedor->forCodigo}}"
                                                @if($emprestimo->fornecedor_forCodigo == $fornecedor->forCodigo)
                                                selected @endif>
                                                {{$fornecedor->forNome}}
                                            </option>
                                             @endforeach   
                                        </select>
                                    <div id="fornecedor_error"></div>
                                </div>
                                <div class="form-group">
                                    <label for="empFinalidade">Finalidade do uso </label>
                                    <textarea name="empFinalidade" id="empFinalidade"  rows="5" class="form-control" placeholder="Breve descrição sobre a finalidade do uso dos patrimônios que serão emprestados">{{$emprestimo->empFinalidade}}</textarea>
                                    @if($errors->get('empFinalidade'))
                                        @foreach($errors->get('empFinalidade') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="empDataEmprestimo">Data do Empréstimo <span class="text-danger">*</span></label>
                                        <input type="date" name="empDataEmprestimo" id="empDataEmprestimo" class="form-control" 
                                        value="{{$emprestimo->empDataEmprestimo}}"placeholder="dd/mm/YYYY">
                                        @if($errors->get('empDataEmprestimo'))
                                            @foreach($errors->get('empDataEmprestimo') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                        <div id="dataEmp_error"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="empDataDevolucao">Data do Devolução <span class="text-danger">*</span></label>
                                        <input type="date" name="empDataDevolucao" id="empDataDevolucao" class="form-control" 
                                        value="{{$emprestimo->empDataDevolucao}}" placeholder="dd/mm/YYYY">
                                        @if($errors->get('empDataDevolucao'))
                                            @foreach($errors->get('empDataDevolucao') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                        <div id="dataDev_error"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="users_useCodigo">Responsável</label>
                                        <input type="text" readonly class="form-control" id="users_useCodigo" name="users_useCodigo" 
                                        value="{{$emprestimo->user->useNome}}">
                                        @if($errors->get('users_useCodigo'))
                                            @foreach($errors->get('users_useCodigo') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                        <div class="pull-right">
                                            <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                            <div class="form-group" style="margin-top:15px;">
                                                <button type="button" onclick="proximaEtapa()" class="btn btn-primary btn-block">Próximo</button>
                                            </div>
                                        </div><br>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="container-fluid hide" id="segunda">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title" style=" margin-left:15px;">Selecione os patrimônios:</h4>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="patrimonio">Patrimônio</label> <br>
                                        <select style="width:100%;" name="patrimonio" id="patrimonio" class="form-control"onchange="filtrar('patrimonio',this.value)">
                                            <option value="0" selected>Todos</option>
                                            @foreach ($patrimonios as $patrimonio)
                                            <option value="{{$patrimonio->patCodigo}}">
                                                {{$patrimonio->patNome}}
                                            </option>
                                            @endforeach   
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="status">Status</label> <br>
                                        <select style="width:100%;" name="status" id="status" class="form-control" onchange="filtrar('status',this.value)">
                                            <option value="0" selected>Todos</option>
                                            <option value="Ativo">Ativo</option>
                                            <option value="Inativo">Inativo</option>
                                            <option value="Manutenção">Manutenção</option>
                                            <option value="Estragado">Estragado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="categoria">Categoria</label> <br>
                                        <select style="width:100%;" name="categoria" id="categoria" class="form-control"onchange="filtrar('categoria',this.value)">
                                            <option value="0" selected>Todas</option>
                                            @foreach ($categorias as $categoria)
                                            <option value="{{$categoria->catCodigo}}">
                                                {{$categoria->catNome}}
                                            </option>
                                            @endforeach   
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="unidade">Unidade</label> <br>
                                        <select style="width:100%;" name="unidade" id="unidade" class="form-control" onchange="filtrar('unidade',this.value)">
                                            <option value="0" selected>Todas</option>
                                            @foreach ($unidades as $uni)
                                                <option value="{{$uni->uniCodigo}}">
                                                    {{$uni->uniNome}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Para selecionar os patrimonios --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right" style="margin-right:15px;">
                                    <label for="pesquisar">Pesquisar</label>
                                    <input type="search" name="pesquisar" id="pesquisar">
                                </div>
                                <div class="form-group pull-left" style="margin-right:10px;">
                                    <div class="checkbox">
                                        <label for="ch">
                                         <input type="checkbox" id="ch" name="ch" onclick="selecionarTodos()">
                                         <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                         Selecionar Todos
                                         </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12" style="margin-top:20px;">
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="table-emprestimo">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Código de Barras</th>
                                                    <th>Nome</th>
                                                    <th>Valor Atual</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <div> 
                                            <span id="card-title"></span>
                                                <span class="pull-right">
                                                    <nav id="paginationNav">
                                                        <ul class="pagination">
                                                        </ul>
                                                    </nav>
                                            </span>
                                            <div class="clearfix"></div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- patrimonios selecionados pelo usuario --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title" style="margin-top:10px; margin-left:15px;">Patrimônios selecionados</h4>
                                <div class="col-lg-12" style="margin-top:20px;">
                                    <div class="table-responsive">
                                        <table class="table table-hover" id="tabelaEmprestado">
                                            <thead>
                                                <tr>
                                                    <th>Código de Barras</th>
                                                    <th>Nome</th>
                                                    <th>Valor Atual</th>
                                                    <th>Opção</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyEmprestado">
                                            </tbody>
                                        </table>
                                        <div class="pull-right" id="pagination" style="display: none;">
                                            <ul class="pagination">
                                                <li class="page-item">
                                                    <a id="anterior" style="cursor:pointer;" class="page-link">Anterior</a>
                                                </li>
                                                <li class="page-item">
                                                    <a id="proximo"style="cursor:pointer;" class="page-link">Próximo</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="pull-left titlePagination" id="mostrarPaginas">
                                        </div>
                                    </div>
                                </div>
                                <h4 class="title-emp">Valor Total: </h4><h4 style="display: inline;font-size: 18px;" id="valorRee">{{$emprestimo->empValorTotal}}</h4>
                            </div>
                        </div>     
                        <div class="pull-right" style="margin-top:20px;">
                            <div class="form-group">
                                <input type="hidden" name="empPatrimonios[]" id="empPatrimonios">
                                <input type="hidden" name="empValorTotal" id="empValorTotal">
                                <button type="button" onclick="voltarEtapa()" class="btn btn-sm btn-danger">Voltar</button>
                                <button type="button" onclick="submeterForm()" class="btn btn-sm btn-primary">Atualizar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>  
        </div>
        <!--Aba 1-->
    </div>
</div>
<br>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão --> 

<!--Modal de error -->
<div class="modal" tabindex="-1" role="dialog" id="error">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Atenção</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <br>
          </button>
        </div>
            <div class="modal-body">
                <p>Não foi selecionado nenhum patrimônio!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
      </div>
    </div>
</div> 
<!--Modal de error -->

<!-- SELECT2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

        $('#fornecedor_forCodigo').select2();
        $('#patrimonio').select2();
        $('#categoria').select2();
        $('#unidade').select2();

        if(empDataEmprestimo.style == 'text'){
            $("#empDataEmprestimo").mask('00/00/0000');
        }

        if(empDataDevolucao.style == 'text'){
            $("#empDataDevolucao").mask('00/00/0000');
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/emprestimo.js') }}"></script>
    <script type="text/javascript" >
        $(function(){
            itensEmprestados();
        });
    </script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
