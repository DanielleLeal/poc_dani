@extends('layouts.master')
@section('title', '- Empréstimo')
@section('emprestimoAtivo','bg-danger')
@section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Style -->
<style>
.font15{
    font-size: 15px;
    color: #616161;
  }
</style>

<h3 style="margin-left:10px;"><i class="fa fa-building"></i> Empréstimos </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Detalhes do Empréstimo</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Nome do Solicitante: </label>
                            <span class="font15">{{$emprestimo->empNomeSolicitante}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Data do Empréstimo: </label>
                            <span class="font15">{{date_create($emprestimo->empDataEmprestimo)->format('d/m/Y')}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Quantidade de Itens: </label>
                            <span class="font15">{{$emprestimo->empQuantidade}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i>Itens de Patrimônios Emprestados:</label>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Finalidade: </label>
                            @if($emprestimo->empFinalidade == null || $emprestimo->empFinalidade =='' )
                                <span class="font15"> - </span>
                            @else
                                <span class="font15">{{$emprestimo->empFinalidade}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Valor Total : </label>
                            <span class="font15">R${{number_format($emprestimo->empValorTotal, 2, ',', '.')}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Status: </label>
                            <span class="font15">{{$emprestimo->empStatus}}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <input type="hidden" name="empCodigo" id="empCodigo" value="{{$emprestimo->empCodigo}}">
                    <div class="col-lg-12" style="margin-top:5px;">
                        <div class="table-responsive" style="margin-top:10px;">
                            <table class="table table-hover" id="itens-table">
                                <thead>
                                    <tr>
                                        <th class="text-center">Código de Barras</th>
                                        <th class="text-center">Nome</th>
                                        <th class="text-center">Valor Atual</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($itens as $item)
                                        <tr>
                                            <td class="text-center">{{$item->itpCodigoBarras}}</td>
                                            <td class="text-center">{{$item->patNome}}</td>
                                            <td class="text-center">R$ {{number_format($item->itpValorAtual, 2, ',', '.')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
        <!--Aba 1-->
    </div>
</div>
<br>
<!--Modal de exclusão --> 
    <script type="text/javascript">
    jQuery(function() {
            jQuery('#itens-table').DataTable({
                dom: 'lBfrtip',
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    'pdfHtml5',
                    'excelHtml5'
                ]
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
