@extends('layouts.master')
@section('title', '- Devolução')
@section('emprestimoAtivo','bg-danger')
@section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="{{asset('css/devolucao.css')}}">


<h3 style="margin-left:10px;"><i class="fa fa-check"></i> Devoluções</h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Devolução</a></li>
        <li style="display: none;" id="li_2"  id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Itens Não Devolvidos</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Nome do Solicitante: </label>
                            <span class="font15">{{$emprestimo->empNomeSolicitante}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Data do Empréstimo: </label>
                            <span class="font15">{{date_create($emprestimo->empDataEmprestimo)->format('d/m/Y')}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Data do Devolução: </label>
                            <span class="font15">{{date_create($emprestimo->empDataDevolucao)->format('d/m/Y')}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Empresa: </label>
                            <span class="font15">{{$emprestimo->fornecedor->forNome}}</span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Finalidade: </label>
                            @if($emprestimo->empFinalidade == null || $emprestimo->empFinalidade =='' )
                                <span class="font15"> - </span>
                            @else
                                <span class="font15"> {{$emprestimo->empFinalidade}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Valor Total : </label>
                            <span class="font15">R$ {{number_format($emprestimo->empValorTotal, 2, ',', '.')}}</span>
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fa fa-arrow-right"></i> Status: </label>
                            <span class="font15">{{$emprestimo->empStatus}}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <h4 class="card-title titulo">Selecione os itens que serão devolvidos</h4>
                        <div class="form-group pull-right" style="margin-right:35px;">
                            <div class="checkbox">
                                <label for="ch">
                                 <input type="checkbox" id="ch" name="ch" onclick="selecionarTodos()">
                                 <span class="cr font15"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                 Devolver todos itens desta página
                                 </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="table-itens" style="margin-top: 20px;" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Código de Barras</th>
                                        <th>Nome</th>
                                        <th>Valor Atual</th>
                                    </tr>
                            </table><br>
                        </div>    
                    </div>
                    <div>
                        <form action="{{route('devolver.store')}}" method="POST" id="formDev">
                            @csrf
                            <input type="hidden" value="{{$emprestimo->empCodigo}}" id="empCodigo" name="empCodigo">
                            <input type="hidden" name="itensDevolvidos" id="itensDevolvidos">
                            <input type="hidden" name="itensNaoDevolvidos[]" id="itensNaoDevolvidos">
                            <div class="pull-right">
                                <div class="form-group">
                                    <a href="{{route('emprestimo.indexD')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                    <button type="button"onclick="formV()" class="btn btn-sm btn-primary">Devolver</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane" id="tab_2">
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="itpStatus">Status</label>
                            <select name="itpStatus" id="itpStatus" class="form-control">
                                <option value="Ativo">Ativo</option>
                                <option value="Inativo">Inativo</option>
                                <option value="Manutenção">Manutenção</option>
                                <option value="Estragado">Estragado</option>
                            </select>
                        </div>
                        <h4 style="margin-left:0px; margin-bottom:20px;" class="card-title titulo">Selecione os itens que deseja aplicar o status</h4>
                    </div>
                    <div class="col-lg-11">
                        <div class="pull-right">
                            <div class="form-group ">
                                <div class="checkbox">
                                    <label for="md">
                                     <input type="checkbox" id="md" name="md" onclick="mudarTodos()">
                                     <span class="cr font15"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                     Selecionar todos
                                     </label>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <table id="table-itensN" style="margin-top: 20px;" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Código de Barras</th>
                                        <th>Nome</th>
                                        <th>Valor Atual</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_itensN">

                                </tbody>
                            </table><br>
                            <div class="pull-right" style="display: none;" id="buttons">
                                <div class="form-group">
                                    <a href="{{route('emprestimo.indexD')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                    <button type="button"onclick="enviar()" class="btn btn-sm btn-primary">Concluir</button>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        <!--Aba 2-->
    </div>
</div>
<br>
    <script>
        jQuery(function() {
                jQuery('#table-itens').DataTable({
                    dom: 'lBfrtip',
                    "ajax": {
                        "url": 'http://localhost:8000/datatable/devolucao/'+empCodigo.value,
                        "type": "GET"
                    },
                    "drawCallback": function(settings){
                        marcados =[];
                        $('.marcar').each(function () {
                            if(this.checked){
                                marcados.push(this);
                            }
                        });
                        if($('.marcar').length == marcados.length){
                            ch.checked = true;
                        }else{
                            ch.checked = false;
                        }
                    },
                    "columns": [{
                            data: 'itpCodigo',
                            render: function(data) {
                                var html = '<div class="checkbox">'+
                                                '<label for="item'+data+'">'+
                                                    '<input type="checkbox" class="marcar" id="item'+data+'" onclick="selecionar(this)"value="'+data+'">'+
                                                    '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>'+
                                                '</label>'+
                                            '</div>';
                                return html;
                            }, 
                            name: '#'
                        },
                        {
                            data: 'itpCodigoBarras',
                            name: 'Código de Barras'
                        },
                        {
                            data: 'patNome',
                            name: 'Nome'
                        },
                        {
                            data: 'itpValorAtual',
                            render: function(data) {
                                var format = data.toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' });
                                return format;
                            },
                            name: 'Valor Atual'
                        }
                    ],
                    "language": {
                        "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                    },
                    buttons: [
                        'pdfHtml5',
                        'excelHtml5'
                    ]
                });
            });
    </script>
    <script type="text/javascript" src="{{asset('js/devolucao.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
