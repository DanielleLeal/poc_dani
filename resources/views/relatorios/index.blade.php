@extends('layouts.master') 
@section('title', '- Relatório')
@section('relatoriosAtivo','bg-danger')
@section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Estilo-->
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-file-pdf-o" style="font-weight:bold;"></i> Relatório </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Relatório</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="uniCodigo">Unidades</label><br>
                            <select name="uniCodigo" id="uniCodigo" class="form-control">
                                <option value="" selected>Todas</option>
                                @foreach ($unidades as $unidade)
                                <option value="{{$unidade->uniCodigo}}">
                                    {{$unidade->uniNome}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="catCodigo">Categorias</label><br>
                            <select name="catCodigo" id="catCodigo" class="form-control">
                                <option value="" selected>Todas</option>
                                @foreach ($categorias as $categoria)
                                <option value="{{$categoria->catCodigo}}">
                                    {{$categoria->catNome}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="itpStatus">Status</label>
                            <select name="itpStatus" id="itpStatus" class="form-control">
                                <option value="" selected>Todos</option>
                                <option value="Ativo">Ativo</option>
                                <option value="Inativo">Inativo</option>
                                <option value="Manuntenção">Manuntenção</option>
                                <option value="Estragado">Estragado</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="itpSituacao">Situação</label>
                            <select name="itpSituacao" id="itpSituacao" class="form-control">
                                <option value="" selected>Todos</option>
                                <option value="Local">Local</option>
                                <option value="Emprestado">Emprestado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="itpTipo">Tipo</label>
                            <select name="itpTipo" id="itpTipo" class="form-control">
                                <option value="" selected>Todos</option>
                                <option value="Doado">Doado</option>
                                <option value="Comprado">Comprado</option>
                            </select>
                        </div>
                        <div class="form-group" style="margin-top:42px;">
                            <button type="button" onclick="filtros()" class="btn btn-pdf btn-block">Gerar PDF</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="modalAlert">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Alerta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <br>
          </button>
        </div>
            <div class="modal-body">
                <p>Não possui patrimônios para gerar PDF com os filtros aplicados</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
      </div>
    </div>
  </div>
  <!-- Modal alerta que não possui patrimônios para gerar pdf-->

  <!-- SELECT2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    $('#uniCodigo').select2();
    $('#catCodigo').select2();
</script>

 <!-- Montagem e preenchimento do PDF-->
<script src="{{asset('js/relatorios.js')}}"></script>


@endsection