@extends('layouts.master') 
@section('title', '- Autorização')
@section('patrimonioAtivo','bg-danger') 
@section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<h3 style="margin-left:10px;"><i class="fa fa-book"></i> Autorizações </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Autorizações</a></li>
        <li><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                                <div class="table-responsive">
                                    <table class="table table-hover " id="autorizacao-table">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Endereço</th>
                                                <th>Numero</th>
                                                <th>Bairro</th>
                                                <th>Cidade</th>
                                                <th>Estado</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane " id="tab_2">
            <div class="box-body box-success">
                <form action="{{ route('unidade.store') }}" method="POST">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="autNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="autNome" name="autNome" placeholder="Digite o nome">
                                    @if($errors->get('autNome'))
                                        @foreach($errors->get('autNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="autDescricao">Descrição</label>
                                    <textarea name="autDescricao" id="autDescricao"rows="5" class="form-control"
                                     placeholder="Descreva uma breve relato sobre a autorização"></textarea>
                                    @if($errors->get('autDescricao'))
                                        @foreach($errors->get('autDescricao') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="autValor">Valor Permitido <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" id="autValor" name="autValor">
                                    @if($errors->get('autValor'))
                                        @foreach($errors->get('autValor') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="autData">Data <span class="text-danger">*</span></label>
                                    <input type="date" class="form-control" id="autData" name="autData">
                                    @if($errors->get('autData'))
                                        @foreach($errors->get('autData') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="users_useCodigo">Responsável <span class="text-danger">*</span></label>
                                    <select name="users_useCodigo" id="users_useCodigo" class="form-control">
                                        <option value="0">Selecione</option>
                                        @foreach ($users as $user)
                                            <option value="{{$user->useCodigo}}">{{$user->useNome}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->get('users_useCodigo'))
                                        @foreach($errors->get('users_useCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('unidade.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 2-->   
    </div>
</div>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão -->  

<script type="text/javascript"> 
    var baseurl = window.location.protocol + '//' + window.location.host + '/';

    //preenchendo o datatable
    jQuery(function() {
            jQuery('#autorizacao-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("autorizacao.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                        data: 'uniNome',
                        name: 'Nome'
                    },
                    {
                        data: 'endereco.endRua',
                        name: 'Rua'
                    },
                    {
                        data: 'endereco.endNumero',
                        name: 'Numero'
                    },
                    {
                        data: 'endereco.endBairro',
                        name: 'Bairro'
                    },
                    {
                        data: 'endereco.cidade.cidCidade',
                        name: 'Cidade'
                    },
                    {
                        data: 'endereco.estado.estEstado',
                        name: 'Estado'
                    },
                    {
                        data: "uniCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="unidade/' + data + '/edit" class="btn btn-primary btn-block">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-6">';
                            html += '<p><button type="button" class="btn btn-danger btn-block" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    'pdfHtml5',
                    'excelHtml5'
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var uniCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'unidade/'+uniCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.unidade.uniCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir a unidade <strong>"+ resposta.unidade.uniNome+"</strong>?";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
        }  
            
        //mandando pro metodo destroy
        function funcaoExcluir(){
            var uniCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "unidade/"+uniCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload('#tab_2');
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
        }
    </script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
