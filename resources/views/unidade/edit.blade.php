@extends('layouts.master') @section('title', '- Unidade')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i> Editar Unidade  </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
        
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="box-body box-success">
                <form action="{{ route('unidade.update',$unidade->uniCodigo) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="uniNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="uniNome" name="uniNome" value="{{$unidade->uniNome}}">
                                    @if($errors->get('uniNome'))
                                        @foreach($errors->get('uniNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="endRua">Rua <span class="text-danger">*</span></label>
                                        <input type="endRua" name="endRua" id="endRua" value="{{$unidade->endereco->endRua}}" class="form-control">
                                            @if($errors->get('endRua'))
                                                @foreach($errors->get('endRua') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="endNumero">Número <span class="text-danger">*</span></label>
                                            <input type="number" name="endNumero" id="endNumero"class="form-control" value="{{$unidade->endereco->endNumero}}">
                                            @if($errors->get('endNumero'))
                                                @foreach($errors->get('endNumero') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label for="endBairro">Bairro <span class="text-danger">*</span></label>
                                    <input type="endBairro" name="endBairro" id="endBairro" value="{{$unidade->endereco->endBairro}}" class="form-control">
                                    @if($errors->get('endBairro'))
                                        @foreach($errors->get('endBairro') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="endCEP">CEP</label>
                                    <input type="text" name="endCEP" id="endCEP" class="form-control" value="{{$unidade->endereco->endCEP}}">
                                    @if($errors->get('endCEP'))
                                        @foreach($errors->get('endCEP') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="estado_estCodigo">Estado <span class="text-danger">*</span></label>
                                    <select name="estado_estCodigo" id="estado_estCodigo" class="form-control">
                                        <option value="0">Selecione</option>
                                        @foreach ($estados as $estado)
                                            <option value="{{$estado->estCodigo}}" 
                                                @if($unidade->endereco->estado_estCodigo == $estado->estCodigo) selected @endif>
                                                {{$estado->estEstado}}
                                            </option>
                                        @endforeach    
                                    </select>
                                    @if($errors->get('estado_estCodigo'))
                                        @foreach($errors->get('estado_estCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="cidade_cidCodigo">Cidade <span class="text-danger">*</span></label>
                                    <select name="cidade_cidCodigo" id="cidade_cidCodigo" class="form-control">
                                        @foreach ($cidades as $cidade)
                                            <option value="{{$cidade->cidCodigo}}"
                                                @if($unidade->endereco->cidade_cidCodigo == $cidade->cidCodigo) selected @endif>
                                                {{$cidade->cidCidade}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->get('cidade_cidCodigo'))
                                        @foreach($errors->get('cidade_cidCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div><br>
                                </div>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('unidade.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 1-->   
    </div>
</div> 

<script type="text/javascript">

    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    //mascara para o campo CEP
    $('#endCEP').mask("00000-000");

    //preenchendo o select de cidade de acordo com o estado escolhido
    $('select[name=estado_estCodigo]').change(function () {
            var estCodigo = $(this).val();
            $.ajax({
                type: 'GET',
                url:baseurl+ "unidade/cidade/"+estCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        console.log(resposta.cidades);
                        $('#cidade_cidCodigo').empty();
                        for(var i=0; i <resposta.tamanho;i++){
                            $('#cidade_cidCodigo').append('<option value=' + resposta.cidades[i].cidCodigo + '>' + resposta.cidades[i].cidCidade + '</option>');
                        }
                        
                    }		
                });

    }); 
</script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
