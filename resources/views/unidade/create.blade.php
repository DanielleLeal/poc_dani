@extends('layouts.master') 
@section('title', '- Unidade')
@section('cadastrosdiversosAtivo','bg-danger') 
@section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-institution"></i> Unidades </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Unidades</a></li>
        <li id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                                <div class="table-responsive">
                                    <table class="table table-hover " id="unidade-table">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Endereço</th>
                                                <th>Número</th>
                                                <th>Bairro</th>
                                                <th>Cidade</th>
                                                <th>CEP</th>
                                                <th>Estado</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane " id="tab_2">
            <div class="box-body box-success">
                <form action="{{ route('unidade.store') }}" method="POST">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="uniNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="uniNome" name="uniNome" placeholder="Digite o nome">
                                    @if($errors->get('uniNome'))
                                        @foreach($errors->get('uniNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="endRua">Rua <span class="text-danger">*</span></label>
                                            <input type="endRua" name="endRua" id="endRua" placeholder="Digite o nome da rua" class="form-control">
                                            @if($errors->get('endRua'))
                                                @foreach($errors->get('endRua') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="endNumero">Número <span class="text-danger">*</span></label>
                                            <input type="number" name="endNumero" id="endNumero"class="form-control" placeholder="Ex: 380">
                                            @if($errors->get('endNumero'))
                                                @foreach($errors->get('endNumero') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label for="endBairro">Bairro <span class="text-danger">*</span></label>
                                    <input type="endBairro" name="endBairro" id="endBairro" placeholder="Digite o nome do bairro" class="form-control">
                                    @if($errors->get('endBairro'))
                                        @foreach($errors->get('endBairro') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="endCEP">CEP</label>
                                    <input type="text" name="endCEP" id="endCEP" class="form-control" placeholder="00000-000">
                                    @if($errors->get('endCEP'))
                                        @foreach($errors->get('endCEP') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="estado_estCodigo">Estado <span class="text-danger">*</span></label>
                                    <select style="width: 100%;" name="estado_estCodigo" id="estado_estCodigo" class="form-control">
                                        <option value="">Selecione</option>
                                        @foreach ($estados as $estado)
                                            <option value="{{$estado->estCodigo}}">
                                                {{$estado->estEstado}} - {{$estado->estSigla}}
                                            </option>
                                        @endforeach
                                    </select>    
                                    @if($errors->get('estado_estCodigo'))
                                        @foreach($errors->get('estado_estCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="cidade_cidCodigo">Cidade <span class="text-danger">*</span></label>
                                    <select style="width: 100%;" name="cidade_cidCodigo" id="cidade_cidCodigo" class="form-control">
                                        <option value="">Selecione</option>
                                    </select>
                                    @if($errors->get('cidade_cidCodigo'))
                                        @foreach($errors->get('cidade_cidCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                </div>
                                <br>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('unidade.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 2-->   
    </div>
</div>
<br>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão -->  
<!-- Controle das tabs -->
@if(count($errors)>0)
    <script type="text/javascript">
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
    </script>
@else
<script type="text/javascript">
    $('#tab_1').addClass('active');
    $('#li_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#li_2').removeClass('active');
</script>
@endif
<!-- Controle das tabs -->

<!-- SELECT2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    //mascara para o campo CEP
    $('#endCEP').mask("00000-000");

    //adicionando pesquisa no select de estado
    $('#estado_estCodigo').select2();
    $('#cidade_cidCodigo').select2();

    //preenchendo o select de cidade de acordo com o estado escolhido
    $('select[name=estado_estCodigo]').change(function () {
            var estCodigo = $(this).val();
            $.ajax({
                type: 'GET',
                url:baseurl+ "unidade/cidade/"+estCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        $('#cidade_cidCodigo').empty();
                        for(var i=0; i <resposta.tamanho;i++){
                            $('#cidade_cidCodigo').append('<option value=' + resposta.cidades[i].cidCodigo + '>' + resposta.cidades[i].cidCidade + '</option>');
                        }
                        
                    }		
                });

    });        
    //preenchendo o datatable
    jQuery(function() {
            jQuery('#unidade-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("unidade.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                        data: 'uniNome',
                        name: 'Nome'
                    },
                    {
                        data: 'endereco.endRua',
                        name: 'Rua'
                    },
                    {
                        data: 'endereco.endNumero',
                        name: 'Numero'
                    },
                    {
                        data: 'endereco.endBairro',
                        name: 'Bairro'
                    },
                    {
                        data: 'endereco.cidade.cidCidade',
                        name: 'Cidade'
                    },
                    {
                        data: 'endereco.endCEP',
                        name: 'CEP'
                    },
                    {
                        data: 'endereco.estado.estEstado',
                        name: 'Estado'
                    },
                    {
                        data: "uniCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="unidade/' + data + '/edit" class="btn btn-primary btn-block">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-6">';
                            html += '<p><button type="button" class="btn btn-danger btn-block" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2,3,4,5,6] 
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2,3,4,5,6] 
                        }
                    },
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var uniCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'unidade/'+uniCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.unidade.uniCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir a unidade <strong>"+ resposta.unidade.uniNome+"</strong>?";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
        }  
            
        //mandando pro metodo destroy
        function funcaoExcluir(){
            var uniCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "unidade/"+uniCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload('#tab_2');
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
        }
    </script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>   
@endsection
