<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('data/css/bootstrap.min.css') }}">
	<!--Bootstrapmim -->
	<link rel='stylesheet' href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'/>
	<link rel="stylesheet" href="{{ asset('data/bootstrap/css/bootstrap.min.css') }}">
	<!-- Template do Menu Lateral-->
	<link rel="stylesheet" href="{{ asset('data/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ asset('data/css/jquery-filestyle.css') }}">
	<link rel="stylesheet" href="{{ asset('data/css/AdminLTESkins.min.css') }}">
	<!-- Buttons do Datatable-->
	<link rel="stylesheet" href="{{ asset('https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css') }}">
	<link rel="stylesheet" href="{{ asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="{{ asset('data/css/styleCss.css') }}"> @section('css') @show
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="{{ asset('/data/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
	<!-- Icone no title -->
	<link rel="shortcut icon" href="{{ asset('imagens/tatame3.png') }}">
	<!--  Folha de estilo -->
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/formularios.css')}}">
	<title>Tatame do Bem @yield('title')</title>
</head>


<body class="fixed sidebar-mini" id="body">
  <?php
    $user =  Auth::user();
  ?>
	<div class="wrapper">
		<header class="main-header" id="header" style="background: white;">
				<!-- Barra Superior -->
				<a href="{{ url('/home') }}" class="logo">
					
					 <span class="logo-mini"><img id="logo1" height="50" width="50" src="{{ asset('imagens/tatame3.png') }}"  alt="Mini Image"></span>
					<span class="logo-lg"><img id="logo2" src="{{ asset('imagens/nomeTatatme.png') }}" alt="Mini Image"></span> 
				</a>
				<nav class="navbar navbar-static-top" role="navigation" id="side0" >
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Navegador</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li>
								<li class="dropdown user user-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										@if($user->useFoto == null || $user->useFoto=='imagens/default-user.png' )
											<img src="{{asset('imagens/default-user.png')}}" class="img-circle user-image" alt="Imagem padrão de usuário">
										@else	
											<img src="/storage/{{$user->useFoto}}"class="img-circle user-image">
										@endIf	
									</a>
									<ul class="dropdown-menu">
										<!-- Imagem do Usuário -->
										<li class="user-header">
											@if($user->useFoto == null || $user->useFoto=='imagens/default-user.png' )
												<img src="{{asset('imagens/default-user.png')}}" class="img-circle" width="70" height="70" alt="Imagem padrão de usuário">
											@else	
												<img src="/storage/{{$user->useFoto}}"class="img-circle" width="70" height="70">
											@endif
											<h4 textalign="center">{{$user->useNome}}</h4> 
											<h5 textalign="center">{{$user->email}}</h5>
										</li>
										<!-- Rodapé Menu Usuario -->
										<li class="user-footer">
											<div textalign="center">
												<a href="{{route('user.perfil',$user->useCodigo)}}" id="perfilbutton" class="btn btn-block btn-tatame">Meu Perfil</a>
												<br>
												<form action="{{ route('logout') }}" method="POST">
													@csrf
													<button type="submit" class="btn btn-block btn-tatame" name = "logout">
														Sair
													</button>
												</form>
											</div>
										</li>
									</ul>
								</li>
							</li>
						</ul>
					</div>
				</nav>
		</header>
		<!-- Barra Superior Esquerda -->
		<aside class="main-sidebar">
			<section class="sidebar">
				<!-- Menu -->
				<ul class="sidebar-menu">
					<li class="header">MENU</li>
					<li class="@yield('principalAtivo')">
						<a href="{{ url('/home') }}">
							<i class="fa fa-home disabled" style="font-size:16px;"></i> <span>Home</span>
						</a>
					</li>
					@if($user->funcao->funNome == 'Administrador' || $user->funcao->funNome == 'Funcionário')
						<li class="treeview @yield('cadastrosdiversosAtivo')">
							<a href="#">
								<i class="fa fa-cubes"></i>
								<span disabled>Cadastros Diversos</span>
								<i class="fa fa-angle-down pull-right"style="font-size:16px;"></i>
							</a>
							<ul class="treeview-menu">
								@if($user->funcao->funNome == 'Administrador')
									<li style="margin-left: 30px;">
										<a href="{{route('user.index')}}"><i class="fa fa-user"></i>Usuário</a>
									</li>
								@endif
								<li style="margin-left: 30px;">
									<a href="{{route('categoria.index')}}"><i class="fa fa-bookmark"></i>Categoria</a>
								</li>
								<li style="margin-left: 30px;">
									<a href="{{route('unidade.index')}}"> <i class="fa fa-institution"></i>Unidade</a>
								</li>
								<li style="margin-left: 30px;">
									<a href="{{route('fornecedor.index')}}"><i class="fa fa-truck"></i>Fornecedor</a>
								</li>
							</ul>
						</li>
						<li class="treeview @yield('patrimonioAtivo')">
							<a href="#">
								<i class="fa fa-institution"></i>
								<span disabled>Patrimônio</span>
								<i class="fa fa-angle-down pull-right" style="font-size:16px;"></i>
							</a>
							<ul class="treeview-menu">
								<li style="margin-left: 30px;">
									<a href="{{route('notaFiscal.index')}}"><i class="fa fa-sticky-note"></i>Nota Fiscal</a>
								</li>
								<li style="margin-left: 30px;">
									<a href="{{route('patrimonio.index')}}"><i class="fa fa-institution"></i>Patrimônios</a>
								</li>
								<li style="margin-left: 30px;">
									<a href="{{route('itensPatrimonio.create')}}"><i class="fa fa-plus"></i>Itens de Patrimônio</a>
								</li>
								<li style="margin-left: 30px;">
									<a href="{{route('transferencia.index')}}"><i class="fa fa-share" aria-hidden="true"></i>Transferências</a>
								</li>
							</ul>
						</li>
						<li class="treeview @yield('emprestimoAtivo')">
							<a href="#">
								<i class="fa fa-building"></i>
								<span disabled>Empréstimo</span>
								<i class="fa fa-angle-down pull-right" style="font-size:16px;"></i>
							</a>
							<ul class="treeview-menu">
								<li style="margin-left: 30px;">
									<a href="{{route('emprestimo.index')}}"><i class="fa fa-building"></i>Empréstimos</a>
								</li>
								@if($user->funcao->funNome == 'Administrador')
									<li style="margin-left: 30px;">
									<a href="{{route('emprestimo.indexD')}}"><i class="fa fa-check" style="font-size: 16px;" aria-hidden="true"></i>Realizar Devolução</a>
									</li>
								@endif
							</ul>
						</li>
						<li class="treeview @yield('depreciacaoAtivo')">
							<a href="{{route('depreciacao.index')}}">
								<i class="fa fa-calculator" style="font-weight:600px;"></i>
								<span>Depreciação</span>
							</a>
						</li>
						<li class="treeview @yield('relatoriosAtivo')">
							<a href="{{route('relatorios.index')}}">
								<i class="fa fa-file-pdf-o" style="font-weight:bold;font-size:16px"></i>
								<span>Relatório</span>
							</a>
						</li>
					@endif
				</ul>
				<!-- Menu -->
			</section>
		</aside>

		<!-- Tela Principal -->
		<div class="content-wrapper">
			<section class="" >
				@section('principal') @show
			</section>
		</div>
		
	</div>

	<!-- Rodapé -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			Topo&nbsp; &nbsp;
			<a href="#top"><i class="fa fa-arrow-up"></i></a>
		</div>
		Copyright &copy; 2020 | Todos os direitos reservados.
	</footer>

	<!-- Control SideBar JS -->
	<aside class="control-sidebar control-sidebar-light" id="controll">
		<div class="tab-content" id="control">
			<div class="tab-pane active" id="control-sidebar-theme-demo-options-tab">
			</div>
		</div>
	</aside>

	<!-- Scripts -->

	<script type="text/javascript" src="{{ asset('data/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('data/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('data/plugins/fastclick/fastclick.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/data/js/jquery-filestyle.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('data/dist/js/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('data/dist/js/demo.js') }}"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js "></script>
	<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<!-- PDFMAKE-->
	<script type="text/javascript" src="{{asset('data/build/pdfmake.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('data/build/vfs_fonts.js')}}"></script>
	<!-- Buttons Datatable -->
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>



	@section('js') @show
</body>

</html>