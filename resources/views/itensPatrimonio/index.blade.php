@extends('layouts.master') @section('title', '- Itens de Patrimônio')@section('patrimonioAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Estilo CSS -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-plus"></i> Itens de Patrimônio</h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Itens de Patrimônio -  {{$patrimonio->patNome}} </a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="hidden" name="patCodigo" value="{{$patCodigo}}" id="patCodigo">
                                @if($patrimonio->patFoto == 'imagens/default-patrimonio.png' || $patrimonio->patFoto == null)
                                    <div class="col-lg-3">
                                        <img src="{{asset('imagens/default-patrimonio.png')}}" alt="Imagem padrão de patrimônios" class="user-image"
                                    height="120" width="120" id="img">
                                    </div>
                                @else
                                    <div class="col-lg-3">
                                        <img src="{{asset('storage/'.$patrimonio->patFoto)}}" class="user-image" id="img" height="120" width="120" alt="Imagem do patrimônio">
                                    </div>
                                @endif
                                <div class="col-lg-3 pull-left">
                                    <label for="">Filtrar por</label>
                                    <select name="filtrar" id="filtrar" class="select"
                                    onchange="filtrarPor(this.value)">
                                        <option value="todos">Todos</option>
                                        <option value="unidade">Unidade</option>
                                        <option value="status">Status</option>
                                        <option value="situacao">Situação</option>
                                        <option value="tipo">Tipo</option>
                                    </select><br>
                                    <label for="" style="margin-right:30px;">Valor</label>
                                    <select name="valor" id="valor" class="select" onchange="filtrarValor(this.value)">
                                        <option value="0">Nenhum</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="pull-right" style="display: block;">
                                    <label for="pesquisar">Pesquisar</label>
                                    <input type="search" name="pesquisar" id="pesquisar">
                                </div>
                                <div style="clear: both;"></div> <br>
                                <div class="form-group pull-right">
                                    <a class ="export"href="{{route('itens_patrimonio.export',$patCodigo)}}">Excel</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                                <div class="table-responsive">
                                    <table class="table table-hover " id="itensPatrimonio-table">
                                        <thead>
                                            <tr>
                                                <th>Código de Barras</th>
                                                <th>Unidade</th>
                                                <th>Status</th>
                                                <th>Situação</th>
                                                <th>Tipo</th>
                                                <th>Valor Atual</th>
                                                <th>Nota Fiscal</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div> 
                                        <span id="card-title"></span>
                                        <span class="pull-right">
                                            <nav id="paginationNav">
                                                <ul class="pagination">
                                                </ul>
                                            </nav>
                                        </span>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div id="svgsTeste"></div>
        </div> 
        <!--Aba 1-->
    </div>
</div><br>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão --> 

<!-- Modal Visualizar nota fiscal -->
<div class="modal" tabindex="-1" role="dialog" id="modalNF">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa-sticky-note"></i> Nota Fiscal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> &times;</button>
        </div>
            <div class="modal-body">
                <div class="text-center">
                    <img  id="imgNF" height="550" width="550">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
      </div>
    </div>
  </div>
<!--Modal Visualizar nota fiscal-->


<!-- Preenchimento da tabela,páginação,filtro,função exluir e abrir modais -->
<script type="text/javascript" src="{{asset('js/itensPatrimonio.js')}}"></script>

<script type="text/javascript" >
        var logo ='';
        $(function(){
            /*Transformando a logo em string base64*/
            function toDataURL(url, callback) {
                    var xhr = new XMLHttpRequest();
                    xhr.onload = function() {
                        var reader = new FileReader();
                        reader.onloadend = function() {
                        callback(reader.result);
                        }
                        reader.readAsDataURL(xhr.response);
                    };
                    xhr.open('GET', url);
                    xhr.responseType = 'blob';
                    xhr.send();
                }
                toDataURL('http://localhost:8000/imagens/tatame3.png', function(dataUrl) {
                    logo = dataUrl
                });
        });
        function gerarBarcode(codigo){ // gerando código de barras no controller
            console.log(codigo);
            $.ajax({
				type: 'GET',
				url: baseurl + 'itensPatrimonio/barcode/'+codigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        //colocando no pdf
                        preencherPDF(resposta.svg,resposta.item);
					}
            });
        }

        function preencherPDF(svg,item){
            var texto = ''+item.patrimonio.patNome+' - '+item.itpCodigoBarras;
            var dd = {
            content: [
                    {
                        table:{
                            body:
                                [
                                    [
                                        {image: logo,width:20,height:20,alignment: 'left'},
                                        {
                                            table: {
                                            body: [
                                                    [
                                                        {text:texto,style:'header'},
                                                    ],
                                                    [
                                                        { image: svg,height:130,width:80}
                                                    ],
                                                ]
                                            },
                                            layout: 'noBorders'
                                        }    
                                    ],
                                ]
                        },
                        layout: 'noBorders'
                    },
                ],
                styles:{
                        header:{
                            fontSize:7,
                            bold:true,
                            alignment:'center'
                        },
                    }
            }
            pdfMake.createPdf(dd).download('Codigo de barras');
        }  
    </script>

    
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>

@endsection
