@extends('layouts.master')
@section('title', '- Itens de Patrimônio')
@section('patrimonioAtivo','bg-danger')
@section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Estilo para tabela -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<div>
    <h3 style="float: left;margin-left:10px;"><i class="fa fa-plus"></i> Itens de Patrimônio</h3>
    <div class="text-center"  style="margin-right:80px;">
        <img id="aviso" src='{{asset('imagens/aguarde.gif')}}' width="180" height="65"  style="display:none;">
    </div>
</div>
<div class="clearfix"></div>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <form  method="POST" id="formItem">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="patrimonio_patCodigo">Patrimônio <span class="text-danger">*</span></label>
                                <select name="patrimonio_patCodigo" id="patrimonio_patCodigo" class="form-control">
                                    <option value="">Selecione</option>
                                    @foreach ($patrimonios as $patrimonio)
                                        <option value="{{$patrimonio->patCodigo}}">
                                            {{$patrimonio->patNome}} - {{$patrimonio->categoria->catNome}}
                                        </option>
                                    @endforeach
                                </select>
                                <div id="patrimonio_error"></div>
                            </div>
                            <div class="form-group">
                                <label for="itpTipo">Tipo <span class="text-danger">*</span></label>
                                <select name="itpTipo" id="itpTipo" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Doado">Doado</option>
                                    <option value="Comprado">Comprado</option>
                                </select>
                                <div id="itpTipo_error"></div>
                            </div>
                            <div class="form-group">
                                <label for="itpSituacao">Situação <span class="text-danger">*</span></label>
                                <select name="itpSituacao" id="itpSituacao" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Local" selected>Local</option>
                                    <option value="Emprestado">Emprestado</option>
                                </select>
                                <div id="itpSituacao_error"></div>
                            </div>
                            <div class="form-group">
                                <label for="itpStatus">Status <span class="text-danger">*</span></label>
                                <select name="itpStatus" id="itpStatus" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="Ativo" selected>Ativo</option>
                                    <option value="Inativo">Inativo</option>
                                    <option value="Manutenção">Manutenção</option>
                                    <option value="Estragado">Estragado</option>
                                </select>
                                <div id="itpStatus_error"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="unidade_uniCodigo">Unidade <span class="text-danger">*</span></label>
                                <select name="unidade_uniCodigo" id="unidade_uniCodigo" class="form-control">
                                    <option value="">Selecione</option>
                                     @foreach ($unidades as $unidade)
                                    <option value="{{$unidade->uniCodigo}}">
                                        {{$unidade->uniNome}}
                                    </option>
                                     @endforeach   
                                </select>
                                <div id="unidade_error"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-11">
                                    <div class="form-group">
                                        <label for="nota_fiscal_nofCodigo">Nota Fiscal <span class="text-danger">*</span></label>
                                        <div style="display:flex;">
                                            <select name="nota_fiscal_nofCodigo" id="nota_fiscal_nofCodigo" class="form-control" >
                                               <option value="">Selecione</option>
                                                @foreach ($notasFiscais as $nota)
                                                    <option value="{{$nota->nofCodigo}}">
                                                        {{$nota->nofDescricao}}
                                                    </option>
                                               @endforeach 
                                            </select>
                                            
                                            <div class="col-lg-1" style="justify content: space beetween; margin-left:15px;" >
                                                <button type="button" onclick="abrir()" class="btn" id="btnNF" title="Cadastrar Nota Fiscal">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div id="nota_error"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="quantidade">Quantidade <span class="text-danger">*</span></label>
                                <input type="number" name="quantidade" id="quantidade" min="1" class="form-control" placeholder="Ex: 3">
                                <div id="quantidade_error"></div>
                                <div class="pull-right">
                                    <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                </div><br>
                            </div>
                            <div class="form-group pull-right">
                                <div class="checkbox">
                                    <label for="gerarCodigo">
                                     <input type="checkbox" id="gerarCodigo" name="gerarCodigo">
                                     <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                     Deseja gerar o código de barras de todos os itens de patrimônio?
                                     </label>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="form-group">
                                    <a href="{{route('patrimonio.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                    <button type="button" onclick="submeterItem()" class="btn btn-sm btn-primary">Cadastrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
        <!--Aba 1-->
    </div>
</div>
<!-- Adicionar NF -->
<div class="modal" tabindex="-1" role="dialog" id="modalNF">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-sticky-note"></i> Nota Fiscal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> &times;</button>
        </div>
            <div class="modal-body">
                <form  method="POST" enctype="multipart/form-data" id="formNF">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="nofArquivo">Arquivo <span class="text-danger">*</span></label>
                                    <input type="file" name="nofArquivo" id="nofArquivo" class="form-control">
                                    <div id="arquivo_error"></div>
                                </div>
                                <div class="form-group">
                                    <label for="nofDescricao">Descrição <span class="text-danger">*</span></label>
                                    <textarea name="nofDescricao" id="nofDescricao" rows="3" class="form-control" placeholder="Um breve descrição sobre a nota fiscal"></textarea>
                                    <div  id="descricao_error"></div>
                                        
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>    
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <div class="form-group">
                        <button type="button" class="btn btn-sm btn-danger mr-3" data-dismiss="modal">Cancelar</button>
                        <button type="button" onclick="submeter()" class="btn btn-sm btn-primary">Cadastrar</button>
                    </div>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Adicionar NF -->

  <!-- SELECT2 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    var logo ='';

    //implementando pesquisa no select
    $('#nota_fiscal_nofCodigo').select2();
    $('#patrimonio_patCodigo').select2();
    $('#unidade_uniCodigo').select2();

    $(function(){
        /*Transformando a imagem em string base64*/
        function toDataURL(url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                    callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob';
                xhr.send();
            }
            toDataURL('http://localhost:8000/imagens/tatame3.png', function(dataUrl) {
                logo = dataUrl
            });


        /*Transformando a imagem em string base64*/
    });

    /*Modal de cadastro para nota fiscal*/
    function abrir(){
        $('#arquivo_error>span').remove();
        $('#descricao_error>span').remove();
        formNF.reset();
        $("#modalNF").modal('show');
    }
    /*Enviando formulário do cadastro de nota fiscal*/
    function submeter() {
            var form = new FormData(formNF);
            $.ajax({
				type: 'POST',
                url: baseurl + '/notaFiscal/cadastrar',
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                data:form,
                processData: false,
                contentType: false,
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta.error != false){
                            $('#arquivo_error>span').remove();
                            if(resposta.error.nofArquivo){
                                $('#arquivo_error').append('<span class="text-danger">'+resposta.error.nofArquivo+'</span>');
                            }
                            $('#descricao_error>span').remove(); 
                            if(resposta.error.nofDescricao){
                                $('#descricao_error').append('<span class="text-danger">'+resposta.error.nofDescricao+'</span>');
                            }
                            $('#modalNF').modal('show');
                        }
                        if(resposta.upload){
                            $('#nota_fiscal_nofCodigo').empty();
                            $('#nota_fiscal_nofCodigo').append('<option value="">Selecione</option>');
                            for(var i=0;i<resposta.tamanho;i++){
                                if(resposta.nota.nofCodigo == resposta.notas[i].nofCodigo){
                                    $('#nota_fiscal_nofCodigo').append('<option selected value="'+resposta.notas[i].nofCodigo+'">'
                                        +resposta.notas[i].nofDescricao+'</option>');
                                }else{
                                    $('#nota_fiscal_nofCodigo').append('<option value="'+resposta.notas[i].nofCodigo+'">'
                                        +resposta.notas[i].nofDescricao+'</option>');
                                    }
                            }
                            $('#modalNF').modal('hide');
                        }else{
                            alert('Ocorreu um erro,tente mais tarde!');
                        }
                        
					}
        });
    }  

    /*Enviando formulário do cadastro de itens de patrimônio*/
    function submeterItem (){
        var form = new FormData(formItem);
            $.ajax({
				type: 'POST',
                url:"{{route('itensPatrimonio.store')}}",
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function () {
                    // adicionando o loader
                    $("#aviso").css('display','');
                }, 
                data:form,
                processData: false,
                contentType: false,
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        $("#aviso").css('display','none');
                        if(resposta.error <= 0 || resposta.error == false){
                            if(resposta.gerou != false){ //verificando se teve algum erro(preenchimento incorreto do form)
                                gerarPDF(resposta.svgs)
                                //limpando os campos
                                $('#itpTipo').val('');
                                $('#itpSituacao').val('Local');
                                $('#itpStatus').val('Ativo');
                                $('#nota_fiscal_nofCodigo').val('').trigger('change');
                                $('#patrimonio_patCodigo').val('').trigger('change');
                                $('#unidade_uniCodigo').val('').trigger('change');
                                $("#unidade_uniCodigo").val('').trigger('change');
                                quantidade.value = '';
                                $('#gerarCodigo').prop("checked", false);
                                $('#patrimonio_error').empty();
                                $('#itpTipo_error').empty();
                                $('#itpSituacao_error').empty();
                                $('#itpStatus_error').empty();
                                $('#unidade_error').empty();
                                $('#nota_error').empty();
                                $('#quantidade_error').empty();
                            }else{
                                window.location.reload();
                            }
                        }else{ // se tem erro, mostra as mensagens
                            //mostrando mensagens de erro
                            if(resposta.error.patrimonio_patCodigo){
                                $('#patrimonio_error>br').remove();
                                $('#patrimonio_error>span').remove();
                                $('#patrimonio_error').append(
                                '<span class="text-danger">'+
                                    resposta.error.patrimonio_patCodigo
                                +'</span><br>')
                            }
                            if(resposta.error.itpTipo){
                                $('#itpTipo_error>br').remove();
                                $('#itpTipo_error>span').remove();
                                $('#itpTipo_error').append('<span class="text-danger">'+
                                    resposta.error.itpTipo
                                +'</span><br>')
                            }

                            if(resposta.error.itpSituacao){
                                $('#itpSituacao_error>br').remove();
                                $('#itpSituacao_error>span').remove();
                                $('#itpSituacao_error').append('<span class="text-danger">'+
                                    resposta.error.itpSituacao
                                +'</span><br>')
                            }

                            if(resposta.error.itpStatus){
                                $('#itpStatus_error>span>br').remove();
                                $('#itpStatus_error>span').remove();
                                $('#itpStatus_error').append('<span class="text-danger">'+
                                    resposta.error.itpStatus
                                +'</span><br>')
                            }

                            if(resposta.error.unidade_uniCodigo){
                                $('#unidade_error>br').remove();
                                $('#unidade_error>span').remove();
                                $('#unidade_error').append('<span class="text-danger">'+
                                    resposta.error.unidade_uniCodigo
                                +'</span><br>')
                            }

                            if(resposta.error.nota_fiscal_nofCodigo){
                                $('#nota_error>br').remove();
                                $('#nota_error>span').remove();
                                $('#nota_error').append('<span class="text-danger">'+
                                    resposta.error.nota_fiscal_nofCodigo
                                +'</span><br>')
                            }

                            if(resposta.error.quantidade){
                                $('#quantidade_error>br').remove();
                                $('#quantidade_error>span').remove();
                                $('#quantidade_error').append('<span class="text-danger">'+
                                    resposta.error.quantidade
                                +'</span><br>')
                            }

                            
                        }
                        
					}
        });
    }
    function gerarPDF(svgs){ // colocando os código de barra no pdf
        
        var column = [];
        var data = [];
        var controle = 0;
        for(var i=0;i<svgs.length;i++){
            controle++;
            var texto = ''+svgs[i].nome+' - '+svgs[i].codigo;
            var conteudo = {
                table: {
                        headerRows: 0,
                        body:[
                            [
                                {image: logo,width:30,height:30,alignment: 'left'},
                                {
                                    table: {
                                        headerRows: 1,
                                        body: [
                                            [{text:texto,style:'header'},],
                                            [{ image: svgs[i].codigoB,height:130,width:80}],
                                        ]
                                    },
                                    layout: 'noBorders'
                                }
                            ]
                        ],
                        noWrap :  true ,
                    },
                layout: 'noBorders',
               };
            //se for o ultimo elemento. E a quantidade impar
            if(controle == svgs.length){
                if(data.length == 0){
                    var coluna = {columns:[conteudo]};
                    column.push(coluna);
                }
            }
            data.push(conteudo);
            if(data.length == 2){
                var coluna = {columns:data};
                column.push(coluna);
                data =[];   
            }
            

            
        }
        var dd = {
                /*content: [ column ],*/
               content:column,
                pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
                    return currentNode.headlineLevel === 1 && followingNodesOnPage.length === 0;
                },

                styles:{
                    header:{
                        fontSize:8,
                        bold:true,
                        alignment:'center'
                    },
                }
            }
        pdfMake.createPdf(dd).download('Codigos de barras');   
    }
        
    
    
</script>


@endsection
