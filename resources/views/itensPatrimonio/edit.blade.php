@extends('layouts.master') @section('title', '- Itens de Patrimônio')@section('patrimonioAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Estilo para tabela -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i>Editar</h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Item de Patrimônio - {{$patrimonio->patNome}}</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <form action="{{route('itensPatrimonio.update',$item->itpCodigo)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="hidden" name="patrimonio_patCodigo" value="{{$patrimonio->patCodigo}}">
                                    <label for="itpTipo">Tipo <span class="text-danger">*</span></label>
                                    <select name="itpTipo" id="itpTipo" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="Doado" @if($item->itpTipo == 'Doado')selected @endif>Doado</option>
                                        <option value="Comprado" @if($item->itpTipo == 'Comprado')selected @endif>Comprado</option>
                                    </select>
                                    @if($errors->get('itpTipo'))
                                        @foreach($errors->get('itpTipo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="itpSituacao">Situação <span class="text-danger">*</span></label>
                                    <select name="itpSituacao" id="itpSituacao" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="Local" @if($item->itpSituacao == 'Local')selected @endif>Local</option>
                                        <option value="Emprestado" @if($item->itpSituacao == 'Emprestado')selected @endif>Emprestado</option>
                                </select>
                                    @if($errors->get('itpSituacao'))
                                        @foreach($errors->get('itpSituacao') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="itpStatus">Status <span class="text-danger">*</span></label>
                                    <select name="itpStatus" id="itpStatus" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="Ativo" @if($item->itpStatus == 'Ativo')selected @endif>Ativo</option>
                                        <option value="Inativo"@if($item->itpStatus == 'Inativo')selected @endif>Inativo</option>
                                        <option value="Manutenção" @if($item->itpStatus == 'Manutenção')selected @endif>Manutenção</option>
                                        <option value="Estragado" @if($item->itpStatus == 'Estragado')selected @endif>Estragado</option>
                                    </select>
                                        @if($errors->get('itpStatus'))
                                            @foreach($errors->get('itpStatus') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="unidade_uniCodigo">Unidade <span class="text-danger">*</span></label>
                                    <select name="unidade_uniCodigo" id="unidade_uniCodigo" class="form-control">
                                        <option value="">Selecione</option>
                                        @foreach ($unidades as $unidade)
                                            <option value="{{$unidade->uniCodigo}}"
                                                @if($unidade->uniCodigo == $item->unidade_uniCodigo)
                                                    selected
                                                @endif>
                                                {{$unidade->uniNome}}
                                            </option>
                                        @endforeach   
                                    </select>
                                        @if($errors->get('unidade_uniCodigo'))
                                            @foreach($errors->get('unidade_uniCodigo') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                </div>
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="form-group">
                                            <label for="nota_fiscal_nofCodigo">Nota Fiscal <span class="text-danger">*</span></label>
                                            <div style="display:flex;">
                                                <select name="nota_fiscal_nofCodigo" id="nota_fiscal_nofCodigo" class="form-control" >
                                                   <option value="">Selecione</option>
                                                    @foreach ($notasFiscais as $nota)
                                                        <option value="{{$nota->nofCodigo}}"
                                                            @if($nota->nofCodigo == $item->nota_fiscal_nofCodigo)
                                                                selected
                                                            @endif>
                                                            {{$nota->nofDescricao}}
                                                        </option>
                                                   @endforeach 
                                                </select>
                                                
                                                <div class="col-lg-1" style="justify content: space beetween; margin-left:15px;" >
                                                    <button type="button" onclick="abrir()" class="btn" id="btnNF" title="Cadastrar Nota Fiscal">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            @if($errors->get('nota_fiscal_nofCodigo'))
                                                @foreach($errors->get('nota_fiscal_nofCodigo') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="itpValorAtual">Valor Atual <span class="text-danger">*</span></label>
                                    <input type="number" name="itpValorAtual" id="itpValorAtual" min="1" class="form-control"
                                    value="{{$item->itpValorAtual}}">
                                    @if($errors->get('itpValorAtual'))
                                        @foreach($errors->get('itpValorAtual') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div><br>
                                </div>
                                <br>
                                <div class="pull-right">
                                    <div class="form-group">
                                    <a href="{{route('patrimonio.itensPatrimonio',$item->patrimonio_patCodigo)}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div> 
        <!--Aba 1-->
    </div>
</div>
<!-- Adicionar NF -->
<div class="modal" tabindex="-1" role="dialog" id="modalNF">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4><i class="fa fa-sticky-note"></i> Nota Fiscal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> &times;</button>
        </div>
            <div class="modal-body">
                <form  method="POST" enctype="multipart/form-data" id="formNF">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="nofArquivo">Arquivo <span class="text-danger">*</span></label>
                                    <input type="file" name="nofArquivo" id="nofArquivo" class="form-control">
                                    <div id="arquivo_error"></div>
                                </div>
                                <div class="form-group">
                                    <label for="nofDescricao">Descrição <span class="text-danger">*</span></label>
                                    <textarea name="nofDescricao" id="nofDescricao" rows="3" class="form-control" placeholder="Um breve descrição sobre a nota fiscal"></textarea>
                                    <div  id="descricao_error"></div>
                                        
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>    
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <div class="form-group">
                        <button type="button" class="btn btn-sm btn-danger mr-3" data-dismiss="modal">Cancelar</button>
                        <button type="button" onclick="submeter()" class="btn btn-sm btn-primary">Cadastrar</button>
                    </div>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Adicionar NF -->
    <!-- SELECT2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        var baseurl = window.location.protocol + '//' + window.location.host + '/';
        //implementando pesquisa no select
        $('#nota_fiscal_nofCodigo').select2();
        $('#unidade_uniCodigo').select2();

        /*Modal de cadastro para nota fiscal*/
        function abrir(){
            $('#arquivo_error>span').remove();
            $('#descricao_error>span').remove();
            formNF.reset();
            $("#modalNF").modal('show');
        }
        /*Enviando formulário do cadastro de nota fiscal*/
        function submeter() {
                var form = new FormData(formNF);
                $.ajax({
                    type: 'POST',
                    url: baseurl + '/notaFiscal/cadastrar',
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'JSON',
                    data:form,
                    processData: false,
                    contentType: false,
                    cache: false,
                        error: function(response) {
                            console.log("Erro:\n"+response);
                        },
                        success: function(resposta) {
                            if(resposta.error != false){
                                $('#arquivo_error>span').remove();
                                if(resposta.error.nofArquivo){
                                    $('#arquivo_error').append('<span class="text-danger">'+resposta.error.nofArquivo+'</span>');
                                }
                                $('#descricao_error>span').remove(); 
                                if(resposta.error.nofDescricao){
                                    $('#descricao_error').append('<span class="text-danger">'+resposta.error.nofDescricao+'</span>');
                                }
                                $('#modalNF').modal('show');
                            }
                            if(resposta.upload){
                                $('#nota_fiscal_nofCodigo').empty();
                                $('#nota_fiscal_nofCodigo').append('<option value="">Selecione</option>');
                                for(var i=0;i<resposta.tamanho;i++){
                                    if(resposta.nota.nofCodigo == resposta.notas[i].nofCodigo){
                                        $('#nota_fiscal_nofCodigo').append('<option selected value="'+resposta.notas[i].nofCodigo+'">'
                                            +resposta.notas[i].nofDescricao+'</option>');
                                    }else{
                                        $('#nota_fiscal_nofCodigo').append('<option value="'+resposta.notas[i].nofCodigo+'">'
                                            +resposta.notas[i].nofDescricao+'</option>');
                                        }
                                }
                                $('#modalNF').modal('hide');
                            }else{
                                alert('Ocorreu um erro,tente mais tarde!');
                            }
                            
                        }
            });
        } 
    </script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>

@endsection
