<!--Modal de exclusão-->
<div class="modal" tabindex="-1" role="dialog" id="modalExclusao">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Formulário de Exclusão</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <br>
          </button>
        </div>
            <input type="hidden" id="codigo">
            <div class="modal-body">
                <p id="modalBody"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-danger" onclick="funcaoExcluir()">Excluir</button>
            </div>
      </div>
    </div>
  </div>
<!--Modal de exclusão -->  