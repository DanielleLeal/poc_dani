<div class="modal" tabindex="-1" role="dialog" id="aviso">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Atenção</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <br>
          </button>
        </div>
            <div class="modal-body">
                <p>O arquivo deve ser uma imagem!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
      </div>
    </div>
  </div>