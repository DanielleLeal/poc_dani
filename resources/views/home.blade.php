@extends('layouts.master')
@section('title', '- Home')
@section('principalAtivo','bg-danger')
@section('principal') @parent

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Estilo css-->
<link rel="stylesheet" href="{{asset('css/home.css')}}">

<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.load('current', {'packages':['corechart']});

     $(function(){
      var baseurl = window.location.protocol + '//' + window.location.host + '/';
     $.ajax({
            type: 'GET',
            url: baseurl + 'home/unidade',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
                error: function(response) {
                    console.log("Erro:\n"+response);
                },
                success: function(resposta) {
                  google.charts.setOnLoadCallback(drawBasic(resposta.unidades));
                },
      });
     });

function drawBasic(dados) {

      var data = google.visualization.arrayToDataTable([[
                {label: "Unidade", type: "string"},{label: "Quantidade", type: "number"}
              ]]);
      if(dados.length == 0){
        data.addRows([['Nenhum patrimônio',0]])
      }
      for(var i=0;i<dados.length;i++){
          data.addRows([[dados[i].uniNome,dados[i].qtde]])
      }

      var options = {
        title: 'Quantidade de patrimônios por unidade',
        chartArea: {width: '50%'},
        hAxis: {
          title: 'Total de Patrimônios',
          minValue: 0
        },
        vAxis: {
          title: 'Unidades'
        }
      };

      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    $(function(){
      var baseurl = window.location.protocol + '//' + window.location.host + '/';
      $.ajax({
            type: 'GET',
            url: baseurl + 'home/categoria',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
                error: function(response) {
                    console.log("Erro:\n"+response);
                },
                success: function(resposta) {
                  google.charts.setOnLoadCallback(drawChart(resposta.categorias));
                },
      });
    });
    
  
    function drawChart(dados) {
          
              var data = google.visualization.arrayToDataTable([[
                {label: "Categoria", type: "string"},{label: "Quantidade", type: "number"}
              ]]);
              if(dados.length == 0){
                data.addRows([['Nenhum patrimônio',100]])
              }
              for(var i=0;i<dados.length;i++){
               data.addRows([[dados[i].catNome,dados[i].qtde]])
              }

              var options = {
                  title: 'Quantidade de patrimônios por categoria'
              };
        
                var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        
                chart.draw(data, options);
        }
  </script>
  <div class="container-fluid cont">
    <div class="row">
        <div class="col-lg-12">
          <p  class="font"><i class="fa fa-home"></i> {{Auth::user()->useNome}}, seja bem-vindo(a) </p>
        </div>
        <div class="col-lg-6" >
            <div class="card">
              <div class="card-body bg-white">
                <h5 class="card-title">Patrimônios por unidade</h5>
                <div class="container-fluid">
                  <div class="row">
                        <div class="chart" id="chart_div"></div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class=" col-lg-6">
          <div class="card">
              <div class="card-body bg-white">
                <h5 class="card-title">Patrimônios por categoria</h5>
                <div class="container-fluid">
                  <div id="piechart" class="chart"></div>
                </div>
              </div>
          </div>
      </div>
      <div class="col-lg-12 ">
          <div class="bloco" style="padding: 20px;">
            <h5 class="card-title">Devoluções Atrasadas</h5>
            <div class="table-responsive">
              <table class="table table-hover" id="emprestimo-table">
                <thead>
                  <tr>
                    <th>Data da Devolução</th>
                    <th>Status</th>
                    <th>Empresa</th>
                    <th>Quantidade de Itens</th>
                    <th>Responsável</th>
                    <th>Opção</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
</div>


<!-- Modal para avisar o usuário logado que ele não pode realizar determinada ação -->
<div class="modal" tabindex="-1" role="dialog" id="modalAtencao">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-danger">Atenção</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Sua função no sistema não lhe permite realizar essa ação!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
@if (Session::has('success'))
  <script type="text/javascript">
      $(function(){
        $('#modalAtencao').modal('show');
      });
  </script>
@endif
<script type="text/javascript">
        function formatarDate(data){
            var ano = data.substr(0,4);
            var mes = data.substr(5,2);
            var dia = data.substr(8,2);
            var dataF = dia+'/'+mes+'/'+ano;
            return dataF;
        }
  //preenchendo o datatable
  jQuery(function() {
            jQuery('#emprestimo-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("home.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                            data: 'empDataDevolucao',
                            render: function(data) {
                                return formatarDate(data);
                                
                            }, 
                            name: 'Data da Devolução'
                    },
                    {
                      data: 'empStatus',
                      render: function(data) {
                        return '<span class="badge badge-danger" style="border-radius:.25rem;">Atrasado</span>';
                      },
                      name: 'Status'
                    },
                    {
                        data: 'fornecedor.forNome',
                        name: 'Empresa'
                    },
                    {
                        data: 'empQuantidade',
                        name: 'Quantidade de Itens'
                    },
                    {
                        data: 'user.useNome',
                        name: 'Responsável'
                    },
                    {
                        data: "empCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="/devolucao/itens/'+data+'" class="btn btn-primary btn-sm">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Realizar Baixa</a></p></div>';
                            return html;
                        },
                        name: 'Opção'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                  {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2,3,4] 
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2,3,4] 
                        }
                    },
                ]
            });
        });
</script>

@endsection





