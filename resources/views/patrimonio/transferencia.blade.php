@extends('layouts.master') @section('title', '- Transferência')@section('patrimonioAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Estilo tabela -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-share" aria-hidden="true"></i> Transferência de Patrimônios </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Transferência</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active " id="tab_1">
            <div class="box-body box-success">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <form action="{{route('transferencia.store')}}" id="formTrans" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="unidade">Transferir para: <span class="text-danger">*</span></label></br>
                                    <select name="unidade" id="unidade" class="form-control">
                                        <option value="">Selecione</option>
                                        @foreach ($unidades as $uni)
                                            <option value="{{$uni->uniCodigo}}">{{$uni->uniNome}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->get('unidade'))
                                        @foreach($errors->get('unidade') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="patrimonio">Selecione o patrimônio<span class="text-danger">*</span></label></br>
                                    <select name="patrimonio" id="patrimonio" class="form-control" onchange="carregarItens(1,this.value)">
                                        <option value="0">Selecione</option>
                                        @foreach ($patrimonios as $pat)
                                            <option value="{{$pat->patCodigo}}">{{$pat->patNome}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <label for="itens-table">Selecione os itens que deseja transferir: <span class="text-danger">*</span></label>
                                    <input type="hidden" name="itensT[]" id="itensT"><br>
                                    @if($errors->get('itensT'))
                                        @foreach($errors->get('itensT') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12">
                            <div class="row " style="float: right;">
                                <div class="col-12">
                                    <label for="pesquisa">Pesquisar</label>
                                    <input type="search" id="pesquisa" name="pesquisa">
                                </div>
                                <div class="col-12">
                                    <div class="checkbox">
                                        <label for="tr">
                                            <input type="checkbox" id="tr" name="tr" onclick="transferirTodos()">
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                             Selecionar Todos
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                            
                        
                        <div class="col-lg-12" style="margin-top:5px;">
                            <div class="table-responsive">
                                <table class="table table-hover " id="itens-table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Código de Barras</th>
                                            <th class="text-center">Unidade</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Situação</th>
                                            <th class="text-center">Tipo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center"  colspan="6">Nenhum patrimônio selecionado</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div> 
                                    <span id="card-title" style="display: none;"></span>
                                    <span class="pull-right">
                                        <nav id="pagination"  style="display: none;">
                                            <ul class="pagination">
                                            </ul>
                                        </nav>
                                    </span>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <br>
                            <div class="pull-right">
                                <div class="form-group">
                                    <a href="{{route('transferencia.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                    <button type="button" onclick="submeter()" class="btn btn-sm btn-primary">Transferir</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                      
            </div>  
        </div>
        <!--Aba 1--> 
    </div>
</div>
</br>
    <!-- SELECT2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/transferencia.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
