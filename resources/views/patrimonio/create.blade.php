@extends('layouts.master') @section('title', '- Patrimônio')@section('patrimonioAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">
<!-- Estilo para tabela -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-institution"></i> Patrimônios </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Patrimônios</a></li>
        <li  id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                                <div class="pull-left">
                                    <label for="">Categoria</label>
                                    <select name="categoria" id="categoria" class="select"
                                    onchange="filtrar(this.value)">
                                        <option value="0">Todos</option>
                                        @foreach ($categorias as $cat)
                                            <option value="{{$cat->catCodigo}}">{{$cat->catNome}}</option>
                                        @endforeach
                                    </select>
                                    <a class ="export"href="{{route('patrimonio.export')}}">Excel</a>
                                </div>
                                <div class="pull-right">
                                    <label for="pesquisar">Pesquisar</label>
                                    <input type="search" name="pesquisar" id="pesquisar">
                                </div>
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table table-hover" id="patrimonio-table" style="margin-top: 20px;">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Nome</th>
                                                <th>Categoria</th>
                                                <th>Fornecedor</th>
                                                <th>Quantidade</th>
                                                <th>Valor Compra</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        </div>
                                    </table>
                                    <div> 
                                        <span id="card-title"></span>
                                        <span class="pull-right">
                                            <nav id="paginationNav">
                                                <ul class="pagination">
                                                </ul>
                                            </nav>
                                        </span>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane" id="tab_2">
            <div class="box-body box-success">
                <form action="{{ route('patrimonio.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-2 text-center  text-sm-left">
                                <img src="{{asset('imagens/default-patrimonio.png')}}" class="img-circle user-image"
                                id="img" height="130" width="130" alt="Imagem padrão de patrimônio">
                                <label for="patFoto"><i class="fa fa-camera"></i></label>
                                <input type="file" name="patFoto" id="patFoto" style="display:none;">
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="patNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="patNome" name="patNome" placeholder="Ex: cadeira">
                                        @if($errors->get('patNome'))
                                            @foreach($errors->get('patNome') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                    </div>
                                    <div class="form-group">
                                        <label for="patValorCompra">Valor de Compra (Unitário) <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="patValorCompra" name="patValorCompra" placeholder="Ex: 15,00">
                                        @if($errors->get('patValorCompra'))
                                            @foreach($errors->get('patValorCompra') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="categoria_catCodigo">Categoria <span class="text-danger">*</span></label>
                                        <select style="width: 100%;" name="categoria_catCodigo" id="categoria_catCodigo" class="form-control">
                                            <option value="0">Selecione</option>
                                             @foreach ($categorias as $categoria)
                                            <option value="{{$categoria->catCodigo}}">
                                                {{$categoria->catNome}}
                                            </option>
                                             @endforeach   
                                        </select>
                                        @if($errors->get('categoria_catCodigo'))
                                            @foreach($errors->get('categoria_catCodigo') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                    </div>
                                    <div class="form-group">
                                        <label for="fornecedor_forCodigo">Fornecedor <span class="text-danger">*</span></label>
                                        <select style="width: 100%;" name="fornecedor_forCodigo" id="fornecedor_forCodigo" class="form-control">
                                            <option value="0">Selecione</option>
                                             @foreach ($fornecedores as $fornecedor)
                                            <option value="{{$fornecedor->forCodigo}}">
                                                {{$fornecedor->forNome}}
                                            </option>
                                             @endforeach   
                                        </select>
                                        @if($errors->get('fornecedor_forCodigo'))
                                            @foreach($errors->get('fornecedor_forCodigo') as $error)
                                                <span class="text-danger">{{ $error }}</span><br>
                                            @endForeach
                                        @endIf
                                        <div class="pull-right" style="margin-bottom:5px;">
                                            <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                        </div><br>
                                    </div>
                                    <div class="pull-right">
                                        <div class="form-group">
                                            <a href="{{route('patrimonio.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                            <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </form>
            </div>  
        </div>
        <!--Aba 2--> 
    </div>
</div>
<br>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão --> 

<!--Modal aviso sobre a foto-->
@component('components.aviso')
@endcomponent
<!--Modal --> 

<!-- Controle das tabs -->
@if(count($errors)>0)
    <script type="text/javascript">
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
    </script>
@else
<script type="text/javascript">
    $('#tab_1').addClass('active');
    $('#li_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#li_2').removeClass('active');
</script>
@endif
<!-- Controle das tabs -->

    <!-- SELECT2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <!-- Preenchimento da tabela,paginação,filtro,função exluir e abrir modais -->
    <script type="text/javascript" src="{{asset('js/patrimonios.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
