@extends('layouts.master')
@section('title', '- Deprecição')
@section('depreciacaoAtivo','bg-danger')
@section('principal')
@parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-calculator" style="font-weight:600px;"></i> Depreciação dos Patrimônios </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Depreciação </a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <form action="{{route('depreciacao.store')}}" method="POST">
                        @csrf
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="categoria">Categorias<span class="text-danger">*</span></label>
                                <select name="categoria" id="categoria" class="form-control" onchange="buscarInfo(this.value)">
                                    <option value="">Selecione</option>
                                    @foreach ($categorias as $cat)
                                        <option value="{{$cat->catCodigo}}">{{$cat->catNome}}</option>
                                    @endforeach
                                </select>
                                @if($errors->get('categoria'))
                                     @foreach($errors->get('categoria') as $error)
                                        <span class="text-danger">{{ $error }}</span><br>
                                    @endForeach
                                @endIf
                            </div>
                            <div class="form-group">
                                <label for="catVidaUtil">Vida útil<span class="text-danger">*</span></label>
                                <input type="number" name="catVidaUtil" id="catVidaUtil" class="form-control">
                                @if($errors->get('catVidaUtil'))
                                     @foreach($errors->get('catVidaUtil') as $error)
                                        <span class="text-danger">{{ $error }}</span><br>
                                    @endForeach
                                @endIf
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="depAno">Ano<span class="text-danger">*</span></label>
                                <input type="number" name="depAno" id="depAno" class="form-control">
                                @if($errors->get('depAno'))
                                     @foreach($errors->get('depAno') as $error)
                                        <span class="text-danger">{{ $error }}</span><br>
                                    @endForeach
                                @endIf
                                <div class="pull-right" style="margin-bottom:5px;">
                                    <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                </div><br>
                            </div>
                            <div class="pull-right">
                                <div class="form-group">
                                    <a href="{{route('depreciacao.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                    <button type="submit" class="btn btn-sm btn-primary">Aplicar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>   
        </div> 
        <!--Aba 1-->
    </div>
</div>
     <!-- SELECT2 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
     <script type="text/javascript">
      var baseurl = window.location.protocol + '//' + window.location.host + '/';
         $("#categoria").select2();

         $(function(){
             var now = new Date();
             depAno.value = now.getFullYear();
         })

         function buscarInfo(catCodigo){
            $.ajax({
				type: 'GET',
				url: baseurl + 'depreciacao/'+catCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            catVidaUtil.value = resposta.categoria.catVidaUtil;
                           
                        }else{
                            alert('Não é possível realizar a deprecicao,tente mais tarde!');
                        }
					}
            });
         }
     </script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
