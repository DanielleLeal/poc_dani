@extends('layouts.master') @section('title', '- Patrimônio')@section('patrimonioAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i> Editar </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active" id="li_1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active " id="tab_1">
            <div class="box-body box-success">
            <form action="{{ route('patrimonio.update',$patrimonio->patCodigo) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-2 text-center  text-sm-left">
                            @if($patrimonio->patFoto == 'imagens/default-patrimonio.png' || $patrimonio->patFoto == null)
                                    <img src="{{asset('imagens/default-patrimonio.png')}}" alt="Imagem padrão de patrimônios" class="user-image"
                                    height="120" width="120" id="img">
                                @else
                                    <img src="{{asset('storage/'.$patrimonio->patFoto)}}" class="user-image" id="img" height="120" width="120" alt="Imagem do patrimônio">
                                    
                                @endif
                            <label for="patFoto"><i class="fa fa-camera"></i></label>
                            <input type="file" name="patFoto" id="patFoto" style="display:none;">
                            <div class="checkbox">
                                <label for="tirar_foto">
                                 <input type="checkbox" id="tirar_foto" name="tirar_foto" onchange="tirarFoto(this)">
                                 <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    Retirar foto
                                 </label>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="patNome">Nome <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="patNome" name="patNome" value="{{$patrimonio->patNome}}">
                                    @if($errors->get('patNome'))
                                        @foreach($errors->get('patNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                            </div>
                            <div class="form-group">
                                <label for="patValorCompra">Valor de Compra <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="patValorCompra" name="patValorCompra" value="{{str_replace('.',',', $patrimonio->patValorCompra)}}">
                                @if($errors->get('patValorCompra'))
                                    @foreach($errors->get('patValorCompra') as $error)
                                        <span class="text-danger">{{ $error }}</span><br>
                                    @endForeach
                                @endIf
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="categoria_catCodigo">Categoria <span class="text-danger">*</span></label>
                                <select name="categoria_catCodigo" id="categoria_catCodigo" class="form-control">
                                    <option value="0">Selecione</option>
                                        @foreach ($categorias as $categoria)
                                    <option value="{{$categoria->catCodigo}}"
                                            @if ($categoria->catCodigo == $patrimonio->categoria_catCodigo)
                                            selected
                                            @endIf>
                                            {{$categoria->catNome}}
                                        </option>
                                         @endforeach   
                                    </select>
                                    @if($errors->get('categoria_catCodigo'))
                                        @foreach($errors->get('categoria_catCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                            </div>
                            <div class="form-group">
                                <label for="fornecedor_forCodigo">Fornecedor <span class="text-danger">*</span></label>
                                <select name="fornecedor_forCodigo" id="fornecedor_forCodigo" class="form-control">
                                    <option value="0">Selecione</option>
                                    @foreach ($fornecedores as $fornecedor)
                                        <option value="{{$fornecedor->forCodigo}}"
                                            @if($fornecedor->forCodigo == $patrimonio->fornecedor_forCodigo) 
                                            selected
                                            @endIf>
                                            {{$fornecedor->forNome}}
                                        </option>
                                    @endforeach   
                                </select>
                                @if($errors->get('fornecedor_forCodigo'))
                                    @foreach($errors->get('fornecedor_forCodigo') as $error)
                                        <span class="text-danger">{{ $error }}</span><br>
                                    @endForeach
                                @endIf
                                <div class="pull-right" style="margin-bottom:5px;">
                                    <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                </div>
                            </div><br>
                            <div class="pull-right">
                                <div class="form-group">
                                    <a href="{{route('patrimonio.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                    <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                        
            </div>  
        </div>
        <!--Aba 1--> 

    </div>
</div>
<!--Modal aviso sobre a foto-->
@component('components.aviso')
@endcomponent
<!--Modal -->  

    <!-- SELECT2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        $('#categoria_catCodigo').select2();
        $('#fornecedor_forCodigo').select2();
        patValorCompra.onfocus = function(){
            $('#patValorCompra').mask('000.000.000.000.000,00', {reverse: true});
        }


        //preview da imagem antes do upload
        $('#patFoto').change(function(){
        const file = $(this)[0].files[0];// acessando o nome do arquivo selecionado
        const fileReader = new FileReader();
        fileReader.onloadend = function(){ // lendo o arquivo
            if(file.type.match('image.*')){
                $('#img').attr('src',fileReader.result);
            }else{
                $('#aviso').modal('show');
            }
                
        }
        fileReader.readAsDataURL(file); // lendo o arquivo
        });

        function tirarFoto(obj){
            if(obj.checked)
            $('#img').attr('src','{{asset("imagens/default-patrimonio.png")}}');
        }
    </script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
