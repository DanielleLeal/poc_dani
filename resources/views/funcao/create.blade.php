@extends('layouts.master') @section('title', '- Função')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<h3 style="margin-left:10px;"><img src="{{asset('imagens/user-shield.png')}}" height="30" width="30">Funções </h3>

<script>
    // redireciona para a guia específica
    /*$(document).ready(function () {
        $('#tab_1').removeClass('active');
        $('#tab_2').addClass('active');
    });*/
    </script>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" id="tabMenu">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Funções</a></li>
        <li><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                            <div class="table-responsive">
                                <table class="table table-hover " id="funcao-table">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Descrição </th>
                                            <th>Opções</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <!--Aba 1--> 
        <!--Aba 2-->
        <div class="tab-pane" id="tab_2">
            <div class="box-body box-success">
                <form action="{{route('funcao.store')}}" method="POST">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="funNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="funNome" name="funNome" placeholder="Ex: administrador">
                                    @if($errors->get('funNome'))
                                        @foreach($errors->get('funNome') as $error)
                                            <span class="text-danger">{{ $error }}</span>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="funDescricao">Descrição </label>
                                    <textarea class="form-control" rows="3" placeholder="Breve descrição sobre a função do usuário" name="funDescricao"></textarea>
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br> 
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('funcao.index')}}" class="btn btn-sm btn-danger mr-3">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 2-->
    </div>
</div>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão -->    

<script type="text/javascript">
    var baseurl = window.location.protocol + '//' + window.location.host + '/';

    //preenchendo a tabela
    jQuery(function() {
            jQuery('#funcao-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("funcao.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                        data: 'funNome',
                        name: 'Nome'
                    },
                    {
                        data: 'funDescricao',
                        name: 'Descrição'
                    },
                    {
                        data: "funCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="funcao/' + data + '/edit" class="btn btn-primary btn-block">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-6">';
                            html += '<p><button type="button" class="btn btn-danger btn-block" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    'pdfHtml5',
                    'excelHtml5'
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var funCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'funcao/'+funCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.funcao.funCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir a função "+ resposta.funcao.funNome+"?";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
        }  
            
        //mandando pro metodo destroy
        function funcaoExcluir(){
            var funCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "funcao/"+funCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload('#tab_2');
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
        }
    </script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
