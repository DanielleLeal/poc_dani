@extends('layouts.master') @section('title', '- Função')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i>  Editar Função </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="box-body box-success">
            <form action="{{route('funcao.update',$funcao->funCodigo)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="funNome">Nome <span class="text-danger">*</span></label>
                                <input type="text" required class="form-control" id="funNome" name="funNome" value="{{$funcao->funNome}}">
                                </div>
                                <div class="form-group">
                                    <label for="funDescricao">Descrição </label>
                                    <textarea class="form-control" rows="3" name="funDescricao">{{$funcao->funDescricao}}</textarea>
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br> 
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('funcao.index')}}" class="btn btn-sm btn-danger mr-3">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                </form>                         
            </div>  
        </div>
        <!--Aba 1-->
    </div>
</div>


 <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
		



@endsection
