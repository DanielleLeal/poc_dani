@extends('layouts.master') @section('title', '- Categoria')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<h3 style="margin-left:10px;"><i class="fa fa-bookmark"></i> Categorias </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Categorias</a></li>
        <li id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
        
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                                <div class="table-responsive">
                                    <table class="table table-hover " id="categoria-table">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Vida Útil (anos)</th>
                                                <th>Descrição</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane " id="tab_2">
            <div class="box-body box-success">
                <form action="{{ route('categoria.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="catNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="catNome" name="catNome" placeholder="Digite o nome da categoria">
                                    @if($errors->get('catNome'))
                                        @foreach($errors->get('catNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="catDescricao">Descrição</label>
                                    <textarea  placeholder="Breve descrição sobre a categoria" name="catDescricao" id="catDescricao"rows="5" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="catVidaUtil">Vida útil (em anos) <span class="text-danger">*</span></label>
                                    <input type="number" name="catVidaUtil" id="catVidaUtil" class="form-control" placeholder="Vida útil em anos da categoria">
                                    @if($errors->get('catVidaUtil'))
                                        @foreach($errors->get('catVidaUtil') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('categoria.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 2-->   
    </div>
</div>
<br>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão --> 

<!-- Controle das tabs -->
@if(count($errors)>0)
    <script type="text/javascript">
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
    </script>
@else
<script type="text/javascript">
    $('#tab_1').addClass('active');
    $('#li_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#li_2').removeClass('active');
</script>
@endif
<!-- Controle das tabs -->

<script type="text/javascript"> 
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    
    jQuery(function() {
            jQuery('#categoria-table').DataTable({ 
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("categoria.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                        data: 'catNome',  
                        name: 'Nome'
                    },
                    {
                        data: 'catVidaUtil',
                        name: 'Vida útil'
                    },
                    {
                        data: 'catDescricao',
                        name: 'Descrição'
                    },
                    {
                        data: "catCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="categoria/' + data + '/edit" class="btn btn-primary btn-block">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-6">';
                            html += '<p><button type="button" class="btn btn-danger btn-block" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2] 
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2] 
                        }
                    },
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var catCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'categoria/'+catCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.categoria.catCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir a categoria <strong>"+ resposta.categoria.catNome+"</strong>?";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
        }  
            
        //mandando pro metodo destroy
        function funcaoExcluir(){
            var catCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "categoria/"+catCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload('#tab_2');
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
        }
    </script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
