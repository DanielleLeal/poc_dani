@extends('layouts.master') @section('title', '- Categoria')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i> Editar Categoria </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
        
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="box-body box-success">
                <form action="{{ route('categoria.update',$categoria->catCodigo) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="catNome">Nome <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="catNome" name="catNome" value="{{$categoria->catNome}}">
                                    @if($errors->get('catNome'))
                                        @foreach($errors->get('catNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="catDescricao">Descrição</label>
                                    <textarea name="catDescricao" id="catDescricao"rows="5" 
                                    class="form-control">{{$categoria->catDescricao}}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="catVidaUtil">Vida útil (em anos) <span class="text-danger">*</span></label>
                                    <input type="number" name="catVidaUtil" id="catVidaUtil" class="form-control" value="{{$categoria->catVidaUtil}}">
                                    @if($errors->get('catVidaUtil'))
                                        @foreach($errors->get('catVidaUtil') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('categoria.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 1-->   
    </div>
</div>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
