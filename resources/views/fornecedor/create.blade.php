@extends('layouts.master') @section('title', '- Fornecedor')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<!-- Modal ver mais -->
<div class="modal" tabindex="-1" role="dialog" id="modalVer">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Informações do Fornecedor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for=""><i class="fa fa-arrow-right"></i> Telefone:</label>
                        <span class="font15"  id="mTelefone"></span>
                    </div>
                    <div class="form-group">
                        <label for=""><i class="fa fa-arrow-right"></i> Rua: </label>
                        <span class="font15"  id="mRua"></span>
                    </div>
                    <div class="form-group">
                        <label for=""><i class="fa fa-arrow-right"></i> Número: </label>
                        <span class="font15" id="mNumero"></span>
                    </div>
                    <div class="form-group">
                        <label for=""><i class="fa fa-arrow-right"></i> Bairro: </label>
                        <span class="font15"  id="mBairro"></span>
                    </div>
                    <div class="form-group">
                        <label for=""><i class="fa fa-arrow-right"></i> CEP: </label>
                        <span class="font15"  id="mCEP"></span>
                    </div>
                    <div class="form-group">
                        <label for=""><i class="fa fa-arrow-right"></i> ESTADO: </label>
                        <span class="font15"  id="mEstado"></span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
<!-- Modal ver mais -->

<h3 style="margin-left:10px;"><i class="fa fa-truck"></i> Fornecedores </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li id="li_1" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Fornecedores</a></li>
        <li id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <form role="form">
                <div class="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                                <div class="table-responsive">
                                    <table class="table table-hover " id="fornecedor-table">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>CPF/CNPJ</th>
                                                <th>E-mail</th>
                                                <th>Tipo</th>
                                                <th>Cidade</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> 
        <!--Aba 1-->
        <!--Aba 2-->
        <div class="tab-pane " id="tab_2">
            <div class="box-body box-success">
                <form action="{{ route('fornecedor.store') }}" method="POST">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="forNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="forNome" name="forNome" placeholder="Digite o nome">
                                    @if($errors->get('forNome'))
                                        @foreach($errors->get('forNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="forEmail">E-mail <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="forEmail" name="forEmail" placeholder="Ex: empresa@gmail.com">
                                    @if($errors->get('forEmail'))
                                        @foreach($errors->get('forEmail') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="forCPF">CPF/CNPJ <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="forCPF" name="forCPF" placeholder="000.000.000-00 ou 00.000.000/0000-00">
                                    @if($errors->get('forCPF'))
                                        @foreach($errors->get('forCPF') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="forTelefone">Telefone</label>
                                            <input type="text" class="form-control" id="forTelefone" name="forTelefone" placeholder="0000-0000">
                                            @if($errors->get('forTelefone'))
                                                @foreach($errors->get('forTelefone') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="forCelular">Celular<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="forCelular" name="forCelular" placeholder="(00) 0 0000-0000">
                                            @if($errors->get('forCelular'))
                                                @foreach($errors->get('forCelular') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="forStatus">Tipo <span class="text-danger">*</span></label>
                                    <select name="forStatus" id="forStatus" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="Parceiro">Parceiro</option>
                                        <option value="Fornecedor">Fornecedor</option>
                                    </select>
                                    @if($errors->get('forStatus'))
                                        @foreach($errors->get('forStatus') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="endRua">Rua <span class="text-danger">*</span></label>
                                            <input type="endRua" name="endRua" id="endRua" placeholder="Digite o nome da rua" class="form-control">
                                            @if($errors->get('endRua'))
                                                @foreach($errors->get('endRua') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="endNumero">Número <span class="text-danger">*</span></label>
                                            <input type="number" name="endNumero" id="endNumero"class="form-control" placeholder="Ex: 380">
                                            @if($errors->get('endNumero'))
                                                @foreach($errors->get('endNumero') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label for="endBairro">Bairro <span class="text-danger">*</span></label>
                                    <input type="endBairro" name="endBairro" id="endBairro" placeholder="Digite o nome do bairro" class="form-control">
                                    @if($errors->get('endBairro'))
                                        @foreach($errors->get('endBairro') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="endCEP">CEP</label>
                                    <input type="text" name="endCEP" id="endCEP" class="form-control" placeholder="00000-000">
                                    @if($errors->get('endCEP'))
                                        @foreach($errors->get('endCEP') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="estado_estCodigo">Estado <span class="text-danger">*</span></label>
                                    <select name="estado_estCodigo" style="width: 100%;" id="estado_estCodigo" class="form-control">
                                        <option value="">Selecione</option>
                                        @foreach ($estados as $estado)
                                            <option value="{{$estado->estCodigo}}">
                                                {{$estado->estEstado}} - {{$estado->estSigla}}
                                            </option>
                                        @endforeach
                                    </select>    
                                    @if($errors->get('estado_estCodigo'))
                                        @foreach($errors->get('estado_estCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="cidade_cidCodigo">Cidade <span class="text-danger">*</span></label>
                                    <select name="cidade_cidCodigo" style="width: 100%;" id="cidade_cidCodigo" class="form-control">
                                        <option value="">Selecione</option>
                                    </select>
                                    @if($errors->get('cidade_cidCodigo'))
                                        @foreach($errors->get('cidade_cidCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                </div>
                                <br>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('fornecedor.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 2-->   
    </div>
</div>
<br>

<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão -->

<!-- Controle das tabs -->
@if(count($errors)>0)
    <script type="text/javascript">
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
    </script>
@else
<script type="text/javascript">
    $('#tab_1').addClass('active');
    $('#li_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#li_2').removeClass('active');
</script>
@endif
<!-- Controle das tabs -->



<!-- SELECT2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">  
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    //mascara para os campos
    $('#endCEP').mask("00000-000");
    $('#forTelefone').mask("0000-0000");
    $('#forCelular').mask("(00) 0 0000-0000");

    var options = {
        onKeyPress: function (cpf, ev, el, op) {
            var masks = ['000.000.000-000', '00.000.000/0000-00'];
            $('#forCPF').mask((cpf.length > 14) ? masks[1] : masks[0], op);
        }
    }

    $('#forCPF').length > 11 ? $('#forCPF').mask('00.000.000/0000-00', options) : $('#forCPF').mask('000.000.000-00#', options);
    // Select 2
    $('#estado_estCodigo').select2();
    $('#cidade_cidCodigo').select2();

    

    //preenchendo o select de cidade de acordo com o estado escolhido
    $('select[name=estado_estCodigo]').change(function () {
            var estCodigo = $(this).val();
            $.ajax({
                type: 'GET',
                url:baseurl+ "unidade/cidade/"+estCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        $('#cidade_cidCodigo').empty();
                        for(var i=0; i <resposta.tamanho;i++){
                            $('#cidade_cidCodigo').append('<option value=' + resposta.cidades[i].cidCodigo + '>' + resposta.cidades[i].cidCidade + '</option>');
                        }
                        
                    }		
                });

    });        
    //preenchendo o datatable
    jQuery(function() {
            jQuery('#fornecedor-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("fornecedor.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                        data: 'forNome',
                        name: 'Nome'
                    },
                    {
                        data: 'forCPF',
                        name: 'CPF'
                    },
                    {
                        data: 'forEmail',
                        name: 'Email'
                    },
                    {
                        data: 'forStatus',
                        name: 'Tipo'
                    },
                    {
                        data: 'endereco.cidade.cidCidade',
                        name: 'Cidade'
                    },
                    {
                        data: "forCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-4">';
                            html += '<p><button type="button" class="btn btn-success btn-md" onclick="verMais('+data+')">';
                            html += '<i class="fa fa-eye"></i>&nbsp; Ver mais</button></p></div>';
                            html += '<div class="col-md-4">';
                            html += '<p><a href="fornecedor/' + data + '/edit" class="btn btn-primary btn-md">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-4">';
                            html += '<p><button type="button" class="btn btn-danger btn-md" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2,3,4,5] 
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2,3,4,5] 
                        }
                    },
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var forCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'fornecedor/'+forCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.fornecedor.forCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir o fornecedor <strong>"+ resposta.fornecedor.forNome+"</strong>?";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
        }  
            
        //mandando pro metodo destroy
        function funcaoExcluir(){
            var forCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "fornecedor/"+forCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload();
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
        }

        function verMais(forCodigo){
            $.ajax({
				type: 'GET',
				url: baseurl + 'fornecedor/verMais/'+forCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            var fornecedor = resposta.fornecedor;
                            if(fornecedor.forTelefone != null){
                                mTelefone.innerHTML = fornecedor.forTelefone;
                            }else{
                                mTelefone.innerHTML = '-';
                            }
                            
                            mRua.innerHTML = fornecedor.endereco.endRua;
                            mNumero.innerHTML = fornecedor.endereco.endNumero;
                            mBairro.innerHTML = fornecedor.endereco.endBairro;
                            mEstado.innerHTML = fornecedor.endereco.estado.estEstado;
                            if(fornecedor.endereco.endCEP != null){
                                mCEP.innerHTML = fornecedor.endereco.endCEP; 
                            }else{
                                mCEP.innerHTML = '-'; 
                            }
                            
                            $('#modalVer').modal('show');	
                        }else{
                            alert('Não é possível ver mais informações');
                        }
					}
            });
        }
    </script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
