@extends('layouts.master') @section('title', '- Fornecedor')@section('cadastrosdiversosAtivo','bg-danger') @section('principal') @parent

<!--Ajax-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--Mascara-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<!-- Select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('css/select2.css')}}">

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i>Editar Fornecedor </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="box-body box-success">
                <form action="{{ route('fornecedor.update',$fornecedor->forCodigo) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="forNome">Nome <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="forNome" name="forNome"value="{{$fornecedor->forNome}}">
                                    @if($errors->get('forNome'))
                                        @foreach($errors->get('forNome') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="forEmail">E-mail <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="forEmail" name="forEmail" value="{{$fornecedor->forEmail}}">
                                    @if($errors->get('forEmail'))
                                        @foreach($errors->get('forEmail') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="forCPF">CPF/CNPJ <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="forCPF" name="forCPF" value="{{$fornecedor->forCPF}}">
                                    @if($errors->get('forCPF'))
                                        @foreach($errors->get('forCPF') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="forTelefone">Telefone</label>
                                            <input type="text" class="form-control" id="forTelefone" name="forTelefone" value="{{$fornecedor->forTelefone}}">
                                            @if($errors->get('forTelefone'))
                                                @foreach($errors->get('forTelefone') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="forCelular">Celular<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="forCelular" name="forCelular" value="{{$fornecedor->forCelular}}">
                                            @if($errors->get('forCelular'))
                                                @foreach($errors->get('forCelular') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="forStatus">Tipo <span class="text-danger">*</span></label>
                                    <select name="forStatus" id="forStatus" class="form-control">
                                        <option value="0">Selecione</option>
                                        <option value="Parceiro" @if($fornecedor->forStatus == 'Parceiro')selected @endIf>
                                            Parceiro
                                        </option>
                                        <option value="Fornecedor" @if($fornecedor->forStatus == 'Fornecedor')selected @endIf>
                                            Fornecedor
                                        </option>
                                    </select>
                                    @if($errors->get('forStatus'))
                                        @foreach($errors->get('forStatus') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="endRua">Rua <span class="text-danger">*</span></label>
                                            <input type="endRua" name="endRua" id="endRua" value="{{$fornecedor->endereco->endRua}}" class="form-control">
                                            @if($errors->get('endRua'))
                                                @foreach($errors->get('endRua') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="endNumero">Número <span class="text-danger">*</span></label>
                                            <input type="number" name="endNumero" id="endNumero"class="form-control" value="{{$fornecedor->endereco->endNumero}}" >
                                            @if($errors->get('endNumero'))
                                                @foreach($errors->get('endNumero') as $error)
                                                    <span class="text-danger">{{ $error }}</span><br>
                                                @endForeach
                                            @endIf
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label for="endBairro">Bairro <span class="text-danger">*</span></label>
                                    <input type="endBairro" name="endBairro" id="endBairro" value="{{$fornecedor->endereco->endBairro}}"  class="form-control">
                                    @if($errors->get('endBairro'))
                                        @foreach($errors->get('endBairro') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="endCEP">CEP</label>
                                    <input type="text" name="endCEP" id="endCEP" class="form-control" value="{{$fornecedor->endereco->endCEP}}" >
                                    @if($errors->get('endCEP'))
                                        @foreach($errors->get('endCEP') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="estado_estCodigo">Estado <span class="text-danger">*</span></label>
                                    <select name="estado_estCodigo" id="estado_estCodigo" class="form-control">
                                        <option value="0">Selecione</option>
                                        @foreach ($estados as $estado)
                                            <option value="{{$estado->estCodigo}}"
                                                @if($fornecedor->endereco->estado_estCodigo == $estado->estCodigo)
                                                selected
                                                @endif>
                                                {{$estado->estEstado}} - {{$estado->estSigla}}
                                            </option>
                                        @endforeach
                                    </select>    
                                    @if($errors->get('estado_estCodigo'))
                                        @foreach($errors->get('estado_estCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="cidade_cidCodigo">Cidade <span class="text-danger">*</span></label>
                                    <select name="cidade_cidCodigo" id="cidade_cidCodigo" class="form-control">
                                        @foreach ($cidades as $cidade)
                                            <option value="{{$cidade->cidCodigo}}"
                                            @if($fornecedor->endereco->cidade_cidCodigo == $cidade->cidCodigo)
                                                selected
                                            @endif>
                                                {{$cidade->cidCidade}}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->get('cidade_cidCodigo'))
                                        @foreach($errors->get('cidade_cidCodigo') as $error)
                                            <span class="text-danger">{{ $error }}</span><br>
                                        @endForeach
                                    @endIf
                                </div>

                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('fornecedor.index')}}" class="btn btn-sm btn-danger">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 1-->  
    </div>
</div>
<br>
<!-- SELECT2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">  
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    var string = forCPF.value;
    //mascara para os campos
    $('#endCEP').mask("00000-000");
    $('#forTelefone').mask("0000-0000");
    $('#forCelular').mask("(00) 0 0000-0000");
    // Mascara de CPF e CNPJ
    
    /*$(function(){
        var options = {
            onKeyPress: function (cpf, ev, el, op) {
                var masks = ['000.000.000-000', '00.000.000/0000-00'];
                $('#forCPF').mask((cpf.length > 14) ? masks[1] : masks[0], op);
            }
        }
        
        string.length > 11 ? $('#forCPF').mask('00.000.000/0000-00', options) : $('#forCPF').mask('000.000.000-009', options);
    });*/

    forCPF.onfocus = function(){
        var options = {
            onKeyPress: function (cpf, ev, el, op) {
                var masks = ['000.000.000-000', '00.000.000/0000-00'];
                $('#forCPF').mask((cpf.length > 14) ? masks[1] : masks[0], op);
            }
        }
        string.length > 11 ? $('#forCPF').mask('00.000.000/0000-00', options) : $('#forCPF').mask('000.000.000-009', options);
    }
    
    // Select 2
    $('#estado_estCodigo').select2();
    $('#cidade_cidCodigo').select2();

    //preenchendo o select de cidade de acordo com o estado escolhido
    $('select[name=estado_estCodigo]').change(function () {
            var estCodigo = $(this).val();
            $.ajax({
                type: 'GET',
                url:baseurl+ "unidade/cidade/"+estCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        console.log(resposta.cidades);
                        $('#cidade_cidCodigo').empty();
                        for(var i=0; i <resposta.tamanho;i++){
                            $('#cidade_cidCodigo').append('<option value=' + resposta.cidades[i].cidCodigo + '>' + resposta.cidades[i].cidCidade + '</option>');
                        }
                        
                    }		
                });

    });
    </script>

    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
