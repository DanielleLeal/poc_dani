@extends('layouts.master') @section('title', '- Nota Fiscal')@section('patrimonioAtivo','bg-danger') @section('principal') @parent
<!--Ajax -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<h3 style="margin-left:10px;"><i class="fa fa-edit"></i> Editar Nota Fiscal </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" id="tabMenu">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Edição</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="box-body box-success">
                <form action="{{route('notaFiscal.update',$nota->nofCodigo)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nofArquivo">Novo Arquivo</label>
                                    <input type="file" name="nofArquivo" id="nofArquivo" class="form-control">
                                    @if($errors->get('nofArquivo'))
                                        @foreach($errors->get('nofArquivo') as $error)
                                            <span class="text-danger">{{ $error }}</span>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="nofDescricao">Descrição <span class="text-danger">*</span></label>
                                    <textarea name="nofDescricao" id="nofDescricao" rows="3" class="form-control" 
                                    placeholder="Um breve descrição sobre a nota fiscal">{{$nota->nofDescricao}}</textarea>
                                    @if($errors->get('nofDescricao'))
                                        @foreach($errors->get('nofDescricao') as $error)
                                            <span class="text-danger">{{ $error }}</span>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('notaFiscal.index')}}" class="btn btn-sm btn-danger mr-3">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Atualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 1-->
    </div>
</div>
@endsection
