@extends('layouts.master') @section('title', '- Nota Fiscal')@section('patrimonioAtivo','bg-danger') @section('principal') @parent
<!--Ajax -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<h3 style="margin-left:10px;"><i class="fa fa-sticky-note"></i> Notas Fiscais </h3>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" id="tabMenu">
        <li id="li_1" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Notas Fiscais</a></li>
        <li id="li_2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Cadastro</a></li>
    </ul>
    <div class="tab-content">
        <!--Aba 1-->
        <div class="tab-pane active" id="tab_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12" style="margin:15px;padding-bottom:20px;">
                        <div class="table-responsive">
                            <table class="table table-hover " id="notafiscal-table">
                                <thead>
                                    <tr>
                                        <th>Descrição </th>
                                        <th>Visualizar Nota</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!--Aba 1--> 
        <!--Aba 2-->
        <div class="tab-pane" id="tab_2">
            <div class="box-body box-success">
                <form action="{{route('notaFiscal.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nofArquivo">Arquivo <span class="text-danger">*</span></label>
                                    <input type="file" name="nofArquivo" id="nofArquivo" class="form-control">
                                    @if($errors->get('nofArquivo'))
                                        @foreach($errors->get('nofArquivo') as $error)
                                            <span class="text-danger">{{ $error }}</span>
                                        @endForeach
                                    @endIf
                                </div>
                                <div class="form-group">
                                    <label for="nofDescricao">Descrição <span class="text-danger">*</span></label>
                                    <textarea name="nofDescricao" id="nofDescricao" rows="3" class="form-control" placeholder="Um breve descrição sobre a nota fiscal"></textarea>
                                    @if($errors->get('nofDescricao'))
                                        @foreach($errors->get('nofDescricao') as $error)
                                            <span class="text-danger">{{ $error }}</span>
                                        @endForeach
                                    @endIf
                                    <div class="pull-right">
                                        <span class="text-danger">*</span><span>Campos Obrigatórios</span>
                                    </div>
                                    <br>
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">
                                        <a href="{{route('notaFiscal.index')}}" class="btn btn-sm btn-danger mr-3">Cancelar</a>
                                        <button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>                         
            </div>  
        </div>
        <!--Aba 2-->
    </div>
</div>
<br>
<!--Modal de exclusão-->
@alerta
@endalerta
<!--Modal de exclusão --> 

<!-- Modal Visualizar nota fiscal -->
<div class="modal" tabindex="-1" role="dialog" id="modalNF">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa-sticky-note"></i> Nota Fiscal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> &times;</button>
        </div>
            <div class="modal-body">
                <div class="text-center">
                    <img  id="imgNF" height="550" width="550">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
      </div>
    </div>
  </div>
<!--Modal Visualizar nota fiscal-->

<!-- Controle das tabs -->
@if(count($errors)>0)
    <script type="text/javascript">
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
    </script>
@else
<script type="text/javascript">
    $('#tab_1').addClass('active');
    $('#li_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#li_2').removeClass('active');
</script>
@endif
<!-- Controle das tabs -->

<script type="text/javascript">
    var baseurl = window.location.protocol + '//' + window.location.host + '/';

    //preenchendo a tabela
    jQuery(function() {
            jQuery('#notafiscal-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("notaFiscal.datatable") }}',
                    "type": "GET"
                },
                "columns": [
                    {
                        data: 'nofDescricao',
                        name: 'Descrição'
                    },
                    {
                        data: 'nofCodigo',
                        render: function(data, type, row) {
                            var html = '<div class="col-md-8">';
                            html += '<p><button id="verNF" type="button" class="btn btn-success btn-block" onclick="visualizarNf('+data+')">';
                            html += '<i class="fa fa-eye"></i> Visualizar </button></p></div>';
                            return html;
                        },
                        name: 'Nota Fiscal'
                    },
                    {
                        data: "nofCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="notaFiscal/' + data + '/edit" class="btn btn-primary btn-block">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-6">';
                            html += '<p><button type="button" class="btn btn-danger btn-block" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    'pdfHtml5',
                    'excelHtml5'
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var nofCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'notaFiscal/'+nofCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.nota.nofCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir a nota fiscal com descrição <strong> "+ resposta.nota.nofDescricao+"?</strong>";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
        }  
            
        //mandando pro metodo destroy
        function funcaoExcluir(){
            var nofCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "notaFiscal/"+nofCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload();
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });   
        };

        function visualizarNf(id)
        {
            $.ajax({
				type: 'GET',
				url: baseurl + 'visualizar/nota_fiscal/'+id,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        console.log(resposta.notaFiscal.nofArquivo);
                        if(resposta.notaFiscal != null){
                            imgNF.src = "/storage/"+resposta.notaFiscal.nofArquivo;
                            $('#modalNF').modal('show');
                        }else{
                            alert('Não é possível visualizar a nota fiscal');
                        }
                        
					}
            });
        }
    </script>
    <script type="text/javascript" src="{{ asset('/data/js/bootstrap-select.js') }}"></script>    
@endsection
