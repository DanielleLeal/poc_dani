<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('useCodigo');
            $table->string('useNome');
            $table->string('email')->unique();
            $table->string('password');
            $table->char('useCelular',16);
            $table->char('useCPF',14)->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('useFoto')->nullable();
            $table->unsignedBigInteger('funcao_funCodigo');
            $table->foreign('funcao_funCodigo')->references('funCodigo')->on('funcao')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
