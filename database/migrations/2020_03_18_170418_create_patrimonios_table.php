<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatrimoniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrimonio', function (Blueprint $table) {
            $table->bigIncrements('patCodigo');
            $table->string('patNome');
            $table->string('patFoto')->nullable();
            $table->Integer('patQuantidade');
            $table->float('patValorCompra');

            $table->unsignedBigInteger('categoria_catCodigo');
            $table->foreign('categoria_catCodigo')->references('catCodigo')->on('categoria')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->unsignedBigInteger('users_useCodigo');
            $table->foreign('users_useCodigo')->references('useCodigo')->on('users')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->unsignedBigInteger('fornecedor_forCodigo');
            $table->foreign('fornecedor_forCodigo')->references('forCodigo')->on('fornecedor')
                                                    ->onDelete('cascade')
                                                        ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patrimonios');
    }
}
