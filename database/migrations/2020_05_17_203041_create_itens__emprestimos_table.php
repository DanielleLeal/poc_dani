<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensEmprestimosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_emprestimo', function (Blueprint $table) {
            $table->bigIncrements('iteCodigo');
            $table->unsignedBigInteger('itensPatrimonio_itpCodigo');
            $table->foreign('itensPatrimonio_itpCodigo')->references('itpCodigo')->on('itens_patrimonios')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->unsignedBigInteger('emprestimo_empCodigo');
            $table->foreign('emprestimo_empCodigo')->references('empCodigo')->on('emprestimo')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('drop trigger updateItensDelete');
    }
}
