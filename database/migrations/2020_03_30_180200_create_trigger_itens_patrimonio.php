<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerItensPatrimonio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Trigger para dar update em quantidade quando for deletado um item de patrimonio
        DB::unprepared('
        CREATE trigger updateQtd after delete
        on itens_patrimonios
        for each row
        Begin 
            update patrimonio
            set patQuantidade= patQuantidade - 1
            where `patCodigo`= old.patrimonio_patCodigo;
        END
        
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `updateQtd`');
    }
}
