<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensPatrimoniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_patrimonios', function (Blueprint $table) {
            $table->bigIncrements('itpCodigo');
            $table->char('itpCodigoBarras')->unique()->nullable();
            $table->float('itpValorAtual');
            $table->integer('itpDepreciacao')->nullable();
            $table->enum('itpStatus',['Ativo','Inativo','Estragado','Manutenção'])->default('Ativo');
            $table->enum('itpSituacao',['Emprestado','Local'])->default('Local');
            $table->enum('itpTipo',['Doado','Comprado']);
            
            $table->unsignedBigInteger('unidade_uniCodigo');
            $table->foreign('unidade_uniCodigo')->references('uniCodigo')->on('unidade')
                                                    ->onDelete('cascade')
                                                        ->onUpdate('cascade');
            $table->unsignedBigInteger('patrimonio_patCodigo');
            $table->foreign('patrimonio_patCodigo')->references('patCodigo')->on('patrimonio')
                                                    ->onDelete('cascade')
                                                        ->onUpdate('cascade');  
            $table->unsignedBigInteger('nota_fiscal_nofCodigo');
            $table->foreign('nota_fiscal_nofCodigo')->references('nofCodigo')->on('nota_fiscal')
                                                    ->onDelete('cascade')
                                                        ->onUpdate('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itens_patrimonios');
    }
}
