<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprestimosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprestimo', function (Blueprint $table) {
            $table->bigIncrements('empCodigo');
            $table->string('empNomeSolicitante',100);
            $table->date('empDataEmprestimo');
            $table->date('empDataDevolucao');
            $table->Integer('empQuantidade');
            $table->float('empValorTotal');
            $table->enum('empStatus',['Pendente','Devolvido','Atrasado'])->default('Pendente');
            $table->text('empFinalidade')->nullable();

            $table->unsignedBigInteger('users_useCodigo');
            $table->foreign('users_useCodigo')->references('useCodigo')->on('users')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->unsignedBigInteger('fornecedor_forCodigo');
            $table->foreign('fornecedor_forCodigo')->references('forCodigo')->on('fornecedor')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprestimos');
    }
}
