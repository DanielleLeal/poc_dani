<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerEmprestimoItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //Trigger para atualizar os itens de patrimonios que estão sendo emprestados
    // a situacao vai de Local para Emprestado
    public function up()
    {
        DB::unprepared('
        CREATE trigger updateItens after insert
        on itens_emprestimo
        for each row
        Begin 
            update itens_patrimonios
            set itpSituacao = "Emprestado"
            where `itpCodigo`= new.itensPatrimonio_itpCodigo;
        END
        
        ');

        /*
        DELIMITER $
CREATE trigger updateItens_destroy after delete
        on emprestimo
        for each row
        Begin 
			set @pr= (select itensPatrimonio_itpCodigo
			from itens_emprestimo
			where emprestimo_empCodigo=old.empCodigo);
            
            update itens_patrimonios
            set itpSituacao = "Local"
            where `itpCodigo`= @pr ;
        END
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('drop trigger updateItens');
    }
}
