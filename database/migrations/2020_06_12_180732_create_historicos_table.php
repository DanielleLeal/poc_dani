<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico', function (Blueprint $table) {
            $table->bigIncrements('hisCodigo');
            $table->Integer('hisAno');
            $table->float('hisValor');
            $table->unsignedBigInteger('itensPatrimonio_itpCodigo');
            $table->foreign('itensPatrimonio_itpCodigo')->references('itpCodigo')->on('itens_patrimonios')
                                                        ->onDelete('cascade')
                                                        ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historicos');
    }
}
