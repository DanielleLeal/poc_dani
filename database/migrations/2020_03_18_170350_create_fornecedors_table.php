<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedor', function (Blueprint $table) {
            $table->bigIncrements('forCodigo');
            $table->string('forNome');
            $table->char('forCPF',20)->unique();
            $table->text('forTelefone')->nullable();
            $table->char('forCelular',16);
            $table->string('forEmail')->unique();
            $table->enum('forStatus',['Parceiro','Fornecedor']);
            $table->unsignedBigInteger('endereco_endCodigo');
            $table->foreign('endereco_endCodigo')->references('endCodigo')->on('endereco')
                                                ->onDelete('cascade')
                                                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedors');
    }
}
