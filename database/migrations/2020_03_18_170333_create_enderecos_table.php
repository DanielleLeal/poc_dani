<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endereco', function (Blueprint $table) {
            $table->bigIncrements('endCodigo');
            $table->string('endRua');
            $table->Integer('endNumero');
            $table->string('endBairro');
            $table->char('endCEP',9)->nullable();
            $table->unsignedBigInteger('estado_estCodigo');
            $table->foreign('estado_estCodigo')->references('estCodigo')->on('estado')
                                                    ->onDelete('cascade')
                                                        ->onUpdate('cascade');

            $table->unsignedBigInteger('cidade_cidCodigo');
            $table->foreign('cidade_cidCodigo')->references('cidCodigo')->on('cidade')
                                                    ->onDelete('cascade')
                                                        ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
