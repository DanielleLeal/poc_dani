<?php

use Illuminate\Database\Seeder;

class PatrimonioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patrimonio')->insert([ 
            [
                'patCodigo'=>1,
                'patNome'=>'Cadeira',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>1,
                'fornecedor_forCodigo' =>2,
                'users_useCodigo' => 1,
                'patValorCompra' => 50,
            ],
            [
                'patCodigo'=>2,
                'patNome'=>'Mesa de computador',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>1,
                'fornecedor_forCodigo' =>2,
                'users_useCodigo' => 1,
                'patValorCompra' => 200,
            ],
            [
                'patCodigo'=>3,
                'patNome'=>'Ventilador',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>1,
                'fornecedor_forCodigo' =>2,
                'users_useCodigo' => 1,
                'patValorCompra' => 80,
            ],
            [
                'patCodigo'=>4,
                'patNome'=>'Computador DELL',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>3,
                'fornecedor_forCodigo' =>1,
                'users_useCodigo' => 1,
                'patValorCompra' => 2500,
            ],
            [
                'patCodigo'=>5,
                'patNome'=>'Data show',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>3,
                'fornecedor_forCodigo' =>1,
                'users_useCodigo' => 1,
                'patValorCompra' => 3000,
            ],
            [
                'patCodigo'=>6,
                'patNome'=>'Saco de pancada',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>2,
                'fornecedor_forCodigo' =>5,
                'users_useCodigo' => 1,
                'patValorCompra' => 140,
            ],
            [
                'patCodigo'=>7,
                'patNome'=>'Placas de tatame',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>2,
                'fornecedor_forCodigo' =>5,
                'users_useCodigo' => 1,
                'patValorCompra' => 80,
            ],
            [
                'patCodigo'=>8,
                'patNome'=>'Halteres',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>2,
                'fornecedor_forCodigo' =>5,
                'users_useCodigo' => 1,
                'patValorCompra' => 90,
            ],
            [
                'patCodigo'=>9,
                'patNome'=>'Armários',
                'patQuantidade'=>0,
                'categoria_catCodigo'=>1,
                'fornecedor_forCodigo' =>2,
                'users_useCodigo' => 1,
                'patValorCompra' => 600,
            ]
        ]);    
    }
}
