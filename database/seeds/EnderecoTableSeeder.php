<?php

use Illuminate\Database\Seeder;

class EnderecoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('endereco')->insert([ 
            [
                'endCodigo' => 1, 
                'endRua' => 'Rua Cristóvão Colombo',
                'endNumero'=>300,
                'endBairro'=>'Água Vermelha',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 2, 
                'endRua' => 'Rua Ten. Oscár Teixeira de Lima',
                'endNumero'=>361,
                'endBairro'=>'Novo Horizonte',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 3, 
                'endRua' => 'Rua Maria Rodrigues Gondin',
                'endNumero'=>77,
                'endBairro'=>'Vila Padre Remaclo',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 4, 
                'endRua' => 'Rua D',
                'endNumero'=>45,
                'endBairro'=>'Centro',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 5, 
                'endRua' => 'Rua E',
                'endNumero'=>50,
                'endBairro'=>'Centro',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 6, 
                'endRua' => 'Rua José Pedro da Silva',
                'endNumero'=>50,
                'endBairro'=>'Bela Vista',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 7, 
                'endRua' => 'Avenida das Laranjeiras',
                'endNumero'=>50,
                'endBairro'=>'Geraldo Veloso',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 8, 
                'endRua' => 'Rua José Cecílio',
                'endNumero'=>76,
                'endBairro'=>'São Luiz',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 9, 
                'endRua' => 'Rua José de Paula',
                'endNumero'=>30,
                'endBairro'=>'Maringá',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 10, 
                'endRua' => 'Rua Lassance Cunha',
                'endNumero'=>400,
                'endBairro'=>'Quinzinho',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 11, 
                'endRua' => 'Rua B',
                'endNumero'=>160,
                'endBairro'=>'Bairro B',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            [
                'endCodigo' => 12, 
                'endRua' => 'Silviano Brandão',
                'endNumero'=>54,
                'endBairro'=>'Centro',
                'endCEP'=>'35570-000',
                'estado_estCodigo'=>13,
                'cidade_cidCodigo'=>1855,
            ],
            
        ]);
    }
}
