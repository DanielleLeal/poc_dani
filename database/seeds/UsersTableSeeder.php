<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'useCodigo'	=>	1,
        	'useNome'=>	'Danielle Leal Silva',
            'email'=>'danielleleal070@gmail.com',
            'password'=>bcrypt('12345678'),
            'useCelular'=>'(37)99846-7616',
            'useCPF'=>'147.789.246-06',
            'useFoto'=>'imagens/default-user.png',
            'funcao_funCodigo'=>1,

        ]);
        User::create([
        	'useCodigo'	=>	2,
        	'useNome'=>	'Dênis Antônio da Silva',
            'email'=>'denissilva59@gmail.com',
            'password'=>bcrypt('123456789'),
            'useCelular'=>'(37)99922-9328',
            'useCPF'=>'528.662.840-03',
            'useFoto'=>'imagens/default-user.png',
            'funcao_funCodigo'=>2,

        ]);
    }
}
