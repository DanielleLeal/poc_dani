<?php

use Illuminate\Database\Seeder;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria')->insert([ 
            [
                'catCodigo' => 1, 
                'catNome' => 'Móveis',
                'catVidaUtil'=>10,
            ],
            [
                'catCodigo' => 2, 
                'catNome' => 'Academia',
                'catVidaUtil'=>5,
            ],
            [
                'catCodigo' => 3, 
                'catNome' => 'Computadores e Periféricos',
                'catVidaUtil'=>5,
            ],
            [
                'catCodigo' => 4, 
                'catNome' => 'Ferramentas',
                'catVidaUtil'=>5,
            ],
            [
                'catCodigo' => 5, 
                'catNome' => 'Veículos',
                'catVidaUtil'=>5,
            ],
            [
                'catCodigo' => 6, 
                'catNome' => 'Imóveis',
                'catVidaUtil'=>25,
            ],
        ]);
    }
}
