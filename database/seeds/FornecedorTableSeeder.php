<?php

use Illuminate\Database\Seeder;

class FornecedorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fornecedor')->insert([ 
            [
                'forCodigo' => 1, 
                'forNome' => 'Tec 2000',
                'forCPF'=>'71.487.125/0001-36',
                'forTelefone'=>'3321-5589',
                'forCelular'=>'(37) 9 9985-5555',
                'forEmail'=>'tec2000@gmail.com',
                'forStatus'=>'Parceiro',
                'endereco_endCodigo'=>4
            ],
            [
                'forCodigo' => 2, 
                'forNome' => 'Casa 505',
                'forCPF'=>'18.068.972/0001-83',
                'forTelefone'=>'3322-5489',
                'forCelular'=>'(37) 9 9975-6666',
                'forEmail'=>'casa505@gmail.com',
                'forStatus'=>'Fornecedor',
                'endereco_endCodigo'=>5
            ],
            [
                'forCodigo' => 3, 
                'forNome' => 'Mão amiga',
                'forCPF'=>'258.568.125-06',
                'forTelefone'=>'3322-6383',
                'forCelular'=>'(37) 9 9937-0192',
                'forEmail'=>'lojaformiga@oficinadastintas.com.br',
                'forStatus'=>'Parceiro',
                'endereco_endCodigo'=>10

            ],
            [
                'forCodigo' => 4, 
                'forNome' => 'Irmandade das Flores',
                'forCPF'=>'554.864.020-82',
                'forTelefone'=>'3322-5874',
                'forCelular'=>'(37) 9 9944-5566',
                'forEmail'=>'irmandadedasflores@gmail.com',
                'forStatus'=>'Parceiro',
                'endereco_endCodigo'=>11

            ],
            [
                'forCodigo' => 5, 
                'forNome' => 'Authentic Sports',
                'forCPF'=>'45.614.264/0001-73',
                'forTelefone'=>'3322-2922',
                'forCelular'=>'(37) 9 9844-9566',
                'forEmail'=>'authentic.sports@gmail.com',
                'forStatus'=>'Fornecedor',
                'endereco_endCodigo'=>12

            ],
            
        ]);
    }
}
