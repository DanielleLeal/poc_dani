<?php

use Illuminate\Database\Seeder;

class NotaFiscalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nota_fiscal')->insert([ 
            [
                'nofCodigo' => 1, 
                'nofDescricao' => 'Esse patrimônio não possui nota fiscal',
                'nofArquivo'=>'notaFiscal/sem_nf.jpg',
            ]
        ]);
    }
}
