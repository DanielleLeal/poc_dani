<?php

use Illuminate\Database\Seeder;
use App\Funcao;

class FuncaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('funcao')->insert([ 
            [
                'funCodigo'	=>	1,
                'funNome'=>	'Administrador',
                'funDescricao'=>'Possui acesso à todas funcionalidades de administração do projeto',
            ],
            [
                'funCodigo'	=>	2,
                'funNome'=>	'Funcionário',
                'funDescricao'=>'Possui acesso à todas funcionalidades de administração do projeto,
                 menos sobre baixa de emprestimos',
            ],
            [
                'funCodigo'	=>	3,
                'funNome'=>	'Professor',
                'funDescricao'=>'Possui acesso a suas turmas, lançamento de frequência e graduação',
            ],
            [
                'funCodigo'	=>	4,
                'funNome'=>	'Assistente social',
                'funDescricao'=>'Possui acesso à todas as turmas e lançamento de notas e frenquênica escolar',
            ]
        ]);
    }
}
