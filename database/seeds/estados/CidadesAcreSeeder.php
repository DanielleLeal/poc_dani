<?php

use Illuminate\Database\Seeder;

class CidadesAcreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
            ['cidCidade' => 'Acrelândia', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Assis Brasil', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Brasiléia', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Bujari', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Capixaba', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Cruzeiro do Sul', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Epitaciolândia', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Feijó', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Jordão', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Mâncio Lima', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Manoel Urbano', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Marechal Thaumaturgo', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Plácido de Castro', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Porto Acre', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Porto Walter', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Rio Branco', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Rodrigues Alves', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Santa Rosa do Purus', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Sena Madureira', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Senador Guiomard', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Tarauacá', 'estado_estCodigo' => 1],
            ['cidCidade' => 'Xapuri', 'estado_estCodigo' => 1]
        ]);

        $this->command->info('Cidades do Acre importadas com sucesso!');
    }
}
