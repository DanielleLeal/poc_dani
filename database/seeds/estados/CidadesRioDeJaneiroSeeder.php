<?php

use Illuminate\Database\Seeder;

class CidadesRioDeJaneiroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
            ['cidCidade' => 'Angra dos Reis', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Aperibé', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Araruama', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Areal', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Armação dos Búzios', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Arraial do Cabo', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Barra do Piraí', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Barra Mansa', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Belford Roxo', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Bom Jardim', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Bom Jesus do Itabapoana', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Cabo Frio', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Cachoeiras de Macacu', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Cambuci', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Campos dos Goytacazes', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Cantagalo', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Carapebus', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Cardoso Moreira', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Carmo', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Casimiro de Abreu', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Comendador Levy Gasparian', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Conceição de Macabu', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Cordeiro', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Duas Barras', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Duque de Caxias', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Engenheiro Paulo de Frontin', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Guapimirim', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Iguaba Grande', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Itaboraí', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Itaguaí', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Italva', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Itaocara', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Itaperuna', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Itatiaia', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Japeri', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Laje do Muriaé', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Macaé', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Macuco', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Magé', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Mangaratiba', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Maricá', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Mendes', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Mesquita', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Miguel Pereira', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Miracema', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Natividade', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Nilópolis', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Niterói', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Nova Friburgo', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Nova Iguaçu', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Paracambi', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Paraíba do Sul', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Parati', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Paty do Alferes', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Petrópolis', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Pinheiral', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Piraí', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Porciúncula', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Porto Real', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Quatis', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Queimados', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Quissamã', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Resende', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Rio Bonito', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Rio Claro', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Rio das Flores', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Rio das Ostras', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Rio de Janeiro', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Santa Maria Madalena', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Santo Antônio de Pádua', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São Fidélis', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São Francisco de Itabapoana', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São Gonçalo', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São João da Barra', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São João de Meriti', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São José de Ubá', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São José do Vale do Rio Pret', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São Pedro da Aldeia', 'estado_estCodigo' => 19],
            ['cidCidade' => 'São Sebastião do Alto', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Sapucaia', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Saquarema', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Seropédica', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Silva Jardim', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Sumidouro', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Tanguá', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Teresópolis', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Trajano de Morais', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Três Rios', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Valença', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Varre-Sai', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Vassouras', 'estado_estCodigo' => 19],
            ['cidCidade' => 'Volta Redonda', 'estado_estCodigo' => 19]
        ]);

        $this->command->info('cidCidades do Rio de Janeiro importadas com sucesso!');
    }
}
