<?php

use Illuminate\Database\Seeder;

class CidadesRoraimaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
            ['cidCidade' => 'Alto Alegre', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Amajari', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Boa Vista', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Bonfim', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Cantá', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Caracaraí', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Caroebe', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Iracema', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Mucajaí', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Normandia', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Pacaraima', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Rorainópolis', 'estado_estCodigo' => 23],
            ['cidCidade' => 'São João da Baliza', 'estado_estCodigo' => 23],
            ['cidCidade' => 'São Luiz', 'estado_estCodigo' => 23],
            ['cidCidade' => 'Uiramutã', 'estado_estCodigo' => 23]
        ]);

        $this->command->info('cidCidades de Roraima importadas com sucesso!');
    }
}
