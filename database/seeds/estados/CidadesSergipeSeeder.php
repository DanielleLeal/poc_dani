<?php

use Illuminate\Database\Seeder;

class CidadesSergipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
            ['cidCidade' => 'Amparo de São Francisco', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Aquidabã', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Aracaju', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Arauá', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Areia Branca', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Barra dos Coqueiros', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Boquim', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Brejo Grande', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Campo do Brito', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Canhoba', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Canindé de São Francisco', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Capela', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Carira', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Carmópolis', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Cedro de São João', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Cristinápolis', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Cumbe', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Divina Pastora', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Estância', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Feira Nova', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Frei Paulo', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Gararu', 'estado_estCodigo' => 26],
            ['cidCidade' => 'General Maynard', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Gracho Cardoso', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Ilha das Flores', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Indiaroba', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Itabaiana', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Itabaianinha', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Itabi', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Itaporanga d`Ajuda', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Japaratuba', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Japoatã', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Lagarto', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Laranjeiras', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Macambira', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Malhada dos Bois', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Malhador', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Maruim', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Moita Bonita', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Monte Alegre de Sergipe', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Muribeca', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Neópolis', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Nossa Senhora Aparecida', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Nossa Senhora da Glória', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Nossa Senhora das Dores', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Nossa Senhora de Lourdes', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Nossa Senhora do Socorro', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Pacatuba', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Pedra Mole', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Pedrinhas', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Pinhão', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Pirambu', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Poço Redondo', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Poço Verde', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Porto da Folha', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Propriá', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Riachão do Dantas', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Riachuelo', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Ribeirópolis', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Rosário do Catete', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Salgado', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Santa Luzia do Itanhy', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Santa Rosa de Lima', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Santana do São Francisco', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Santo Amaro das Brotas', 'estado_estCodigo' => 26],
            ['cidCidade' => 'São Cristóvão', 'estado_estCodigo' => 26],
            ['cidCidade' => 'São Domingos', 'estado_estCodigo' => 26],
            ['cidCidade' => 'São Francisco', 'estado_estCodigo' => 26],
            ['cidCidade' => 'São Miguel do Aleixo', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Simão Dias', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Siriri', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Telha', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Tobias Barreto', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Tomar do Geru', 'estado_estCodigo' => 26],
            ['cidCidade' => 'Umbaúba', 'estado_estCodigo' => 26]
        ]);

        $this->command->info('cidCidades de Sergipe importadas com sucesso!');
    }
}
