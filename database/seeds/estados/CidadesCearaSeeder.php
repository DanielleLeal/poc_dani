<?php

use Illuminate\Database\Seeder;

class CidadesCearaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
        	['cidCidade' => 'Abaiara', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Acarape', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Acaraú', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Acopiara', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Aiuaba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Alcântaras', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Altaneira', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Alto Santo', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Amontada', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Antonina do Norte', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Apuiarés', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Aquiraz', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Aracati', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Aracoiaba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ararendá', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Araripe', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Aratuba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Arneiroz', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Assaré', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Aurora', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Baixio', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Banabuiú', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Barbalha', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Barreira', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Barro', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Barroquinha', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Baturité', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Beberibe', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Bela Cruz', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Boa Viagem', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Brejo Santo', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Camocim', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Campos Sales', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Canindé', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Capistrano', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Caridade', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Cariré', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Caririaçu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Cariús', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Carnaubal', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Cascavel', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Catarina', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Catunda', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Caucaia', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Cedro', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Chaval', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Choró', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Chorozinho', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Coreaú', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Crateús', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Crato', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Croatá', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Cruz', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Deputado Irapuan Pinheiro', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ererê', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Eusébio', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Farias Brito', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Forquilha', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Fortaleza', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Fortim', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Frecheirinha', 'estado_estCodigo' => 6],
            ['cidCidade' => 'General Sampaio', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Graça', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Granja', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Granjeiro', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Groaíras', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Guaiúba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Guaraciaba do Norte', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Guaramiranga', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Hidrolândia', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Horizonte', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ibaretama', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ibiapina', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ibicuitinga', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Icapuí', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Icó', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Iguatu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'IncidCidadeependência', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ipaporanga', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ipaumirim', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ipu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ipueiras', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Iracema', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Irauçuba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Itaiçaba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Itaitinga', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Itapagé', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Itapipoca', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Itapiúna', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Itarema', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Itatira', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jaguaretama', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jaguaribara', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jaguaribe', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jaguaruana', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jardim', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jati', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jijoca de Jericoacoara', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Juazeiro do Norte', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Jucás', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Lavras da Mangabeira', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Limoeiro do Norte', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Madalena', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Maracanaú', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Maranguape', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Marco', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Martinópole', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Massapê', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Mauriti', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Meruoca', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Milagres', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Milhã', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Miraíma', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Missão Velha', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Mombaça', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Monsenhor Tabosa', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Morada Nova', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Moraújo', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Morrinhos', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Mucambo', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Mulungu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Nova Olinda', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Nova Russas', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Novo Oriente', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ocara', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Orós', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pacajus', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pacatuba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pacoti', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pacujá', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Palhano', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Palmácia', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Paracuru', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Paraipaba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Parambu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Paramoti', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pedra Branca', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Penaforte', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pentecoste', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pereiro', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pindoretama', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Piquet Carneiro', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Pires Ferreira', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Poranga', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Porteiras', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Potengi', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Potiretama', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Quiterianópolis', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Quixadá', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Quixelô', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Quixeramobim', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Quixeré', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Redenção', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Reriutaba', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Russas', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Saboeiro', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Salitre', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Santa Quitéria', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Santana do Acaraú', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Santana do Cariri', 'estado_estCodigo' => 6],
            ['cidCidade' => 'São Benedito', 'estado_estCodigo' => 6],
            ['cidCidade' => 'São Gonçalo do Amarante', 'estado_estCodigo' => 6],
            ['cidCidade' => 'São João do Jaguaribe', 'estado_estCodigo' => 6],
            ['cidCidade' => 'São Luís do Curu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Senador Pompeu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Senador Sá', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Sobral', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Solonópole', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Tabuleiro do Norte', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Tamboril', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Tarrafas', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Tauá', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Tejuçuoca', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Tianguá', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Trairi', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Tururu', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Ubajara', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Umari', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Umirim', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Uruburetama', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Uruoca', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Varjota', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Várzea Alegre', 'estado_estCodigo' => 6],
            ['cidCidade' => 'Viçosa do Ceará', 'estado_estCodigo' => 6]
        ]);

        $this->command->info('Cidades do Ceará importadas com sucesso!');
    }
}
