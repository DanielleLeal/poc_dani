<?php

use Illuminate\Database\Seeder;

class CidadesMatoGrossoDoSulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
        	['cidCidade' => 'Água Clara', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Alcinópolis', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Amambaí', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Anastácio', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Anaurilândia', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Angélica', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Antônio João', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Aparecida do Taboado', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Aquidauana', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Aral Moreira', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Bandeirantes', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Bataguassu', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Bataiporã', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Bela Vista', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Bodoquena', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Bonito', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Brasilândia', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Caarapó', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Camapuã', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Campo Grande', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Caracol', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Cassilândia', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Chapadão do Sul', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Corguinho', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Coronel Sapucaia', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Corumbá', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Costa Rica', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Coxim', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Deodápolis', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Dois Irmãos do Buriti', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Douradina', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Dourados', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Eldorado', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Fátima do Sul', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Figueirão', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Glória de Dourados', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Guia Lopes da Laguna', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Iguatemi', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Inocência', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Itaporã', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Itaquiraí', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Ivinhema', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Japorã', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Jaraguari', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Jardim', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Jateí', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Juti', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Ladário', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Laguna Carapã', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Maracaju', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Miranda', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Mundo Novo', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Naviraí', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Nioaque', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Nova Alvorada do Sul', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Nova Andradina', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Novo Horizonte do Sul', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Paranaíba', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Paranhos', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Pedro Gomes', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Ponta Porã', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Porto Murtinho', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Ribas do Rio Pardo', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Rio Brilhante', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Rio Negro', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Rio Verde de Mato Grosso', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Rochedo', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Santa Rita do Pardo', 'estado_estCodigo' => 12],
            ['cidCidade' => 'São Gabriel do Oeste', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Selvíria', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Sete Quedas', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Sidrolândia', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Sonora', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Tacuru', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Taquarussu', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Terenos', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Três Lagoas', 'estado_estCodigo' => 12],
            ['cidCidade' => 'Vicentina', 'estado_estCodigo' => 12]
        ]);

        $this->command->info('Cidades do Mato Grosso do Sul importadas com sucesso!');
    }
}
