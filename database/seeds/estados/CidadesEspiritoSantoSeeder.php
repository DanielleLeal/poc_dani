<?php

use Illuminate\Database\Seeder;

class CidadesEspiritoSantoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
        	['cidCidade' => 'Afonso Cláudio', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Água Doce do Norte', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Águia Branca', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Alegre', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Alfredo Chaves', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Alto Rio Novo', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Anchieta', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Apiacá', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Aracruz', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Atilio Vivacqua', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Baixo Guandu', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Barra de São Francisco', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Boa Esperança', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Bom Jesus do Norte', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Brejetuba', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Cachoeiro de Itapemirim', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Cariacica', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Castelo', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Colatina', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Conceição da Barra', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Conceição do Castelo', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Divino de São Lourenço', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Domingos Martins', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Dores do Rio Preto', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Ecoporanga', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Fundão', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Governador Lindenberg', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Guaçuí', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Guarapari', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Ibatiba', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Ibiraçu', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Ibitirama', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Iconha', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Irupi', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Itaguaçu', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Itapemirim', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Itarana', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Iúna', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Jaguaré', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Jerônimo Monteiro', 'estado_estCodigo' => 8],
            ['cidCidade' => 'João Neiva', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Laranja da Terra', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Linhares', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Mantenópolis', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Marataízes', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Marechal Floriano', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Marilândia', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Mimoso do Sul', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Montanha', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Mucurici', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Muniz Freire', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Muqui', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Nova Venécia', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Pancas', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Pedro Canário', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Pinheiros', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Piúma', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Ponto Belo', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Presidente Kennedy', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Rio Bananal', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Rio Novo do Sul', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Santa Leopoldina', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Santa Maria de Jetibá', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Santa Teresa', 'estado_estCodigo' => 8],
            ['cidCidade' => 'São Domingos do Norte', 'estado_estCodigo' => 8],
            ['cidCidade' => 'São Gabriel da Palha', 'estado_estCodigo' => 8],
            ['cidCidade' => 'São José do Calçado', 'estado_estCodigo' => 8],
            ['cidCidade' => 'São Mateus', 'estado_estCodigo' => 8],
            ['cidCidade' => 'São Roque do Canaã', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Serra', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Sooretama', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Vargem Alta', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Venda Nova do Imigrante', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Viana', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Vila Pavão', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Vila Valério', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Vila Velha', 'estado_estCodigo' => 8],
            ['cidCidade' => 'Vitória', 'estado_estCodigo' => 8]
        ]);

        $this->command->info('Cidades do Espírito Santo importadas com sucesso!');
    }
}
