<?php

use Illuminate\Database\Seeder;

class CidadesAmazonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
        	['cidCidade' => 'Alvarães', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Amaturá', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Anamã', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Anori', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Apuí', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Atalaia do Norte', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Autazes', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Barcelos', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Barreirinha', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Benjamin Constant', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Beruri', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Boa Vista do Ramos', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Boca do Acre', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Borba', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Caapiranga', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Canutama', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Carauari', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Careiro', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Careiro da Várzea', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Coari', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Codajás', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Eirunepé', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Envira', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Fonte Boa', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Guajará', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Humaitá', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Ipixuna', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Iranduba', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Itacoatiara', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Itamarati', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Itapiranga', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Japurá', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Juruá', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Jutaí', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Lábrea', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Manacapuru', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Manaquiri', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Manaus', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Manicoré', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Maraã', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Maués', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Nhamundá', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Nova Olinda do Norte', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Novo Airão', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Novo Aripuanã', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Parintins', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Pauini', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Presidente Figueiredo', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Rio Preto da Eva', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Santa Isabel do Rio Negro', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Santo Antônio do Içá', 'estado_estCodigo' => 4],
            ['cidCidade' => 'São Gabriel da Cachoeira', 'estado_estCodigo' => 4],
            ['cidCidade' => 'São Paulo de Olivença', 'estado_estCodigo' => 4],
            ['cidCidade' => 'São Sebastião do Uatumã', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Silves', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Tabatinga', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Tapauá', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Tefé', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Tonantins', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Uarini', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Urucará', 'estado_estCodigo' => 4],
            ['cidCidade' => 'Urucurituba', 'estado_estCodigo' => 4]
        ]);

        $this->command->info('Cidades do Amazonas importadas com sucesso!');
    }
}
