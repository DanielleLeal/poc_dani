<?php

use Illuminate\Database\Seeder;

class CidadesDistritoFederalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
        	['cidCidade' => 'Brasília', 'estado_estCodigo' => 7]
        ]);

        $this->command->info('Cidades do Distrito Federal importadas com sucesso!');
    }
}
