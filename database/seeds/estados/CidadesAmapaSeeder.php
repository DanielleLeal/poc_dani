<?php

use Illuminate\Database\Seeder;

class CidadesAmapaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
        	['cidCidade' => 'Amapá', 'estado_estCodigo' => 3],
			['cidCidade' => 'Calçoene', 'estado_estCodigo' => 3],
			['cidCidade' => 'Cutias', 'estado_estCodigo' => 3],
			['cidCidade' => 'Ferreira Gomes', 'estado_estCodigo' => 3],
			['cidCidade' => 'Itaubal', 'estado_estCodigo' => 3],
			['cidCidade' => 'Laranjal do Jari', 'estado_estCodigo' => 3],
			['cidCidade' => 'Macapá', 'estado_estCodigo' => 3],
			['cidCidade' => 'Mazagão', 'estado_estCodigo' => 3],
			['cidCidade' => 'Oiapoque', 'estado_estCodigo' => 3],
			['cidCidade' => 'Pedra Branca do Amaparí', 'estado_estCodigo' => 3],
			['cidCidade' => 'Porto Grande', 'estado_estCodigo' => 3],
			['cidCidade' => 'Pracuúba', 'estado_estCodigo' => 3],
			['cidCidade' => 'Santana', 'estado_estCodigo' => 3],
			['cidCidade' => 'Serra do Navio', 'estado_estCodigo' => 3],
			['cidCidade' => 'Tartarugalzinho', 'estado_estCodigo' => 3],
			['cidCidade' => 'Vitória do Jari', 'estado_estCodigo' => 3]
        ]);

        $this->command->info('Cidades do Amapá importadas com sucesso!');
    }
}
