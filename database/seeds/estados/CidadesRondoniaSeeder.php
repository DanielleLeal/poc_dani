<?php

use Illuminate\Database\Seeder;

class CidadesRondoniaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert([
            ['cidCidade' => 'Alta Floresta d`Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Alto Alegre dos Parecis', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Alto Paraíso', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Alvorada d`Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Ariquemes', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Buritis', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Cabixi', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Cacaulândia', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Cacoal', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Campo Novo de Rondônia', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Candeias do Jamari', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Castanheiras', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Cerejeiras', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Chupinguaia', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Colorado do Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Corumbiara', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Costa Marques', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Cujubim', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Espigão d`Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Governador Jorge Teixeira', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Guajará-Mirim', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Itapuã do Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Jaru', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Ji-Paraná', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Machadinho d`Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Ministro Andreazza', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Mirante da Serra', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Monte Negro', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Nova Brasilândia d`Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Nova Mamoré', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Nova União', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Novo Horizonte do Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Ouro Preto do Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Parecis', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Pimenta Bueno', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Pimenteiras do Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Porto Velho', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Presidente Médici', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Primavera de Rondônia', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Rio Crespo', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Rolim de Moura', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Santa Luzia d`Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'São Felipe d`Oeste', 'estado_estCodigo' => 22],
            ['cidCidade' => 'São Francisco do Guaporé', 'estado_estCodigo' => 22],
            ['cidCidade' => 'São Miguel do Guaporé', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Seringueiras', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Teixeirópolis', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Theobroma', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Urupá', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Vale do Anari', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Vale do Paraíso', 'estado_estCodigo' => 22],
            ['cidCidade' => 'Vilhena', 'estado_estCodigo' => 22]
        ]);

        $this->command->info('cidCidades de Rondônia importadas com sucesso!');
    }
}
