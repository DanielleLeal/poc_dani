<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado')->insert([
            ['estEstado' => 'Acre', 'estSigla' => 'AC'],
            ['estEstado' => 'Alagoas', 'estSigla' => 'AL'],
            ['estEstado' => 'Amapá', 'estSigla' => 'AP'],
            ['estEstado' => 'Amazonas', 'estSigla' => 'AM'],
            ['estEstado' => 'Bahia', 'estSigla' => 'BA'],
            ['estEstado' => 'Ceará', 'estSigla' => 'CE'],
            ['estEstado' => 'Distrito Federal', 'estSigla' => 'DF'],
            ['estEstado' => 'Espírito Santo', 'estSigla' => 'ES'],
            ['estEstado' => 'Goiás', 'estSigla' => 'GO'],
            ['estEstado' => 'Maranhão', 'estSigla' => 'MA'],
            ['estEstado' => 'Mato Grosso', 'estSigla' => 'MT'],
            ['estEstado' => 'Mato Grosso do Sul', 'estSigla' => 'MS'],
            ['estEstado' => 'Minas Gerais', 'estSigla' => 'MG'],
            ['estEstado' => 'Pará', 'estSigla' => 'PA'],
            ['estEstado' => 'Paraíba', 'estSigla' => 'PB'],
            ['estEstado' => 'Paraná', 'estSigla' => 'PR'],
            ['estEstado' => 'Pernambuco', 'estSigla' => 'PE'],
            ['estEstado' => 'Piauí', 'estSigla' => 'PI'],
            ['estEstado' => 'Rio de Janeiro', 'estSigla' => 'RJ'],
            ['estEstado' => 'Rio Grande do Norte', 'estSigla' => 'RN'],
            ['estEstado' => 'Rio Grande do Sul', 'estSigla' => 'RS'],
            ['estEstado' => 'Rondônia', 'estSigla' => 'RO'],
            ['estEstado' => 'Roraima', 'estSigla' => 'RR'],
            ['estEstado' => 'Santa Catarina', 'estSigla' => 'SC'],
            ['estEstado' => 'São Paulo', 'estSigla' => 'SP'],
            ['estEstado' => 'Sergipe', 'estSigla' => 'SE'],
            ['estEstado' => 'Tocantins', 'estSigla' => 'TO']
        ]);

        $this->command->info('estEstados Brasileiros importados com sucesso!');

    }
}
