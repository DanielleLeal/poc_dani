<?php

use Illuminate\Database\Seeder;

class UnidadeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unidade')->insert([ 
            [
                'uniCodigo' => 1, 
                'uniNome' => 'Unidade 1',
                'endereco_endCodigo'=>1,
            ],
            [
                'uniCodigo' => 2, 
                'uniNome' => 'Unidade 2',
                'endereco_endCodigo'=>2,
            ],
            [
                'uniCodigo' => 3, 
                'uniNome' => 'Unidade 3',
                'endereco_endCodigo'=>3,
            ],
            [
                'uniCodigo' => 4, 
                'uniNome' => 'Sede',
                'endereco_endCodigo'=>6,
            ],
            [
                'uniCodigo' => 5, 
                'uniNome' => 'Unidade 4',
                'endereco_endCodigo'=>7,
            ],
            [
                'uniCodigo' => 6, 
                'uniNome' => 'Unidade 5- Patronato São Luiz',
                'endereco_endCodigo'=>8,
            ],
            [
                'uniCodigo' => 7, 
                'uniNome' => 'Unidade 6',
                'endereco_endCodigo'=>9,
            ],
        ]);
    }
}
