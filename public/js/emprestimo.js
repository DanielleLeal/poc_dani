var baseurl = window.location.protocol + '//' + window.location.host + '/';
var unidade =0;
var status=0;
var categoria=0;
var patrimonio=0;
var valorT=0;
var current_page =1;
var emprestados =[];
var pagina=0;
var tamanhoPagina = 10;
var trs =[];
//preenchendo o modal
function abriModal(data){
    var empCodigo = data;
    $.ajax({
        type: 'GET',
        url: baseurl + 'emprestimo/'+empCodigo,
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
            error: function(response) {
                console.log("Erro:\n"+response);
            },
            success: function(resposta) {
                if(resposta != 'Vazio'){
                    codigo.value = resposta.emprestimo.empCodigo;
                    modalBody.innerHTML = "Tem certeza que deseja excluir o <strong>empréstimo</strong> feito  para a empresa "+ resposta.emprestimo.fornecedor.forNome+"</strong>?";
                    $('#modalExclusao').modal('show');	
                }else{
                    alert('Não é possível realizar a exclusão,tente mais tarde!');
                }
            }
    });
}  
    
//mandando pro metodo destroy
function funcaoExcluir(){
    var empCodigo = codigo.value;
    console.log(empCodigo);
    $.ajax({
        type: 'DELETE',
        url:baseurl+ "emprestimo/"+empCodigo,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json',
            'Authorization': '${this.state.tokenType} ${this.state.token}'
        },
        cache: false,
            error: function(response) {
                console.log("Erro:\n"+response);
            },
            success: function(resposta) {
                console.log(resposta);
                if(resposta == 'true'){
                    $('#modalExclusao').modal('hide');
                    window.location.reload();
                }else{
                    alert('Não foi possível realizar a exclusão,tente mais tarde!');
                }
            }		
        });
}

$(function(){
    carregarPatrimonios(1,this.unidade,this.status,this.categoria,this.patrimonio);
    valorRee.innerHTML = (valorT).toLocaleString('pt-BR', { minimumFractionDigits: 2 });
});

function carregarPatrimonios(pagina,uniCodigo=0,status=0,catCodigo=0,patCodigo=0) { 
    console.log(uniCodigo,status,catCodigo,patCodigo);
    $.get('/emprestimo/patrimonios/json/'+uniCodigo+'/'+status+'/'+catCodigo+'/'+patCodigo,{page: pagina}, function(resp) { 
        getCurrentPage(resp.patrimonios.current_page);
        montarTabela(resp.patrimonios.data);
        montarPaginator(resp.patrimonios);
        $("#paginationNav>ul>li>a").click(function(){
            carregarPatrimonios($(this).attr('pagina'),uniCodigo,status,catCodigo,patCodigo);
        })
        $("#card-title").html( "Mostrando de " + resp.patrimonios.from + " até  "+ resp.patrimonios.to+
        " de " + resp.patrimonios.total+ " registros");
        });

        
};

function getCurrentPage(page){
    this.current_page = page;
}

/***************************TABELA E PAGINAÇÃO************************************** */
function montarTabela(dados) {
    $("#table-emprestimo>tbody>tr").remove();
    if(dados.length < 1){
        $("#table-emprestimo>tbody").append(
            '<tr><td colspan="6" style="padding: 15px; font-size:14px;" class="text-center">Nenhum registro encontrado</td></tr>'
        );
    }
    for(i=0;i<dados.length;i++) {
        $("#table-emprestimo>tbody").append(
            montarLinha(dados[i])
        );
    }
};

function montarLinha(patrimonio) { 
    if(in_array(patrimonio.itpCodigo,emprestados) == -1){
        var linha = '<tr id="'+patrimonio.itpCodigo+'">' +
        '<td class="text-center"><div class="checkbox">'+
            '<label for="'+patrimonio.itpCodigoBarras+'">'+
                '<input type="checkbox" id="'+patrimonio.itpCodigoBarras+'" onclick="selecionar('+patrimonio.itpCodigo+')"value="'+patrimonio.itpCodigo+'">'+
                '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>'+
            '</label>'+
        '</div></td>'+
        '  <td>' + patrimonio.itpCodigoBarras + '</td>' +
        '  <td>' + patrimonio.patNome + '</td>' +
        '  <td>' + (patrimonio.itpValorAtual).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' }) + '</td>' +
        ' </td>'  +  
        '</tr>';
        return linha;
    }
    return '';
};

function getNextItem(data) {
    i = data.current_page+1; 
    if (data.current_page == data.last_page)
        s = '<li class="page-item disabled">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Próximo</a></li>';
    return s;
}

function getPreviousItem(data) {
    i = data.current_page-1; 
    if (data.current_page == 1)
        s = '<li class="page-item disabled">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Anterior</a></li>';
    return s;

}

function getItem(data, i) {
    if (data.current_page == i) 
        s = '<li class="page-item active">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">' + i + '</a></li>';
    return s;
}

function montarPaginator(data) {
    
    $("#paginationNav>ul>li").remove();

    $("#paginationNav>ul").append(
        getPreviousItem(data)
    );
    
    n = 2;
    if (data.current_page - n/2 <= 1) 
        inicio = 1;
    else if (data.last_page - data.current_page < n) 
        inicio = data.last_page - n + 1;
    else
        inicio = data.current_page - n/2;
    
    fim = inicio + n-1;

    for (i=inicio;i<=fim;i++) {
        $("#paginationNav>ul").append(
            getItem(data,i)
        );
    }
    $("#paginationNav>ul").append(
        getNextItem(data)
    );
};
/***************************TABELA E PAGINAÇÃO************************************** */


//fltrando os itens de patrimonio na tabela
function filtrar(tipo,paramentro){
    if(tipo == 'unidade'){
        this.unidade = paramentro;
    }else if( tipo == 'status'){
        this.status = paramentro;
    }else if(tipo == 'categoria'){
        this.categoria = paramentro;
    }else if(tipo == 'patrimonio'){
        this.patrimonio = paramentro;
    }
    carregarPatrimonios(1,this.unidade,this.status,this.categoria,this.patrimonio);
}

//implementando pesquisa instântanea
$("#pesquisar").keyup(function(){
    if($("#pesquisar").val().length >= 2){
        var valor = $("#pesquisar").val();
        
        $.get('/emprestimo/patrimonios/json/'+unidade+'/'+status+'/'+categoria+'/'+patrimonio,
            {pesquisar: valor}, function(resp) {
            montarTabela(resp.patrimonios.data);
            montarPaginator(resp.patrimonios);
            
            $("#paginationNav>ul>li>a").click(function(){
                carregarPatrimonios($(this).attr('pagina'),this.unidade,this.status,this.categoria,this.patrimonio);
            });
            $("#card-title").html( "Mostrando de " + resp.patrimonios.from + " até  "+ resp.patrimonios.to+
            " de " + resp.patrimonios.total+ " registros");   
        });
    }else{
        carregarPatrimonios(1,unidade,status,categoria,patrimonio);
    }
    
});

//fazendo validação dos campos antes de passar pra proxima etapa
function proximaEtapa(){
    $('#empNome_error>span').remove();
    $('#fornecedor_error>span').remove();
    $('#dataEmp_error>span').remove();
    $('#dataDev_error>span').remove();
    if(empNomeSolicitante.value == ''){
        $('#empNome_error>span').remove();
        $('#empNome_error').append(
            '<span class="text-danger">Nome é um requisito obrigatório</span>');
    }
    if(fornecedor_forCodigo.value == 0){
        $('#fornecedor_error>span').remove();
        $('#fornecedor_error').append(
            '<span class="text-danger">Selecione uma empresa válida</span>');
    }
    if(empDataEmprestimo.value == '' && empDataEmprestimo.length == 10){
        $('#dataEmp_error>span').remove();
        $('#dataEmp_error').append(
            '<span class="text-danger">Data empréstimo é um requisito obrigatório</span>');
    }
    if(empDataEmprestimo.value.length != 10){
        $('#dataEmp_error>span').remove();
        $('#dataEmp_error').append(
            '<span class="text-danger">Formato errado!Ex: dd/mm/Yyyy </span>');
    }
    if(empDataDevolucao.value == ''){
        $('#dataDev_error>span').remove();
        $('#dataDev_error').append(
            '<span class="text-danger">Data devolução é um requisito obrigatório</span>');
    }
    if(empDataDevolucao.value.length != 10){
        $('#dataDev_error>span').remove();
        $('#dataDev_error').append(
            '<span class="text-danger">Formato errado!Ex: dd/mm/Yyyy </span>');
    }
    if(empNomeSolicitante.value != '' && fornecedor_forCodigo.value != 0 
    && empDataEmprestimo.value != '' && empDataDevolucao.value != ''
    && empDataDevolucao.value.length == 10 && empDataEmprestimo.value.length == 10  ){
        $('#primeira').removeClass("show");
        $('#primeira').addClass("hide");
        $('#segunda').removeClass("hide");
        $('#segunda').addClass("show");
        
        $('#empNome_error>span').remove();
        $('#fornecedor_error>span').remove();
        $('#dataEmp_error>span').remove();
        $('#dataDev_error>span').remove();
        
    }
}

function voltarEtapa(){
    $('#segunda').removeClass("show");
    $('#segunda').addClass("hide");
    $('#primeira').removeClass("hide");
    $('#primeira').addClass("show");
}

//para verificar se existe um elemento em um array
function in_array(needle, haystack){
    var found = 0;
    for (var i=0, len=haystack.length;i<len;i++) {
    if (haystack[i] == needle) return i;
    found++;
        }
    return -1;
}

//buscar elemento selecionado para ser emprestado
function selecionar(codigo)
{
    var page = this.current_page;
    var unidade = this.unidade;
    var status = this.status;
    var categoria = this.categoria;
    var patrimonio = this.patrimonio;

    $.get('/emprestimo/item/adicionar/'+codigo, function(resp) { // vai no controller e busca os patrimônios de acordo com que foi digitado
        if(resp != false){
            if(in_array(resp.item.itpCodigo,emprestados) == -1){
                emprestados.push(resp.item.itpCodigo);
                valorT += resp.item.itpValorAtual;
                var linha = '<tr id="item'+resp.item.itpCodigo+'">' +
                '  <td>' + resp.item.itpCodigoBarras + '</td>' +
                '  <td>' + resp.item.patNome + '</td>' +
                '  <td> ' + resp.item.itpValorAtual + '</td>' +
                ' <td>' +
                        '<button type="button" onclick="(remover('+resp.item.itpCodigo+','+resp.item.itpValorAtual+'))" class="btn btn-warning">'+
                                'Remover</button>'+
                ' </td>'  +  
                '</tr>';
                paginar(linha);
                valorRee.innerHTML = (valorT).toLocaleString('pt-BR', { minimumFractionDigits: 2 });
                $('#'+codigo).remove();
                if($("#table-emprestimo>tbody>tr").length == 0){
                    page = page+1;
                    carregarPatrimonios(page,unidade,status,categoria,patrimonio);
                }
            }
            
        }
        
    });
    

};

/*********Paginação da Segunda Tabela******** */
function paginar(linha = 0) {
    $('#tbodyEmprestado>tr').remove();
    if(linha != 0){  
        trs.push(linha);
    }
    for (var i = pagina * tamanhoPagina; i < trs.length && i < (pagina + 1) *  tamanhoPagina; i++) {
        $('#tbodyEmprestado').append(trs[i]);
    }
    if(trs.length > tamanhoPagina){
        $("#pagination").css('display','block');
    }
    $('#mostrarPaginas>span').remove();
    $('#mostrarPaginas').append('<span>Mostrando '+(pagina +1)+' de '+ Math.ceil(trs.length / tamanhoPagina)+' páginas </span>');
}

function ajustarBotoes() {
    console.log(trs.length <= tamanhoPagina || pagina >= Math.ceil(trs.length / tamanhoPagina) - 1);
    $('#proximo').prop('disabled', trs.length <= tamanhoPagina || pagina >= Math.ceil(trs.length / tamanhoPagina) - 1);
    $('#anterior').prop('disabled', trs.length <= tamanhoPagina || pagina == 0);
}


$('#proximo').click(function() {
    if (pagina < trs.length / tamanhoPagina - 1) {
        pagina++;
        paginar();
        ajustarBotoes();
    }
});
$('#anterior').click(function() {
    if (pagina > 0) {
        pagina--;
        paginar();    
        ajustarBotoes();
    }
});
/*********Paginação da Segunda Tabela******** */


//remover itens de patrimonio da tabela
function remover(id,valor){
    //removendo a tr do array trs
    var rowRemove = 'item'+id;
    var string = document.getElementById(rowRemove).innerHTML;
    string = '<tr id="item'+id+'">'+string+'</tr>';
    var indexRow = trs.indexOf(string);
    if ( indexRow > -1) {
        trs.splice(indexRow, 1);
    }
    //se retirar todos, eu volto uma página
    if (pagina > 0 && $('#tbodyEmprestado>tr').length <= 1) {
        pagina--;
        paginar();    
        ajustarBotoes();
    }
    var row = "#item"+id;
    $(row).remove();
    valorT = valorT - valor;
    valorRee.innerHTML = (valorT).toLocaleString('pt-BR', { minimumFractionDigits: 2 });
    var index = emprestados.indexOf(id);
    if ( index > -1) {
        emprestados.splice(index, 1);
    }
    carregarPatrimonios(this.current_page,this.unidade,this.status,this.categoria,this.patrimonio);
}

//selecionar todos os inputs checkbox
function selecionarTodos(){
    $('input:checkbox').prop('checked',true);
    $('input:checkbox').click();
    $('#ch').prop('checked',false);
}

function submeterForm()
{
    empPatrimonios.value = emprestados;
    empValorTotal.value = valorT;
    if(emprestados.length == 0 || valorT == 0 ){
        $('#error').modal('show');
    }else{
        formEmp.submit();
    }
}
/**************EDITAR***************** */
//preencher tabela de patrimônio selecionados - EDITAR
function itensEmprestados(){
    var empCodigo = emprestimo_id.value; 
    emprestados = [];
    $.get('/emprestimo/editar/json/'+empCodigo, function(resp) {
        $("#tbodyEmprestado>tr").remove();
        for(var i=0;i<resp.itens.length;i++){
            if(in_array(resp.itens[i].itpCodigo,emprestados) == -1){
                emprestados.push(resp.itens[i].itpCodigo);
                var linha = '<tr id="item'+resp.itens[i].itpCodigo+'">' +
                '  <td>' + resp.itens[i].itpCodigoBarras + '</td>' +
                '  <td>' + resp.itens[i].patNome + '</td>' +
                '  <td> ' + resp.itens[i].itpValorAtual + '</td>' +
                ' <td>' +
                        '<button type="button" onclick="(removerEditar('+resp.itens[i].itpCodigo+','+resp.itens[i].itpValorAtual+'))" class="btn btn-warning">'+
                                'Remover</button>'+
                ' </td>'  +  
                '</tr>';
                paginar(linha);
            }
        }
        getValorTotal(resp.emprestimo.empValorTotal)
        valorRee.innerHTML = (resp.emprestimo.empValorTotal).toLocaleString('pt-BR', { minimumFractionDigits: 2 });
    });
}
function getValorTotal(valor){
    this.valorT = valor;
}

function procurar(obj, agulha) {
    var chaves = Object.keys(obj);
    for (var i = 0; i < chaves.length; i++) {
        var chave = chaves[i];
        if (!obj[chave]) continue;
        else if (typeof obj[chave] == 'object') return procurar(obj[chave], agulha);
        else if (obj[chave].indexOf(agulha) != -1) return [obj, chave];
    }
    return false;
}
//remover item da tabela e adicionar na outra
function removerEditar(id,valor){
    //removendo a tr do array trs
    var rowRemove = 'item'+id;
    var row = "#item"+id;
    var string = document.getElementById(rowRemove).innerHTML;
    string = '<tr id="item'+id+'">'+$(row).html()+'</tr>';
    var indexRow = trs.indexOf(string.trim());
    if ( indexRow > -1) {
        trs.splice(indexRow, 1);
    }
    console.log(indexRow);
    //se retirar todos, eu volto uma página;
    if (pagina > 0 && $('#tbodyEmprestado>tr').length <= 1) {
        pagina--;
        paginar();    
        ajustarBotoes();
    }
    $(row).remove();

    $.get('/emprestimo/editar/item/'+id, function(resp) {
        valorT = valorT - valor;
        valorRee.innerHTML = (valorT).toLocaleString('pt-BR', { minimumFractionDigits: 2 });
        var index = emprestados.indexOf(id);

        if ( index > -1) { // se o itpCodigo estiver nos emprestados, eu removo
            emprestados.splice(index, 1);
        }
        var linha = '<tr id="'+resp.item.itpCodigo+'">' +
        '<td class="text-center"><div class="checkbox">'+
            '<label for="'+resp.item.itpCodigoBarras+'">'+
                '<input type="checkbox" id="'+resp.item.itpCodigoBarras+'" onclick="selecionarEditar('+resp.item.itpCodigo+')"value="'+resp.item.itpCodigo+'">'+
                '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>'+
            '</label>'+
        '</div></td>'+
        '  <td>' + resp.item.itpCodigoBarras + '</td>' +
        '  <td>' + resp.item.patNome + '</td>' +
        '  <td> ' + (resp.item.itpValorAtual).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })  + '</td>' +
        ' </td>'  +  
        '</tr>';

        $("#table-emprestimo>tbody").append(linha);
        

    });
}


//selecionar novamente o item que tinha sido removido - EDITAR
function selecionarEditar(id){
    var page = this.current_page;
    var unidade = this.unidade;
    var status = this.status;
    var categoria = this.categoria;
    var patrimonio = this.patrimonio;
    $.get('/emprestimo/editar/item/'+id, function(resp) {
        if(in_array(resp.item.itpCodigo,emprestados) == -1){
            emprestados.push(resp.item.itpCodigo);
            valorT += resp.item.itpValorAtual;
            var linha = '<tr id="item'+resp.item.itpCodigo+'">' +
                '  <td>' + resp.item.itpCodigoBarras + '</td>' +
                '  <td>' + resp.item.patNome + '</td>' +
                '  <td>' + (resp.item.itpValorAtual).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })  + '</td>' +
                ' <td colspan="3" class="text-center">' +
                    ' <div class="col-md-4">'+
                        '<button type="button" onclick="(removerEditar('+resp.item.itpCodigo+','+resp.item.itpValorAtual+'))" class="btn btn-warning">'+
                                'Remover</button>'+
                    '</div>'+
                ' </td>'  +  
                '</tr>';
            $('#tbodyEmprestado').append(linha);
            carregarPatrimonios(page,unidade,status,categoria,patrimonio);
            valorRee.innerHTML = (valorT).toLocaleString('pt-BR', { minimumFractionDigits: 2 });
        }
    });    
}
