var baseurl = window.location.protocol + '//' + window.location.host + '/';
var logo ='';
$(function(){
    /*Transformando a imagem em string base64*/
    function toDataURL(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                callback(reader.result);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }
        toDataURL('http://localhost:8000/imagens/tatame3.png', function(dataUrl) {
            logo = dataUrl
        });


    /*Transformando a imagem em string base64*/
});
function filtros(){
    $.ajax({
            type: 'POST',
            url: baseurl + 'relatorios',
            data:{'uniCodigo':uniCodigo.value,
                'catCodigo':catCodigo.value,
                'itpStatus':itpStatus.value,
                'itpSituacao':itpSituacao.value,
                'itpTipo':itpTipo.value},
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
                error: function(response) {
                    console.log("Erro:\n"+response);
                },
                success: function(resposta) {
                    //console.log(resposta.patrimonios);
                    if(resposta.patrimonios.length >= 1){
                        gerarPDF(resposta.patrimonios);
                    }else{
                       $('#modalAlert').modal('show');
                    }
                    
                }
        });
}
function gerarPDF(patrimonios){
    /***Preenchendo a tabela com conteudo dinamico***/
    var th = [{text:'Patrimônio',style:'th'},{text:'N°',style:'th'},{text:'Valor Atual(Unit)',style:'th'},{text:'Valor Total',style:'th'}];
    //verificando se o filtro foi aplicado, se foi deve ser retirado da listagem
    if(catCodigo.value == ''){
        th.push({text:'Categoria',style:'th'});
    }
    if(uniCodigo.value == ''){
        th.push({text:'Unidade',style:'th'});
    }
    if(itpStatus.value == ''){
        th.push({text:'Status',style:'th'});
    }
    if(itpTipo.value == ''){
        th.push({text:'Tipo',style:'th'});
    }
    if(itpSituacao.value == ''){
        th.push({text:'Situação',style:'th'});
    }
    let corpo = [th];
    var c = th.length -1;
    console.log(c);
    if(c != 8 && c !=7 && c!= 6){
        var tamanho = [200];
        for (var i = 0; i < c; i++) {
            tamanho.push('*');
        }
    }else{
        var tamanho = 0;
    }
    
    console.log(tamanho)
    var quantidade = 0;
    var valorTotal =0;
    var catEscolhida;
    var uniEscolhida;
    
    for(var i=0;i<patrimonios.length;i++){
        var valores = [];
        // formatando os números
        var total = (patrimonios[i].qtde * +patrimonios[i].itpValorAtual).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' });
        valores.push(patrimonios[i].patNome,patrimonios[i].qtde,(patrimonios[i].itpValorAtual).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' }),
            total)
            
            //verificando se o filtro foi aplicado, se foi deve ser retirado da listagem
            if(catCodigo.value == ''){
                valores.push(patrimonios[i].catNome);
            }else{
                //quer dizer que o filtro foi aplicado, então recuperando o nome
                catEscolhida = 'Categoria: '+patrimonios[i].catNome;
            }

            if(uniCodigo.value == ''){
                valores.push(patrimonios[i].uniNome);
            }else{
                //quer dizer que o filtro foi aplicado, então recuperando o nome
                uniEscolhida = 'Unidade: '+patrimonios[i].uniNome;
            }
            if(itpStatus.value == ''){
                valores.push(patrimonios[i].itpStatus);
            }
            if(itpTipo.value == ''){
                valores.push(patrimonios[i].itpTipo);
            }
            if(itpSituacao.value == ''){
                valores.push(patrimonios[i].itpSituacao);
            }
            corpo.push(valores);

        quantidade += patrimonios[i].qtde;
        valorTotal += patrimonios[i].qtde * patrimonios[i].itpValorAtual;
    }

    valorTotal = (valorTotal).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' });
    var thInfo = [[{text:'Quantidade: '+ quantidade,alignment:'left',style:'infos'},
    {text:'Valor Total: R$ '+ valorTotal, alignment:'center',style:'infos'}]];
    /********Mostrando os filtros escolhidos ********** */
    if(catCodigo.value != ''){
        thInfo.push([{text:catEscolhida,alignment:'left',style:'infos_2'},{text:''}]);
    }
    
    if(uniCodigo.value != ''){
        thInfo.push([{text:uniEscolhida,alignment:'left',style:'infos_2'},{text:''}]);
    }
    if(itpStatus.value != ''){
        var statusEscolhido = 'Status: '+ itpStatus.value;
        thInfo.push([{text:statusEscolhido,alignment:'left',style:'infos_2'},{text:''}]);
    }
    if(itpTipo.value != ''){
        var tipoEscolhido = 'Tipo: '+ itpTipo.value;
        thInfo.push([{text:tipoEscolhido,alignment:'left',style:'infos_2'},{text:''}]);
    }
    if(itpSituacao.value != ''){
        var situacaoEscolhida = 'Situação: '+ itpSituacao.value;
        thInfo.push([{text:situacaoEscolhida,alignment:'left',style:'infos_2'},{text:''}]);
    }

    monName = new Array ("janeiro", "fevereiro", "março", "abril", "maio", "junho","julho", "agosto", "setembro","outubro", "novembro", "dezembro")
    now = new Date();
    var data = "Formiga, " + now.getDate() + " de " + monName[now.getMonth()] + " de " + now.getFullYear();
    /*** FONTS***/
        pdfMake.fonts = {
        Roboto: {
            normal: 'Roboto-Regular.ttf',
            bold: 'Roboto-Medium.ttf',
            italics: 'Roboto-Italic.ttf',
            bolditalics: 'Roboto-Italic.ttf'
        },
        Times: {
           normal: 'Times-Roman.ttf',
           bold: 'Times-Bold.otf',
           italics: 'Times-Italic.ttf',
           bolditalics: 'Times-BoldItalic.ttf'
        }
    };      
    /*** FONTS***/

    /*** INÍCIO DA MONTAGEM ***/
    var dd = {
        pageSize: 'A4',
        pageMargins: [ 18, 20, 18, 10 ],
        content: [
            /***CABEÇALHO***/
            {
                style:'cabecalho',
                table:{
                    body:
                        [
                            [
                                {image: logo,width:60,height:60,alignment: 'left'}, 
                                {
                                    table: {
                                    body: [
                                            [
                                                {text:'ASSOCIAÇÃO TATAME DO BEM',style:'header'}
                                            ],   
                                            [
                                                {text:'R. José Pedro da Silva, - Bela Vista, Formiga - MG, 35570-000',style:'header_2'}
                                            ]
                                        ]
                                    },
                                    layout: 'noBorders'
                                }    
                            ],
                        ]
                },
                layout: 'noBorders'
            },
            /***CABEÇALHO***/
            /*** TÍTULO***/
            {
                text:'Relatório dos Bens Patrimoniais do Tatame do Bem',
                style:'header_3'
            },
            /*** TÍTULO***/
            {
                table:{
                    body: thInfo
                },
                layout:'noBorders'
            },
            /*** TABELA COM OS PATRIMÔNIOS***/
            {
                style: 'tablePat',
                table: {
                    widths:tamanho,
                    body:corpo
                },
                layout: {
                        paddingBottom: function(i, node) { return 10; },
                        paddingTop: function(i, node) { return 10; },
                }
            },
            /*** TABELA COM OS PATRIMÔNIOS***/
            /*** ASSINATURAS E DATA ***/
            {
                style: 'assinatura_data',
                text: data
            },
            {
                style: 'assinatura_linha',
                alignment: 'center',
                text: '_________________________________________________'
            },
            {
                style: 'assinatura_nome',
                alignment: 'center',
                text:'Presidente do Tatame do Bem'
            },
            {
                style: 'assinatura_linha',
                alignment: 'center',
                text: '_________________________________________________'
            },
            {
                style: 'assinatura_nome',
                alignment: 'center',
                text:'Vice-Presidente do Tatame do Bem'
            },
        ],
        /*** ASSINATURAS E DATA ***/
        /*** QUEBRA DE PÁGINAS ***/
        pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
                return currentNode.headlineLevel === 1 && followingNodesOnPage.length === 0;
        },
        /*** QUEBRA DE PÁGINAS ***/
        /*** ESTILIZAÇÃO ***/
        styles:{
            th:{
                font:'Times',
                fontSize:12,
                bold:true,
                alignment:'center'
            },
            header:{
                font:'Times',
                fontSize:15,
                bold:true,
                margin:[ 75, 0, 0, 0 ],
                alignment:'center'
            },
            header_2:{
                font:'Times',
                fontSize:10,
                bold:true,
                margin:[ 80, 0, 0, 0 ],
                alignment:'center'
            },
            header_3:{
                font:'Times',
                fontSize:14,
                bold:true,
                margin:[ 0, 15, 0, 0 ],
                alignment:'center'
            },
            infos:{
                font:'Times',
                fontSize:13,
                margin: [8,30, 0,0],
            },
            infos_2:{
                font:'Times',
                fontSize:13,
                margin: [8,10, 0,0],
            },
            tablePat:{
                font:'Times',
                fontSize:12,
                margin: [0,20, 0,20],
            },
            assinatura_data: {
                margin: [0,20, 0,30],
                alignment:'center'
            },
            assinatura_linha: {
                margin: [0,20, 0,0],
                alignment:'center'
            },
            assinatura_nome:{
                margin:[0,0,0,30],
                alignment:'center',
            }
        }
    };
    /*** ESTILIZAÇÃO ***/
    /*** FIM DA MONTAGEM ***/

    //pdfMake.createPdf(dd).open({}, window); 
    pdfMake.createPdf(dd).download('Relatório Patrimonios');
}