
pesquisarL.addEventListener('keyup', function(e){
  var key = e.which || e.keyCode;
  if (key == 13) { // codigo da tecla enter
    $.ajax({
        type: 'GET',
        url: baseurl + '/emprestimo/adicionar/leitor/'+this.value,
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
            error: function(response) {
                console.log("Erro:\n"+response);
            },
            success: function(resposta) {
                if(resposta != false){
                    if(in_array(resposta.item.itpCodigo,emprestados) == -1){ // se já nn tiver adicionado
                        emprestados.push(resposta.item.itpCodigo);
                        valorT += resposta.item.itpValorAtual;
                        var linha = '<tr id="item'+resposta.item.itpCodigo+'">' +
                        '  <td>' + resposta.item.itpCodigoBarras + '</td>' +
                        '  <td>' + resposta.item.patrimonio.patNome + '</td>' +
                        '  <td> ' + resposta.item.itpValorAtual + '</td>' +
                        ' <td>' +
                                '<button type="button" onclick="(remover('+resposta.item.itpCodigo+','+resposta.item.itpValorAtual+'))" class="btn btn-warning">'+
                                        'Remover</button>'+
                        ' </td>'  +  
                        '</tr>';
                        paginar(linha);
                        valorRee.innerHTML = (valorT).toLocaleString('pt-BR', { minimumFractionDigits: 2 });
                        $('#'+resposta.item.itpCodigo).remove();
                        if($("#table-emprestimo>tbody>tr").length == 0){
                            page = page+1;
                            carregarPatrimonios(page,unidade,status,categoria,patrimonio);
                        }
                    }   
                    
                }else{
                    alert('Esse código não foi encontrado ou o item está emprestado!');
                }
            }
    });
  }
});







