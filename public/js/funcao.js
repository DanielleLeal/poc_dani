var baseurl = window.location.protocol + '//' + window.location.host + '/';
    jQuery(function() {
            jQuery('#funcao-table').DataTable({
                dom: 'lBfrtip',
                "ajax": {
                    "url": '{{ route("funcao.datatable") }}',
                    "type": "GET"
                },
                "columns": [{
                        data: 'funNome',
                        name: 'Nome'
                    },
                    {
                        data: 'funDescricao',
                        name: 'Descrição'
                    },
                    {
                        data: "funCodigo",
                        render: function(data, type, row) {
                            var html = '<div class="col-md-6">';
                            html += '<p><a href="funcao/' + data + '/edit" class="btn btn-primary btn-block">';
                            html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                            html += '<div class="col-md-6">';
                            html += '<p><button type="button" class="btn btn-danger btn-block" onclick="abriModal('+data+')">';
                            html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div>';
                            return html;
                        },
                        name: 'Opções'
                    }
                ],
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    'pdfHtml5',
                    'excelHtml5'
                ]
            });
        });

        //preenchendo o modal
        function abriModal(data){
            var funCodigo = data;
            $.ajax({
				type: 'GET',
				url: baseurl + 'funcao/modal/'+funCodigo,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.funcao.funCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir a função "+ resposta.funcao.funNome+"?";
                            $('#modalExclusao').modal('show');	
                        }
					}
            });
        }  
            
            //mandando pro metodo destroy
            function funcaoExcluir(){
                var funCodigo = codigo.value;
                console.log(funCodigo);
                $.ajax({
                    type: 'DELETE',
                    url:baseurl+ "funcao/"+funCodigo,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
						'Content-Type': 'application/json',
    					'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                    cache: false,
                        error: function(response) {
                            console.log("Erro:\n"+response);
                        },
                        success: function(resposta) {
                            console.log(resposta);
                            $('#modalExclusao').modal('hide');
                            window.location.reload('#tab_2');
                        }		
                });
            
        }