var transferidos =[];

$('#patrimonio').select2();
$('#unidade').select2();

function getNextItem(data) {
    i = data.current_page+1; 
    if (data.current_page == data.last_page) 
        s = '<li class="page-item disabled">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Próximo</a></li>';
    return s;
}

function getPreviousItem(data) {
    i = data.current_page-1; 
    if (data.current_page == 1) 
        s = '<li class="page-item disabled">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Anterior</a></li>';
    return s;

}

function getItem(data, i) {
    if (data.current_page == i)
        s = '<li class="page-item active">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">' + i + '</a></li>';
    return s;
}

function montarPaginator(data) {
    
    $("#pagination>ul>li").remove(); 

    $("#pagination>ul").append(
        getPreviousItem(data)
    );
    
    n = 2; 
    if (data.current_page - n/2 <= 1) 
        inicio = 1;
    else if (data.last_page - data.current_page < n) 
        inicio = data.last_page - n + 1;
    else
        inicio = data.current_page - n/2;
    
    fim = inicio + n-1;

    for (i=inicio;i<=fim;i++) {
        $("#pagination>ul").append(
            getItem(data,i)
        );
    }
    $("#pagination>ul").append(
        getNextItem(data)
    );
}

function montarLinha(item) { 
    var html =  '<tr>';
            if(in_array(item.itpCodigo,transferidos) != -1){ // se tiver no array
                html += '<td class="text-center"><div class="checkbox">'+
                '<label for="'+item.itpCodigoBarras+'">'+
                    '<input type="checkbox" checked id="'+item.itpCodigoBarras+'" class="trans" onclick="transferir(this)"value="'+item.itpCodigo+'">'+
                    '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>'+
                '</label>';
            }else{
                html +='<td class="text-center"><div class="checkbox">'+
                '<label for="'+item.itpCodigoBarras+'">'+
                    '<input type="checkbox" id="'+item.itpCodigoBarras+'" class="trans" onclick="transferir(this)"value="'+item.itpCodigo+'">'+
                    '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>'+
                '</label>';
            }
                html +='<td class="text-center">'+item.itpCodigoBarras+'</td>'+
                '<td class="text-center">'+item.unidade.uniNome+'</td>'+
                '<td class="text-center">'+item.itpStatus+'</td>'+
                '<td class="text-center">'+item.itpSituacao+'</td>'+
                '<td class="text-center">'+item.itpTipo+'</td>'+
            '</tr>';

    return html;
}
function montarTabela(dados) {
    $("#itens-table>tbody>tr").remove();
    if(dados.length < 1){
        $("#itens-table>tbody").append(
            '<tr><td colspan="6" style="padding: 15px; font-size:14px;" class="text-center">Nenhum registro encontrado</td></tr>'
        );
    }
    for(i=0;i<dados.length;i++) {
        $("#itens-table>tbody").append(
            montarLinha(dados[i])
        );
    }
}


function carregarItens(pagina,filtro) { 
    $.get('/transferencia/itens/'+filtro,{page: pagina}, function(resp) {
        if(resp.itens.data.length > 0){
            $('#pagination').css('display','block');
            $('#card-title').css('display','block');
        }

        tr.checked = false;
        montarTabela(resp.itens.data);
        montarPaginator(resp.itens);
        
        $("#pagination>ul>li>a").click(function(){
            carregarItens($(this).attr('pagina'),filtro);
        });
        $("#card-title").html( "Mostrando de " + resp.itens.from + " até  "+ resp.itens.to+
        " de " + resp.itens.total+ " registros");
        });
};

function transferirTodos(){
    if(tr.checked){
        $('.trans').each(function () {
            if(this.checked == false){
                this.checked = true;
                if(in_array(this.value,transferidos) == -1){ // se nn tiver no array de transferidos;
                    transferidos.push(this.value);
                }
            }
            
        });
    }else{
        $('.trans').each(function () {
            this.checked = false;
            var index = transferidos.indexOf(this.value);
            if ( index > -1) { // se o itpCodigo estiver nos transferidos, eu removo
                transferidos.splice(index, 1);
            }
        });
    }
}
 function transferir(data){
     if(data.checked){
        if(in_array(data.value,transferidos) == -1){ // se nn tiver no array de transferidos;
            transferidos.push(data.value);
        }
     }else{
        var index = transferidos.indexOf(data.value);
        if ( index > -1) { // se o itpCodigo estiver nos transferidos, eu removo
            transferidos.splice(index, 1);
        }
     }
 }

 function submeter(){
     console.log(transferidos);
    itensT.value = transferidos;
    formTrans.submit();
 }
//para verificar se existe um elemento em um array
function in_array(needle, haystack){
    var found = 0;
    for (var i=0, len=haystack.length;i<len;i++) {
    if (haystack[i] == needle) return i;
    found++;
        }
    return -1;
}



//implementando pesquisa instântanea
$("#pesquisa").keyup(function(){
    if($("#pesquisa").val().length >= 2){
        var valor = $("#pesquisa").val();
        var patCodigo = $('#patrimonio').val();
        $.get('/transferencia/pesquisar/itens/'+patCodigo,{pesquisar: valor}, function(resp) {
            if(resp.itens.data.length > 0){
                $('#pagination').css('display','block');
                $('#card-title').css('display','block');
            }
    
            tr.checked = false;
            montarTabela(resp.itens.data);
            montarPaginator(resp.itens);
            
            $("#pagination>ul>li>a").click(function(){
                carregarItens($(this).attr('pagina'),filtro);
            });
            $("#card-title").html( "Mostrando de " + resp.itens.from + " até  "+ resp.itens.to+
            " de " + resp.itens.total+ " registros");
        });
    }else{
        carregarItens(1);
    }
    
});

