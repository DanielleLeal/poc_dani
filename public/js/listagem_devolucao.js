var paginaD=0;
var paginaS=0;
var paginaM=0;
var paginaT=0;
var tamanhoPagina = 10;
var tamanhoTodos = 10;
var emprestimosDia;
var emprestimoSemana;
var emprestimoMes;
var emprestimosTodos;
var baseurl = window.location.protocol + '//' + window.location.host + '/';

$(function(){
    $("#visu").val("Dia");
    $.ajax({
        type: 'GET',
        url: baseurl + '/emprestimo/devolucao/itens/',
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
            error: function(response) {
                console.log("Erro:\n"+response);
            },
            success: function(resposta) {
                paginarTodos(resposta.empTodos);
                paginarDia(resposta.empDia);
                paginarSemana(resposta.empSemana);
                paginarMes(resposta.empMes);
            }
    });
});
/*******Paginando as li do DIA****** */
function paginarDia(empDia) {
    emprestimosDia = empDia;
    $('#dia>li').remove();
    $('#dia>br').remove();
    if(empDia.length == 0){
        var li = '<li class="item vcenter">'+
                    '<span style="margin-left:60px;">'+
                            'Não possui nenhum empréstimo a ser devolvido neste dia'+
                    '</span>'+
                '</li><br>';
        $('#dia').append(li);
        $('#paginationD>ul').remove(); 
    }else{
        for (var i = paginaD * tamanhoPagina; i < empDia.length && i < (paginaD + 1) *  tamanhoPagina; i++) {
            var data = formatarDate(empDia[i].empDataEmprestimo);
            var li = '<li class="item vcenter ">'+
                    '<div class="divisoria">'+
                        ' <a href="/devolucao/itens/'+empDia[i].empCodigo+'" style="margin-right: 20px;">'+
                            '<i class="fa fa-angle-right ico" style="justify-content:end;" title="Devolver" aria-hidden="true"></i> '+
                        '</a>'+
                    '</div>'+
                    '<span class="text">Empréstimo feito à empresa '+
                        empDia[i].fornecedor.forNome+' no dia '+ ''+data+
                    '</span>'+
                '</li><br>';
            $('#dia').append(li);
            
        }
        $('#mostrarD>span').remove();
        if(empDia.length > tamanhoPagina){
            $('#paginationD').removeClass('hide');
            $('#paginationD').addClass('show');
            $('#mostrarD').append('<span>Mostrando '+(paginaD +1)+'de '+ Math.ceil(emprestimosDia.length / tamanhoPagina)+' páginas </span>');
        }
        
    }
    
}

function ajustarBotoesDia() {
    $('#proximoDia').prop('disabled', emprestimosDia.length <= tamanhoPagina || paginaD >= Math.ceil(emprestimosDia.length / tamanhoPagina) - 1);
    $('#anteriorDia').prop('disabled', emprestimosDia.length <= tamanhoPagina || paginaD == 0);
}


$('#proximoDia').click(function() {
    console.log(emprestimosDia );
    if (paginaD < emprestimosDia.length / tamanhoPagina - 1) {
        paginaD++;
        paginarDia(emprestimosDia);
        ajustarBotoesDia();
    }
});
$('#anteriorDia').click(function() {
    if (paginaD > 0) {
        paginaD--;
        paginarDia(emprestimosDia);    
        ajustarBotoesDia();
    }
});

/*******Paginando as li do SEMANA****** */
function paginarSemana(empSemana) {
    emprestimosSemana= empSemana;
    $('#semana>li').remove();
    $('#semana>br').remove();
    if(empSemana.length == 0){
        var li = '<li class="item vcenter ">'+
                    '<span style="margin-left:60px;"">Não possui nenhum empréstimo a ser devolvido nessa semana</span>'+
                '</li><br>';
        $('#semana').append(li);
        $('#paginationS>ul').remove(); 
    }else{
        for (var i = paginaS * tamanhoPagina; i < empSemana.length && i < (paginaS + 1) *  tamanhoPagina; i++) {
            var data = formatarDate(empSemana[i].empDataEmprestimo);
            var li = '<li class="item vcenter">'+
                    '<div class="divisoria">'+
                        ' <a href="/devolucao/itens/'+empSemana[i].empCodigo+'" style="margin-right: 20px;">'+
                            '<i class="fa fa-angle-right ico" style="justify-content:end;" title="Devolver" aria-hidden="true"></i> '+
                        '</a>'+
                    '</div>'+
                    '<span class="text">Empréstimo feito à empresa '+
                        empSemana[i].fornecedor.forNome+' no dia '+ ''+data+
                    '</span>'+
                '</li><br>';
            $('#semana').append(li);
            
        }

        $('#mostrarS>span').remove();
        if(empSemana.length > tamanhoPagina){
            $('#paginationS').removeClass('hide');
            $('#paginationS').addClass('show');
            $('#mostrarS').append('<span>Mostrando '+(paginaS +1)+'de '+ Math.ceil(emprestimosSemana.length / tamanhoPagina)+' páginas </span>');
        }
    }
    
}

function ajustarBotoesSemana() {
    $('#proximoS').prop('disabled', emprestimosSemana.length <= tamanhoPagina || paginaS >= Math.ceil(emprestimosSemana.length / tamanhoPagina) - 1);
    $('#anteriorS').prop('disabled', emprestimosSemana.length <= tamanhoPagina || paginaS == 0);
}


$('#proximoS').click(function() {
    if (paginaS < emprestimosSemana.length / tamanhoPagina - 1) {
        paginaS++;
        paginarSemana(emprestimosSemana);
        ajustarBotoesSemana();
    }
});
$('#anteriorS').click(function() {
    if (paginaS > 0) {
        paginaS--;
        paginarSemana(emprestimosSemana);    
        ajustarBotoesSemana();
    }
});

/*******Paginando as li do MÊS****** */
function paginarMes(empMes) {
    emprestimosMes =  empMes;
    $('#mes>li').remove();
    $('#mes>br').remove();
    if(empMes.length == 0){
        var li = '<li class="item vcenter">'+
                '<span style="margin-left:60px;">Não possui nenhum empréstimo a ser devolvido nesse mês</span>'+
                '</li><br>';
        $('#mes').append(li);
        $('#paginationM>ul').remove(); 
    }else{
        for (var i = paginaM * tamanhoPagina; i < empMes.length && i < (paginaM + 1) *  tamanhoPagina; i++) {
            var data = formatarDate(empMes[i].empDataEmprestimo);
            var li = '<li class="item vcenter ">'+
                        '<div class="divisoria">'+
                            ' <a href="/devolucao/itens/'+empMes[i].empCodigo+'" style="margin-right: 20px;">'+
                                '<i class="fa fa-angle-right ico" style="justify-content:end;" title="Devolver" aria-hidden="true"></i> '+
                            '</a>'+
                        '</div>'+
                        '<span class="text">Empréstimo feito à empresa '+
                            empMes[i].fornecedor.forNome+' no dia '+ ''+data+
                        '</span>'+
                        
                    '</li><br>';
            $('#mes').append(li);
        }
        $('#mostrarM>span').remove();
        if(empMes.length > tamanhoPagina){
            $('#paginationM').removeClass('hide');
            $('#paginationM').addClass('show');
            $('#mostrarM').append('<span>Mostrando '+(paginaM +1)+'de '+ Math.ceil(emprestimosMes.length / tamanhoPagina)+' páginas </span>');
        }
        
    }
    
}

function ajustarBotoesMes() {
    $('#proximoM').prop('disabled', emprestimosMes.length <= tamanhoPagina || paginaM >= Math.ceil(emprestimosMes.length / tamanhoPagina) - 1);
    $('#anteriorM').prop('disabled', emprestimosMes.length <= tamanhoPagina || paginaM == 0);
}


$('#proximoM').click(function() {
    if (paginaM < emprestimosMes.length / tamanhoPagina - 1) {
        paginaM++;
        paginarMes(emprestimosMes);
        ajustarBotoesMes();
    }
});
$('#anteriorM').click(function() {
    if (paginaM > 0) {
        paginaM--;
        paginarMes(emprestimosMes);    
        ajustarBotoesMes();
    }
});

function formatarDate(data){
    var ano = data.substr(0,4);
    var mes = data.substr(5,2);
    var dia = data.substr(8,2);
    var dataF = dia+'/'+mes+'/'+ano;
    return dataF;
}

function visualizar(valor){
    if(valor == 'Todas'){
        $('#dDia').removeClass('show');
        $('#dSemana').removeClass('show');
        $('#dMes').removeClass('show');

        $('#dDia').addClass('hide');
        $('#dSemana').addClass('hide');
        $('#dMes').addClass('hide');

        $('#todos').removeClass('hide');
        $('#todos').addClass('show');

    }else if(valor == 'Dia'){
        $('#todos').removeClass('show');
        $('#dSemana').removeClass('show');
        $('#dMes').removeClass('show');

        $('#todos').addClass('hide');
        $('#dSemana').addClass('hide');
        $('#dMes').addClass('hide');

        $('#dDia').removeClass('hide');
        $('#dDia').addClass('show');
    }else if(valor == 'Semana'){
        $('#todos').removeClass('show');
        $('#dDia').removeClass('show');
        $('#dMes').removeClass('show');

        $('#todos').addClass('hide');
        $('#dDia').addClass('hide');
        $('#dMes').addClass('hide');

        $('#dSemana').removeClass('hide');
        $('#dSemana').addClass('show');
    }else if(valor == 'Mes'){
        $('#todos').removeClass('show');
        $('#dDia').removeClass('show');
        $('#dSemana').removeClass('show');

        $('#todos').addClass('hide');
        $('#dDia').addClass('hide');
        $('#dSemana').addClass('hide');

        $('#dMes').removeClass('hide');
        $('#dMes').addClass('show');
    }
}


/*******Paginando as li do TODAS****** */
function paginarTodos(empTodos) {
    console.log(empTodos);
    emprestimosTodos = empTodos;
    $('#liTodos>li').remove();
    $('#liTodos>br').remove();
    if(empTodos.length == 0){
        var li = '<li class="item vcenter ">'+
                '<span style="margin-left:60px;" >Não possui nenhum empréstimo a ser devolvido neste dia</span>'+
                '</li><br>';
        $('#liTodos').append(li);
        $('#paginationT>ul').remove(); 
    }else{
        for (var i = paginaT * tamanhoTodos; i < empTodos.length && i < (paginaT + 1) *  tamanhoTodos; i++) {
            var data = formatarDate(empTodos[i].empDataEmprestimo);
            var li = '<li class="item vcenter ">'+
                        '<div class="divisoria">'+
                            ' <a href="/devolucao/itens/'+empTodos[i].empCodigo+'" style="margin-right: 20px;">'+
                                '<i class="fa fa-angle-right ico" style="justify-content:end;" title="Devolver" aria-hidden="true"></i> '+
                            '</a>'+
                        '</div>'+
                        '<span class="text">Empréstimo feito à empresa '+
                            empTodos[i].fornecedor.forNome+' no dia '+ ''+data+
                        '</span>'+
                '</li><br>';

                
            $('#liTodos').append(li);
        }
        $('#mostrarT>span').remove();
        if(empTodos.length >= tamanhoTodos){
            $('mostrarT').css('display','none');
            $('#mostrarT').append('<span>Mostrando '+(paginaT +1)+'de '+ Math.ceil(emprestimosTodos.length / tamanhoTodos)+' páginas </span>');
            $('#paginationT').css('display','none');
        }
        
    }
    
}

function ajustarBotoesTodos() {
    $('#proximoTodos').prop('disabled', emprestimosTodos.length <= tamanhoTodos || paginaT >= Math.ceil(emprestimosTodos.length / tamanhoTodos) - 1);
    $('#anteriorTodos').prop('disabled', emprestimosTodos.length <= tamanhoTodos || paginaT == 0);
}


$('#proximoTodos').click(function() {
    if (paginaT < emprestimosTodos.length / tamanhoTodos - 1) {
        paginaT++;
        paginarTodos(emprestimosTodos);
        ajustarBotoesTodos();
    }
});
$('#anteriorTodos').click(function() {
    if (paginaT > 0) {
        paginaT--;
        paginarTodos(emprestimosTodos);    
        ajustarBotoesTodos();
    }
});