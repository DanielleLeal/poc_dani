var filtro;


function formatarDate(data){
    var ano = data.substr(0,4);
    var mes = data.substr(5,2);
    var dia = data.substr(8,2);
    var dataF = dia+'/'+mes+'/'+ano;
    return dataF;
}
function cor(data){
    switch(data){
        case 'Pendente':
            return '<span class="badge badge-warning">Pendente</span>';
        break;
        case 'Atrasado':
            return '<span class="badge badge-danger" >Atrasado</span>';
        break;
        case 'Devolvido':
            return '<span class="badge badge-success">Devolvido</span>';
        break;
            
    }
}
function datatableFiltro(filtro){
    this.filtro = filtro;
    if(filtro != ''){
        carregarEmprestimo(1,filtro);
    }else{
        carregarEmprestimo(1);
    }
    
}

function filtroData(data){
    if(this.filtro == ''){
        this.filtro =0;
    }
    if(data != ''){
        carregarEmprestimo(1,this.filtro,data);
    }else{
        carregarEmprestimo(1,this.filtro);
    }
    
}


function getNextItemE(data) {
    i = data.current_page+1; 
    if (data.current_page == data.last_page) 
        s = '<li class="page-item disabled">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Próximo</a></li>';
    return s;
}

function getPreviousItemE(data) {
    i = data.current_page-1; 
    if (data.current_page == 1) 
        s = '<li class="page-item disabled">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Anterior</a></li>';
    return s;

}

function getItemE(data, i) {
    if (data.current_page == i)
        s = '<li class="page-item active">';
    else
        s = '<li class="page-item">';
    s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">' + i + '</a></li>';
    return s;
}

function montarPaginatorE(data) {
    
    $("#paginationEmp>ul>li").remove(); 

    $("#paginationEmp>ul").append(
        getPreviousItemE(data)
    );
    
    n = 2; 
    if (data.current_page - n/2 <= 1) 
        inicio = 1;
    else if (data.last_page - data.current_page < n) 
        inicio = data.last_page - n + 1;
    else
        inicio = data.current_page - n/2;
    
    fim = inicio + n-1;

    for (i=inicio;i<=fim;i++) {
        $("#paginationEmp>ul").append(
            getItemE(data,i)
        );
    }
    $("#paginationEmp>ul").append(
        getNextItemE(data)
    );
}

function montarLinhaE(emprestimo) { 
    var html = '<tr>';
                    var dataDev = formatarDate(emprestimo.empDataDevolucao);
                    html += '<td>'+dataDev+'</td>';
                    var corStatus = cor(emprestimo.empStatus);
                    html += '<td>'+corStatus+'</td>'+
                            '<td>'+emprestimo.forNome+'</td>'+
                            '<td>'+emprestimo.useNome+'</td>';
                    if(emprestimo.empStatus !='Devolvido'){
                        html+='<td><div class="col-md-4">';
                        html += '<p><a href="emprestimo/verMais/' + emprestimo.empCodigo + '" class="btn btn-success opacity btn-block">';
                        html += '<i class="fa fa-eye"></i>&nbsp; Ver mais</a></p></div>';
                        html += '<div class="col-md-4">';
                        html += '<p><a href="emprestimo/' + emprestimo.empCodigo + '/edit" class="btn btn-primary opacity btn-block">';
                        html += '<i class="fa fa-edit"></i>&nbsp; Editar</a></p></div>';
                        html += '<div class="col-md-4">';
                        html += '<p><button type="button" class="btn btn-danger opacity btn-block" onclick="abriModal('+emprestimo.empCodigo+')">';
                        html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div></td></tr>';
                    }else{
                        html+='<td><div class="col-md-4">';
                        html += '<p><a href="emprestimo/verMais/' + emprestimo.empCodigo + '" class="btn btn-success opacity btn-block">';
                        html += '<i class="fa fa-eye"></i>&nbsp; Ver mais</a></p></div>';
                        html += '<div class="col-md-4">';
                        html += '<p><button type="button" disabled  class="btn btn-primary disable opacity btn-block">';
                        html += '<i class="fa fa-edit"></i>&nbsp; Editar</button></p></div>';
                        html += '<div class="col-md-4">';
                        html += '<p><button type="button" class="btn btn-danger opacity btn-block" onclick="abriModal('+emprestimo.empCodigo+')">';
                        html += '<i class="fa fa-remove"></i>&nbsp; Excluir</button></p></div></td></tr>';
                    }         

                    return html;
}
function montarTabelaEmp(dados) {
    $("#emprestimo-table>tbody>tr").remove();
    if(dados.length < 1){
        $("#emprestimo-table>tbody").append(
            '<tr><td colspan="6" style="padding: 15px; font-size:14px;" class="text-center">Nenhum registro encontrado</td></tr>'
        );
    }
    for(i=0;i<dados.length;i++) {
        $("#emprestimo-table>tbody").append(
            montarLinhaE(dados[i])
        );
    }
}


function carregarEmprestimo(pagina,filtroE = 0,$data = 0) { 
    $.get('/datatable/emprestimo/'+filtroE+'/'+$data,{page: pagina}, function(resp) {
        montarTabelaEmp(resp.emprestimo.data);
        montarPaginatorE(resp.emprestimo);
        
        $("#paginationEmp>ul>li>a").click(function(){
            carregarEmprestimo($(this).attr('pagina'));
        });
        $("#mostrarNum").html( "Mostrando de " + resp.emprestimo.from + " até  "+ resp.emprestimo.to+
        " de " + resp.emprestimo.total+ " registros");
        });
};

$(function(){
    carregarEmprestimo(1);
});


//implementando pesquisa instântanea
$("#empPesquisa").keyup(function(){
    if($("#empPesquisa").val().length >= 2){
        var status = $("#empStatus").val();
        var valor = $("#empPesquisa").val();
        $.get('/emprestimo/listar/pesquisar',{pesquisar: valor,empStatus:status }, function(resp) {
            
           montarTabelaEmp(resp.emprestimo.data);
            montarPaginatorE(resp.emprestimo);
            
            $("#paginationEmp>ul>li>a").click(function(){
                carregarEmprestimo($(this).attr('pagina'));
            });

            $("#mostrarNum").html( "Mostrando de " + resp.emprestimo.from + " até  "+ resp.emprestimo.to+
            " de " + resp.emprestimo.total+ " registros"); 
        });
    }else{
        carregarEmprestimo(1);
    }
    
});

