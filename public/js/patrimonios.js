
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    var patrimonios; 

    $('#fornecedor_forCodigo').select2();
    $('#categoria_catCodigo').select2();
    $('#categoria').select2();
    
    //preview da imagem antes do upload
    $('#patFoto').change(function(){
        const file = $(this)[0].files[0];// acessando o nome do arquivo selecionado
        const fileReader = new FileReader();
        fileReader.onloadend = function(){ // lendo o arquivo
            if(file.type.match('image.*')){
                $('#img').attr('src',fileReader.result);
            }else{
                $('#aviso').modal('show');
            }
        }
        fileReader.readAsDataURL(file); // lendo o arquivo
        });   
    
    //preenchendo o modal de exclusão
    function abrirModal(data){
            $.ajax({
				type: 'GET',
				url: baseurl + 'patrimonio/'+data,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.patrimonio.patCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir o patrimônio <strong> "
                            + resposta.patrimonio.patNome+"</strong>? Consequentemente todos os itens de patrimônio associados a este serão deletados.";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
    }

    //mandando pro metodo destroy
    function funcaoExcluir(){
            var patCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "patrimonio/"+patCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        console.log(resposta);
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload();
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
        }

    //current_page = mostra o numero da página dos elementos.
    // no meu controlador está Patrimonio::paginate(10). Ou seja,vou mostrar 10 elementos de cada vez
    //então o current_page mostra qual das 10,está mostrando
    function getNextItem(data) {
        i = data.current_page+1; // o numero que fica no próximo é a página corrente mais 1. Ex: 91+1=92. Então quando eu pressionar o próximo ele vai para página 92
        if (data.current_page == data.last_page) // se o current_page for o última,o botão próximo deve ficar desabilitado
            s = '<li class="page-item disabled">';
        else
            s = '<li class="page-item">';
        s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Próximo</a></li>';
        return s;
    }
    
    function getPreviousItem(data) {
        i = data.current_page-1; // o numero que fica no anterior é a página corrente menos 1. Ex: 91-1=90. Então quando eu pressionar o anterior ele vai para página 90
        if (data.current_page == 1) // se a página atual for a primeira, o botão anterior deve ficar desabilitado
            s = '<li class="page-item disabled">';
        else
            s = '<li class="page-item">';
        s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Anterior</a></li>';
        return s;
        //o porque desse atributo pagina? No momento do clique no item, o elemento utilize esse atributo página para chamar a função carregarClientes

    }
    
    function getItem(data, i) {
        if (data.current_page == i) //Como estou paginando de 1 a 10, se o numero(item) for igual ao current_page eu ativo ele, pra ficar em destaque 
            s = '<li class="page-item active">';
        else
            s = '<li class="page-item">';
            //Esse javascript:void(0) evita que minha página volte ao começo quando clicar em um item
        s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">' + i + '</a></li>';
        return s;
    }

    function montarPaginator(data) {
        
        $("#paginationNav>ul>li").remove(); // removendo as li da ul do pagination

        $("#paginationNav>ul").append(
            getPreviousItem(data)
        );// aadicionando o botão 'anterior' da paginação
        
        n = 2; // eu quero que mostre 10 páginas de cada vez
        /*
        Eu quero mostrar 10 itens(paginas) de cada vez. Sendo que 5 anteriores 1 elemento ativo e os outros 4 posteriores
        Entretando isso não se aplica ao primeiro e ultimo caso. 
          1° caso :1 2 3 4 5 6 7* 8 9 10 (Problem)
          2° caso: 22 23 24 25 26 27 28 29 30 31 (Caso comum)
          3°caso :91 92 93 94 95 96 97 98 99 100 (Problem)

        */
        if (data.current_page - n/2 <= 1) // quem vai cair nesse caso é de 1 ao 6
            inicio = 1;
        else if (data.last_page - data.current_page < n) // quem vai cair é o 3° caso
            inicio = data.last_page - n + 1;
        else // Caso comum
            inicio = data.current_page - n/2;
        
        fim = inicio + n-1;

        for (i=inicio;i<=fim;i++) {
            $("#paginationNav>ul").append(
                getItem(data,i)
            );// adicionando os itens da páginação(1,2,3...)
        }
        $("#paginationNav>ul").append(
            getNextItem(data)
        );
    }
    
    function montarLinha(patrimonio) { //montando a linha da tabela, com cada patrimonio
        return '<tr>' +
            '  <td>' + patrimonio.patCodigo + '</td>' +
            '  <td>' + patrimonio.patNome + '</td>' +
            '  <td>' + patrimonio.categoria.catNome + '</td>' +
            '  <td>' + patrimonio.fornecedor.forNome + '</td>' +
            '  <td>' + patrimonio.patQuantidade + '</td>' +
            '  <td>' + (patrimonio.patValorCompra).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' }) + '</td>' +
                ' <td colspan="3" class="text-center">' +
                ' <div class="col-md-4">'+
                    '<a class="btn btn-success btn-block" href="patrimonio/itensPatrimonio/'+patrimonio.patCodigo+'">'+
                        '<i class="fa fa-eye"></i> Ver mais' +
                    '</a>' +
                '</div>'+ 
                '<div class="col-md-4">' +
                    '<a class="btn btn-primary btn-block"href="patrimonio/'+patrimonio.patCodigo+'/edit">'+
                        '<i class="fa fa-edit"></i> Editar'+
                    '</a>'+
                '</div>'+
                '<div class="col-md-4">'+
                    '<button class="btn btn-danger btn-block" type="button" onclick="abrirModal('+patrimonio.patCodigo+')">'+
                            '<i class="fa fa-remove"></i> Excluir'+
                    '</button>'  +
                '</div>'+
            ' </td>'  +  
            
            '</tr>';
    }
    function montarTabela(dados) {
        $("#patrimonio-table>tbody>tr").remove(); // remover todas as linhas,antes de carregar os meus patrimonios
        if(dados.length < 1){
            $("#patrimonio-table>tbody").append(
                '<tr><td colspan="7" style="padding: 15px; font-size:14px;" class="text-center">Nenhum registro encontrado</td></tr>'
            );
        }
        for(i=0;i<dados.length;i++) {
            $("#patrimonio-table>tbody").append(
                montarLinha(dados[i])
            );// adicionando os tr no tbody da tabela
        }
    }

    function carregarPatrimonios(pagina,i =0) { 
        $.get('/patrimonios/json/'+i,{page: pagina}, function(resp) { // vai no controller e busca os patrimônios
            montarTabela(resp.patrimonios.data);
            montarPaginator(resp.patrimonios);
            // Porque colocar esse evento aqui? Porque nesse momento todos os cliente já foram carregados, a tabela e o paginator
            $("#paginationNav>ul>li>a").click(function(){// no momento que eu clicar no item dá paginação, ele chama a função carregar clientes e passa qual página quer que carregue 
                carregarPatrimonios($(this).attr('pagina'));
            });
            /*Todos os atributos utilizados já vem com o objeto retornado do paginate(lá do controller)*/
            $("#card-title").html( "Mostrando de " + resp.patrimonios.from + " até  "+ resp.patrimonios.to+
            " de " + resp.patrimonios.total+ " registros");   
            });
    };

    $(function(){ // é executado assim que todos os elementos do browser é renderizado é executada essa função automaticamente
        carregarPatrimonios(1);
        $('#patValorCompra').mask('000.000.000.000.000,00', {reverse: true});
    });

    //filtra os patrimônios, por categoria
    function filtrar(catCodigo){
        carregarPatrimonios(1,catCodigo);
    }  
    
    //implementando pesquisa instântanea
    $("#pesquisar").keyup(function(){
        if($("#pesquisar").val().length >= 2){
            var valor = $("#pesquisar").val();
            $.get('/patrimonios/pesquisar',{pesquisar: valor}, function(resp) { // vai no controller e busca os patrimônios de acordo com que foi digitado
                montarTabela(resp.patrimonios.data);
                montarPaginator(resp.patrimonios);
                // Porque colocar esse evento aqui? Porque nesse momento todos os cliente já foram carregados, a tabela e o paginator
                $("#paginationNav>ul>li>a").click(function(){// no momento que eu clicar no item dá paginação, ele chama a função carregar clientes e passa qual página quer que carregue 
                    carregarPatrimonios($(this).attr('pagina'));
                });
                /*Todos os atributos utilizados já vem com o objeto retornado do paginate(lá do controller)*/
                $("#card-title").html( "Mostrando de " + resp.patrimonios.from + " até  "+ resp.patrimonios.to+
                " de " + resp.patrimonios.total+ " registros");   
            });
        }else{
            carregarPatrimonios(1);
        }
        
    });