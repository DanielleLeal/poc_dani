
var baseurl = window.location.protocol + '//' + window.location.host + '/';
var devolvidos = [];
var marcados =[];
var marcados2 = [];
var itensNDevolvidos =[];

function selecionarTodos(){
    if(ch.checked){ // se o devolver todos estiver selecionado
        $('.marcar').each(function () { //eu seleciono todos
            this.checked = true;
            if(in_array(this.value,devolvidos) == -1){ // se nn tiver no array de devolvidos;
                devolvidos.push(this.value);
            }
        });
        console.log('marcando todos:'+devolvidos);
    }else{ // caso não esteja selecionado
        $('.marcar').each(function () { // eu desmarco todos
            this.checked = false;
            var index = devolvidos.indexOf(this.value);
            if ( index > -1) { // se o itpCodigo estiver nos devolvidos, eu removo
                    devolvidos.splice(index, 1);
            }
            
        });
    }
}


  

//para verificar se existe um elemento em um array
function in_array(needle, haystack){
    var found = 0;
    for (var i=0, len=haystack.length;i<len;i++) {
    if (haystack[i] == needle) return i;
    found++;
        }
    return -1;
}


function selecionar(data){
    if(in_array(data.value,devolvidos) == -1){ // se nn tiver no array de devolvidos;
        devolvidos.push(data.value);
    }else{
        var index = devolvidos.indexOf(data.value);
        if ( index > -1) { // se o itpCodigo estiver nos devolvidos, eu removo
            devolvidos.splice(index, 1);
        }
    }
}

var nao_devolvidos =[];
function formV(){
    $.get('/datatable/devolucao/'+empCodigo.value, function(resp) {
            var itens = resp.data;
            itens.forEach(element => {
                if(in_array(element.itpCodigo,devolvidos) == -1){ //se nn tiver no array de devolvidos; 
                    nao_devolvidos.push(element);
                }
            });
            itensDevolvidos.value = devolvidos;
            submeterForm(nao_devolvidos);
    });
}

function submeterForm(devolvidosN){
    this.itensNDevolvidos = devolvidosN;
    if(devolvidosN.length == 0){
        formDev.submit();
    }else{

        devolvidosN.forEach(element => {
            var tr = '<tr>'+
                        '<td><div class="checkbox">'+
                        '<label for="itemN'+element.itpCodigo+'">'+
                            '<input type="checkbox" class="mudar" id="itemN'+element.itpCodigo+'" onclick="mudarStatus(this)" value="'+element.itpCodigo+'">'+
                            '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>'+
                        '</label>'+
                    '</div></td>'+
                    '<td>'+element.itpCodigoBarras+'</td>'+
                    '<td>'+element.patNome+'</td>'+
                    '<td>'+(element.itpValorAtual).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })+'</td>'+
                    '<td id="status'+element.itpCodigo+'"> - </td>';
            $('#tbody_itensN').append(tr);   
        });
        
        $('#tab_1').removeClass('active');
        $('#li_1').removeClass('active');
        $('#li_1').css('display','none');
        $('#tab_2').addClass('active');
        $('#li_2').addClass('active');
        $('#li_2').css('display','block');

        $('#buttons').css('display','block');

        jQuery(function() {
            jQuery('#table-itensN').DataTable({
                dom: 'lBfrtip',
                "drawCallback": function(settings){
                    marcados2 =[];
                    $('.mudar').each(function () {
                        if(this.checked){
                            marcados2.push(this);
                        }
                    });
                    if($('.mudar').length == marcados2.length){
                        md.checked = true;
                    }else{
                        md.checked = false;
                    }
                },
                "language": {
                    "url": "http://cdn.datatables.net/plug-ins/1.10.15/i18n/Portuguese-Brasil.json"
                },
                buttons: [
                    'pdfHtml5',
                    'excelHtml5'
                ]
            });
        });
    }
}

function mudarStatus(input)
{
    if(input.checked){
        var td = '#status'+input.value;
        $(td).html( itpStatus.value);
    }else{
        var td = '#status'+input.value;
        $(td).html('-');
    }
    
        
}

function mudarTodos(){
    if(md.checked){
        $('.mudar').each(function () {
            if(this.checked == false){
                this.checked = true;
                var td = '#status'+this.value;
                $(td).html( itpStatus.value);
            }
            
        });
    }else{
        $('.mudar').each(function () {
            this.checked = false;
            var td = '#status'+this.value;
            $(td).html('-');
        });
    }
    
}

function enviar(){
    var itens =[];
    var status
    itensNDevolvidos.forEach(element => {
        var td = '#status'+element.itpCodigo;
        if($(td).html() == undefined || $(td).html() == '' || $(td).html() == " - "){
            status = null;
        }else{
            status = $(td).html();
        }
        var item = {itpCodigo:element.itpCodigo,itpStatus: status};
        itens.push(item);
    });
    $.ajax({
        type: 'POST',
        url:baseurl+ "devolucao/storeJson",
        data:{'itensNaoDevolvidos': itens,'itensDevolvidos': itensDevolvidos.value,'empCodigo': empCodigo.value},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
            error: function(response) {
                console.log("Erro:\n"+response);
            },
            success: function(resposta) {
                console.log(resposta);
                if(resposta == true){
                    window.history.pushState("", "", "/emprestimo/devolucao/index");
                    window.location.reload();
                }else{
                    alert('Não foi possível realizar a devolução');
                }
            }		
        });
}


