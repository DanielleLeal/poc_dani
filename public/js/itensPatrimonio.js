
    var baseurl = window.location.protocol + '//' + window.location.host + '/';
    var tipo ='';       
    
    //preenchendo o modal
    function abrirModal(data){
            $.ajax({
				type: 'GET',
				url: baseurl + 'itensPatrimonio/'+data,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        if(resposta != 'Vazio'){
                            codigo.value = resposta.item.itpCodigo;
                            modalBody.innerHTML = "Tem certeza que deseja excluir o item de patrimônio de <strong> Código "+ resposta.item.itpCodigoBarras+"</strong>";
                            $('#modalExclusao').modal('show');	
                        }else{
                            alert('Não é possível realizar a exclusão,tente mais tarde!');
                        }
					}
            });
    }

    //mandando pro metodo destroy
    function funcaoExcluir(){
            var itpCodigo = codigo.value;
            $.ajax({
                type: 'DELETE',
                url:baseurl+ "itensPatrimonio/"+itpCodigo,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
					'Content-Type': 'application/json',
    				'Authorization': '${this.state.tokenType} ${this.state.token}'
				},
                cache: false,
                    error: function(response) {
                        console.log("Erro:\n"+response);
                    },
                    success: function(resposta) {
                        if(resposta == 'true'){
                            $('#modalExclusao').modal('hide');
                            window.location.reload();
                        }else{
                            alert('Não foi possível realizar a exclusão,tente mais tarde!');
                        }
                    }		
                });
            
    }
    //visualizar a nota fiscal
    function abriNF(id){
            $.ajax({
				type: 'GET',
				url: baseurl + 'itensPatrimonio/nota_fiscal/'+id,
                headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				cache: false,
					error: function(response) {
						console.log("Erro:\n"+response);
					},
					success: function(resposta) {
                        console.log(resposta.notaFiscal.nofArquivo);
                        if(resposta.notaFiscal != null){
                            imgNF.src = "/storage/"+resposta.notaFiscal.nofArquivo;
                            $('#modalNF').modal('show');
                        }else{
                            alert('Não é possível visualizar a nota fiscal');
                        }
                        
					}
            });
    }
    //abrir modal de adicionar item de patrimônio
    function abrirAdicionar(){
        $('#modalAdicionar').modal('show');
    }
    //submeter formulário do adicionar
    function submeter(){
        $('#formItem').submit()(function(event){
            event.preventDefault();
            $('#modalAdicionar').modal('hide');    
        });
    }

    //current_page = mostra o numero da página dos elementos.
    // no meu controlador está Patrimonio::paginate(10). Ou seja,vou mostrar 10 elementos de cada vez
    //então o current_page mostra qual das 10,está mostrando
    function getNextItem(data) {
        i = data.current_page+1; // o numero que fica no próximo é a página corrente mais 1. Ex: 91+1=92. Então quando eu pressionar o próximo ele vai para página 92
        if (data.current_page == data.last_page) // se o current_page for o última,o botão próximo deve ficar desabilitado
            s = '<li class="page-item disabled">';
        else
            s = '<li class="page-item">';
        s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Próximo</a></li>';
        return s;
    }
    
    function getPreviousItem(data) {
        i = data.current_page-1; // o numero que fica no anterior é a página corrente menos 1. Ex: 91-1=90. Então quando eu pressionar o anterior ele vai para página 90
        if (data.current_page == 1) // se a página atual for a primeira, o botão anterior deve ficar desabilitado
            s = '<li class="page-item disabled">';
        else
            s = '<li class="page-item">';
        s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">Anterior</a></li>';
        return s;
        //o porque desse atributo pagina? No momento do clique no item, o elemento utilize esse atributo página para chamar a função carregarClientes

    }
    
    function getItem(data, i) {
        if (data.current_page == i) //Como estou paginando de 1 a 10, se o numero(item) for igual ao current_page eu ativo ele, pra ficar em destaque 
            s = '<li class="page-item active">';
        else
            s = '<li class="page-item">';
            //Esse javascript:void(0) evita que minha página volte ao começo quando clicar em um item
        s += '<a class="page-link" ' + 'pagina="'+i+'" ' + ' href="javascript:void(0);">' + i + '</a></li>';
        return s;
    }

    function montarPaginator(data) {
        
        $("#paginationNav>ul>li").remove(); // removendo as li da ul do pagination

        $("#paginationNav>ul").append(
            getPreviousItem(data)
        );// aadicionando o botão 'anterior' da paginação
        
        n = 2; // eu quero que mostre 10 páginas de cada vez
        /*
        Eu quero mostrar 10 itens(paginas) de cada vez. Sendo que 5 anteriores 1 elemento ativo e os outros 4 posteriores
        Entretando isso não se aplica ao primeiro e ultimo caso. 
          1° caso :1 2 3 4 5 6 7* 8 9 10 (Problem)
          2° caso: 22 23 24 25 26 27 28 29 30 31 (Caso comum)
          3°caso :91 92 93 94 95 96 97 98 99 100 (Problem)

        */
        if (data.current_page - n/2 <= 1) // quem vai cair nesse caso é de 1 ao 6
            inicio = 1;
        else if (data.last_page - data.current_page < n) // quem vai cair é o 3° caso
            inicio = data.last_page - n + 1;
        else // Caso comum
            inicio = data.current_page - n/2;
        
        fim = inicio + n-1;

        for (i=inicio;i<=fim;i++) {
            $("#paginationNav>ul").append(
                getItem(data,i)
            );// adicionando os itens da páginação(1,2,3...)
        }
        $("#paginationNav>ul").append(
            getNextItem(data)
        );
    }
    function montarLinha(item) { //montando a linha da tabela, com cada patrimonio
        return '<tr>' +
            '  <td>' + item.itpCodigoBarras + '</td>'+
            '  <td>' + item.unidade.uniNome + '</td>' +
            '  <td>' + item.itpStatus + '</td>' +
            '  <td>' + item.itpSituacao + '</td>' +
            '  <td>' + item.itpTipo + '</td>' +
            '  <td>' + (item.itpValorAtual).toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' }) + '</td>' +
            ' <td class="text-center">'+ '<button class="btn btn-success" onclick="abriNF('+item.nota_fiscal_nofCodigo+')" type="button" id="verNF">'+
                        '<i class="fa fa-eye"></i>'+
                    '</button>'+
             '</td>'+
             '<td colspan="3" class="text-center">'+
                '<div class="col-md-3">'+
                    '<a class="btn btn-primary btn-md opac" href="'+item.itpCodigo+'/edit">'+
                        '<i class="fa fa-edit"></i> Editar'+
                    '</a>'+
                '</div>'+
                '<div class="col-md-3">'+
                    '<button class="btn btn-danger btn-md opac" type="button" onclick="abrirModal('+item.itpCodigo+')">'+
                        '<i class="fa fa-remove"></i> Excluir'+
                    '</button>'+
                '</div>'+
                '<div class="col-md-3">'+
                    '<button class="btn btnCodigo btn-md opac" id="btnCodigo" type="button" onclick="gerarBarcode('+item.itpCodigo+')">'+
                        '<i class="fa fa-barcode"></i> Código de Barras'+
                    '</button>'+
                '</div>'+    
                        
            '</td>' +
            '</tr>';
    }
    function montarTabela(dados) {
        $("#itensPatrimonio-table>tbody>tr").remove(); // remover todas as linhas,antes de carregar os meus itensPatrimonio-table
            if(dados.length < 1){
                $("#itensPatrimonio-table>tbody").append(
                '<tr><td colspan="9" style="padding: 15px; font-size:14px;" class="text-center">Nenhum registro encontrado</td></tr>'
                );
            }
            
        for(i=0;i<dados.length;i++) {
            $("#itensPatrimonio-table>tbody").append(
                montarLinha(dados[i])
            );// adicionando os tr no tbody da tabela
        }
    }
    function carregarItensPatrimonios(pagina,tipo = null,valor =null) { 
        var pat = patCodigo.value;
        console.log(pat);
        $.get('json/'+pat+'/'+tipo+'/'+valor,{page: pagina}, function(resp) { // vai no controller e busca os patrimônios
            console.log(resp);
            montarTabela(resp.itens.data);
            montarPaginator(resp.itens);
            // Porque colocar esse evento aqui? Porque nesse momento todos os cliente já foram carregados, a tabela e o paginator
            $("#paginationNav>ul>li>a").click(function(){// no momento que eu clicar no item dá paginação, ele chama a função carregar clientes e passa qual página quer que carregue 
                carregarItensPatrimonios($(this).attr('pagina'));
            });
            /*Todos os atributos utilizados já vem com o objeto retornado do paginate(lá do controller)*/
            $("#card-title").html( "Mostrando de " + resp.itens.from + " até  "+ resp.itens.to+
            " de " + resp.itens.total+ " registros");  
            
        });    
    };
    $(function(){ // é executado assim que todos os elementos do browser é renderizado é executada essa função automaticamente
        carregarItensPatrimonios(1);
    });

    function filtrarValor(valor){
        var pat = patCodigo.value;
        carregarItensPatrimonios(1,tipo,valor)
    }

    //preencher o select valor
    function filtrarPor(e){
        tipo = e;
        $.get('/filtrarPor/'+tipo, function(resp) {
            $("#valor>option").remove();//removendo os options
            $("#valor").append('<option value="0">Selecione</option>');
          if(tipo == 'unidade'){
            for(var i=0;i<resp.unidades.length;i++){
                $("#valor").append(
                    '<option value="'+resp.unidades[i].uniCodigo+'">'+resp.unidades[i].uniNome+'</option>')
            }
           }else if (tipo == 'status'){
                for(var i=0;i<resp.status.length;i++){
                    $("#valor").append(
                        '<option value="'+resp.status[i].nome+'">'+resp.status[i].nome+'</option>')
                }
            }else if(tipo == 'situacao'){
                for(var i=0;i<resp.situacao.length;i++){
                    $("#valor").append(
                        '<option value="'+resp.situacao[i].nome+'">'+resp.situacao[i].nome+'</option>')
                }
            }else if(tipo == 'tipo'){
                for(var i=0;i<resp.tipo.length;i++){
                    $("#valor").append(
                        '<option value="'+resp.tipo[i].nome+'">'+resp.tipo[i].nome+'</option>')
                }
            }else if(tipo == 'todos'){
                $("#valor>option").remove();
                $('#valor').append('<option>Nenhum</>')
                var pat = patCodigo.value
                carregarItensPatrimonios(1,tipo)
            }  
            
            
        });  
    }

    //implementando pesquisa instântanea
    $("#pesquisar").keyup(function(){
        if($("#pesquisar").val().length >= 1){
            var valor = $("#pesquisar").val();
            $.get('/itensPatrimonios/pesquisar',{pesquisar: valor}, function(resp) { // vai no controller e busca os patrimônios de acordo com que foi digitado
                montarTabela(resp.itens.data);
                montarPaginator(resp.itens);
                // Porque colocar esse evento aqui? Porque nesse momento todos os cliente já foram carregados, a tabela e o paginator
                $("#paginationNav>ul>li>a").click(function(){// no momento que eu clicar no item dá paginação, ele chama a função carregar clientes e passa qual página quer que carregue 
                    carregarItensPatrimonios($(this).attr('pagina'));
                });
                /*Todos os atributos utilizados já vem com o objeto retornado do paginate(lá do controller)*/
                $("#card-title").html( "Mostrando de " + resp.itens.from + " até  "+ resp.itens.to+
                " de " + resp.itens.total+ " registros");   
            });
        }else{
            carregarItensPatrimonios(1);
        }
        
    });
