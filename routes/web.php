<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/teste/pdf',function(){
    return view('teste');
});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('datatable/home','HomeController@dataTableJson')->name('home.datatable');
Route::get('home/categoria','HomeController@getCategorias');
Route::get('home/unidade','HomeController@getUnidades');

/**************ROTAS FUNÇÃO*************/
Route::resource('/funcao', 'FuncaoController');
Route::get('datatable/funcao','FuncaoController@dataTableJson')->name('funcao.datatable');

/**************ROTAS USUÁRIOS*************/
Route::resource('/user', 'UserController');
Route::get('datatable/user','UserController@dataTableJson')->name('user.datatable');
Route::get('/user/perfil/{useCodigo}','UserController@perfil')->name('user.perfil');//visualizar o perfil
Route::post('user/editar','UserController@editarPerfil'); //editar
Route::post('user/uploadFoto','UserController@upload')->name('user.uploadFoto');

/**************ROTAS CATEGORIAS*************/
Route::resource('/categoria', 'CategoriaController');
Route::get('datatable/categoria','CategoriaController@dataTableJson')->name('categoria.datatable');

/**************ROTAS UNIDADES*************/
Route::resource('/unidade', 'UnidadeController');
Route::get('datatable/unidade','UnidadeController@dataTableJson')->name('unidade.datatable');
Route::get('unidade/cidade/{estCodigo}','UnidadeController@getCidades')->name('unidade.cidade');

/**************ROTAS FORNECEDORES*************/
Route::resource('/fornecedor', 'FornecedorController');
Route::get('datatable/fornecedor','FornecedorController@dataTableJson')->name('fornecedor.datatable');
Route::get('fornecedor/verMais/{forCodigo}','FornecedorController@verMais')->name('fornecedor.verMais');
Route::get('fornecedor/cidade/{estCodigo}','FornecedorController@getCidades')->name('fornecedor.cidade');

/**************ROTAS PATRIMONIOS*************/
Route::resource('/patrimonio', 'PatrimonioController');
Route::get('patrimonios/json/{catCodigo?}','PatrimonioController@getPatrimonios');//preencher da tabela
Route::get('patrimonios/pesquisar','PatrimonioController@pesquisa');//pesquisa instantanea
Route::get('patrimonios/export', 'PatrimonioController@export')->name('patrimonio.export');

/**************ROTAS ITENS DE PATRIMONIOS*************/
Route::get('patrimonio/itensPatrimonio/{patCodigo}','PatrimonioController@getItensPatrimonio')->name('patrimonio.itensPatrimonio');
Route::resource('/itensPatrimonio','ItensPatrimonioController');
Route::get('patrimonio/itensPatrimonio/{itpCodigo}/edit','ItensPatrimonioController@edit');//editar um item de patrimonio
Route::get('patrimonio/itensPatrimonio/json/{patCodigo}/{tipoS?}/{valor?}','ItensPatrimonioController@getJson');//preenchendo a tabela
Route::get('filtrarPor/{tipo}','ItensPatrimonioController@preencherFiltro');//preencher o filtro
Route::get('itensPatrimonio/nota_fiscal/{nofCodigo}','ItensPatrimonioController@getNF');//visualizar nota fiscal
Route::get('itensPatrimonios/pesquisar','ItensPatrimonioController@pesquisa');//pesquisa instântanea
Route::get('itensPatrimonio/barcode/{itpCodigo}','ItensPatrimonioController@gerarCodigoBarras');//gerar código de barras
Route::get('itensPatrimonios/pesquisar/notaFiscal','ItensPatrimonioController@pesquisaNF');
Route::get('itens_patrimonio/export/{patCodigo}', 'ItensPatrimonioController@export')->name('itens_patrimonio.export');


/**************ROTAS NOTA FISCAL*************/
Route::resource('/notaFiscal', 'NotaFiscalController');
Route::get('datatable/notaFiscal','NotaFiscalController@dataTableJson')->name('notaFiscal.datatable');
Route::get('visualizar/nota_fiscal/{nofCodifo}','NotaFiscalController@verNF');
Route::post('/notaFiscal/cadastrar','NotaFiscalController@cadastrarJS')->name('notaFiscal.cadastrar');

/**************ROTAS AUTORIZACAO*************/
Route::resource('/autorizacao', 'AutorizacaoController');
Route::get('datatable/autorizacao','AutorizacaoController@dataTableJson')->name('autorizacao.datatable');

/**************ROTAS RELATÓRIOS*************/
Route::resource('relatorios', 'RelatoriosController');

/**************ROTAS EMPRESTIMOS*************/
Route::resource('/emprestimo', 'EmprestimoController');
Route::get('/emprestimo/patrimonios/json/{uniCodigo?}/{status?}/{catCodigo?}/{patCodigo?}','EmprestimoController@getPatrimonios');// listar patrimonios que podem ser emprestados
Route::get('datatable/emprestimo/{empStatus?}/{empData?}','EmprestimoController@dataTableJson')->name('emprestimo.datatable'); // listando os emprestimos
Route::get('/emprestimo/item/adicionar/{itpCodigo}','EmprestimoController@adicionar');// buscando item que foi selecionado para ser emprestado
Route::get('/emprestimo/verMais/{empCodigo}','EmprestimoController@verMais');// ver todos os detalhes do emprestimo
Route::get('/emprestimo/editar/json/{empCodigo}','EmprestimoController@getItensEditar'); // buscar itens que foram emprestados durante o cadastro para edição
Route::get('/emprestimo/editar/item/{itpCodigo}','EmprestimoController@getItem');
Route::get('/emprestimo/listar/pesquisar','EmprestimoController@pesquisarListagem');
Route::get('/emprestimo/adicionar/leitor/{itpCodigoBarras}','EmprestimoController@getItemLeitor'); // buscar item no banco pelo código de barras lido pelo leitor

/**************ROTAS DEVOLUÇÕES*************/
Route::get('/emprestimo/devolucao/index/','DevolucaoController@index')->name('emprestimo.indexD');
Route::get('/emprestimo/devolucao/itens/','DevolucaoController@devolucao')->name('emprestimo.devolucao');
Route::get('/devolucao/itens/{empCodigo}','DevolucaoController@telaDevolver');
Route::get('/datatable/devolucao/{empCodigo}','DevolucaoController@dataTableJson')->name('devolucao.datatable');
Route::post('/devolucao/storeJson','DevolucaoController@storeJson');
Route::resource('devolver', 'DevolucaoController');

/**************ROTAS DEPRECIAÇÃO*************/
Route::resource('depreciacao', 'DepreciacaoController');


/**************ROTAS TRANSFERÊNCIA*************/
Route::resource('transferencia', 'TransferirController');
Route::get('/transferencia/itens/{patCodigo}', 'TransferirController@getItens');
Route::get('/transferencia/pesquisar/itens/{patCodigo?}', 'TransferirController@pesquisar');


